#!/usr/bin/env python
#############################################
# Script to plot and compare pulls from 
# several FitCrossChecks.root files
# Adapted from:
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/WorkspaceMaker#Fit_Diagnostics
#
# How to run:
# get the Moriond16 tag of WSMaker: https://svnweb.cern.ch/trac/atlasphys-hsg5/browser/Physics/Higgs/HSG5/Limits/InputPreparation/WSMaker/tags/WSMaker-02-00-00
# source setup.sh 
# python comparePulls.py <input dir> <region> <is_conditional> <mu> <output dir> <fit1> <fit2> <...>
# where <input dir>/<fit1> contains FitCrossChecks.root
# e.g.:
# python comparePulls.py "inputRootFiles" "Global" 0 0 "./outDir" llbb_Dec7_2000_mergeCR llbb_Dec7_2000_mergeCR100
#
#############################################

import sys
import os

import ROOT
from ROOT import gDirectory, gROOT
import makeReducedDiagPlots

#extension = "png"
#extension = "pdf"
extension = "eps"

def main(versions, basedir_fcc, is_region, is_conditional, mu_conditionnal, outplotdir):
    # should pass as parameters the region and type of fit we want...
    # and filename oo
    if len(versions)<1:
        print "Nothing to merge, aborting"
        return
    os.system("mkdir -vp "+outplotdir)
    gROOT.SetBatch(True)
    ROOT.gSystem.Load("libPlotUtils.so")
    pulls = []
    axiss = []
    nuiss = []

    for v in versions:
        directory = basedir_fcc.format(v)
        print directory
        fname = "{0}/FitCrossChecks.root".format(directory)
        pull, corr = get_nuis_corr(fname, is_region, is_conditional, mu_conditionnal)
        pulls.append(pull)
        #print pull
        nuis = initial_sort(pull)
        nuiss.append(nuis)
        # useful to compare with Asimov - not there yet
        #if len(versions) is 1:
        #    pull, corr = get_nuis_corr(fname, is_region, is_conditional, not is_asimov)
        #    pulls.append(pull)
        #    nuis = initial_sort(pull)
        #    nuiss.append(nuis)

    res = ROOT.std.vector('TGraph*')()
    for n in nuiss:
        res.push_back(n)
    ROOT.PU.mergeTGraphAxis(res, True)
    for n in nuiss:
        axiss.append(n.GetXaxis())
    
    
    # play with canvas of NP
    # reference:
    g_tmp = pulls[0].GetListOfPrimitives().At(1) 
    g_2s = g_tmp.GetListOfPrimitives().At(1) 
    g_1s = g_tmp.GetListOfPrimitives().At(2) 

    amin = axiss[0].GetXmin()
    amax = axiss[0].GetXmax()
    length = int(amax-amin)+1

    for i in range(length):
        g_2s.SetPoint(i,amin+i,-2)
        g_1s.SetPoint(i,amin+i,-1)
    for i in range(length):
        g_2s.SetPoint(2*length-i-1,amin+i,2)
        g_1s.SetPoint(2*length-i-1,amin+i,1)
    
    # then plot only interesting things
    res = reduce_all(nuiss, g_2s, g_1s, axiss, excludes=makeReducedDiagPlots.vector_TString("HiggsNorm"))
    nuiss = [n.Clone() for n in res[0]]
    axiss = [nuis.GetXaxis().Clone() for nuis in res[0]]
    nuis_plot(outplotdir, "all", *res, hmin=-3, hmax=3)
    
    # leptons
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString("EG_SCALE_ALL", "EG_RESOLUTION_ALL", "EL_EFF_ID_TotalCorrUncertainty",
                                                                    "EL_EFF_Iso_TotalCorrUncertainty", "EL_EFF_Reco_TotalCorrUncertainty",
                                                                    "EL_EFF_Trigger_TotalCorrUncertainty"))
    nuis_plot(outplotdir, "Egamma", *res, hmin=-3, hmax=3)
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString("MUON_ISO_SYS", "MUON_ISO_STAT", "MUON_EFF_SYS", "MUON_EFF_STAT",
                                                                    "MUON_EFF_TrigSystUncertainty", "MUON_EFF_TrigStatUncertainty",
                                                                    "MUONS_MS", "MUONS_ID", "MUONS_SCALE", "MUON_TTVA"))
    nuis_plot(outplotdir, "Muons", *res, hmin=-3, hmax=3)

    # b-tagging
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString("FT_EFF_Eigen_B"))
    nuis_plot(outplotdir, "BTagB", *res, hmin=-3, hmax=3)
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString("FT_EFF_Eigen_C"))
    nuis_plot(outplotdir, "BTagC", *res, hmin=-3, hmax=3)
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString("FT_EFF_Eigen_L"))
    nuis_plot(outplotdir, "BTagL", *res, hmin=-3, hmax=3)
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString("FT_EFF_extrapolation"))
    nuis_plot(outplotdir, "BTagExtra", *res, hmin=-3, hmax=3)

    # fat jet
    res = reduce_all(nuiss, g_2s, g_1s, axiss,
                     includes=makeReducedDiagPlots.vector_TString("FATJET", "JET_Hbb"))
    nuis_plot(outplotdir, "FatJet", *res, hmin=-3, hmax=3)


    # norm
    res = reduce_all(nuiss, g_2s, g_1s, axiss,
                     includes=makeReducedDiagPlots.vector_TString("XS"))
    nuis_plot(outplotdir, "Norm", *res, hmin=-3, hmax=3)
    
    #nuis_plot(outplotdir, "Rest", *res, hmin=-3, hmax=3)

    suspicious_NP = []
    for nuis,axis in zip(nuiss, axiss):
        suspicious_NP.extend(makeReducedDiagPlots.flag_suspicious_NP(nuis, axis, .5, .5))
    res = reduce_all(nuiss, g_2s, g_1s, axiss, includes=makeReducedDiagPlots.vector_TString(*suspicious_NP))
    nuis_plot(outplotdir, "Suspicious", *res, hmin=-3, hmax=3)


def nuis_plot(outplotdir, name, nuiss, yellow, green, h=None, hmin=-5, hmax=5):
    c = ROOT.TCanvas("c","c",1000,600)
    if hmin < -1:
        hmax = hmax + 4
        hmin = hmin - 1
    else:
        hmax = hmax + 1
    if h is not None:
        h.SetMinimum(hmin)
        h.SetMaximum(hmax)
        ya = h.GetYaxis()
        h.Draw()
        nuiss[0].Draw("pz")
    else:
        nuiss[0].SetMinimum(hmin)
        nuiss[0].SetMaximum(hmax)
        nuiss[0].Draw("pza")
        ya = nuiss[0].GetYaxis()
    nuiss[0].SetTitle("")
    ya.SetTitle("pull")
    ya.SetTitleOffset(.5)
    ya.SetTickLength(0)
    yellow.Draw("f")
    green.Draw("f")
    colors=[1,2,ROOT.kBlue-4,6,ROOT.kGray+1] #black, red, blue, pink, gray
    markerSize = 1
    lineWidth = 2
    if name is "all":
        markerSize = 0.5
        lineWidth = 1
    markerShift = 0.25

    for num,nuis in enumerate(nuiss):
        if num == 0:
            continue
        if num == 1:
            ROOT.PU.shiftTGraph(nuis, markerShift, True)
        if num == 2:
            ROOT.PU.shiftTGraph(nuis, -markerShift, True)
        if num > 2:
            print "WARNING: this is going to be ugly and hard to read"
            ROOT.PU.shiftTGraph(nuis, (num-1)*markerShift, True)
        col = colors[num]
        nuis.SetMarkerSize(markerSize)
        nuis.SetLineWidth(lineWidth)
        nuis.SetMarkerColor(col)
        nuis.SetLineColor(col)
        nuis.Draw("pz")
    nuiss[0].SetMarkerSize(markerSize)
    nuiss[0].SetLineWidth(lineWidth)
    nuiss[0].Draw("pz")
    nuiss[0].GetXaxis().LabelsOption("v")

    label = ROOT.TLatex()
    label.SetTextSize(0.035)
    offset = 0
    if  nuiss[0].GetXaxis().GetNbins() > 0:
        offset = 0.2 * nuiss[0].GetXaxis().GetNbins() * label.GetTextSize()
    if name is "all":
        label.SetTextSize(0.023)
    offset = 0.1 
    label.SetTextAngle(90)
    label.SetTextFont(42)
    for i in range(1, nuiss[0].GetXaxis().GetNbins() + 1):
        labelStr = ROOT.TString(nuiss[0].GetXaxis().GetBinLabel(i))
        nuiss[0].GetXaxis().SetBinLabel(i, "")
        xpos = nuiss[0].GetXaxis().GetBinCenter(i) + offset
        labelStr.ReplaceAll("extrapolation", "extrpol")
        label.DrawText(xpos, 2.2, labelStr.Data())

    label.SetTextSize(0.03)
    for num,nuis in enumerate(nuiss):
        if name is "all":
            continue
        xVals = nuis.GetX()
        yVals = nuis.GetY()
        yErr = nuis.GetEYhigh()
        for i in range(0, nuis.GetN()):
            yPos = hmin + 0.2
            labelStr = "%.2f #pm %.2f" % (abs(yVals[i]), yErr[i])
            label.DrawLatex(xVals[i], yPos, labelStr)
            if yVals[i] < 0:
                label.DrawLatex(xVals[i], yPos-0.11, "-")

    ROOT.gPad.SetTopMargin(ROOT.gPad.GetTopMargin()*0.15)
    ROOT.gPad.SetBottomMargin(ROOT.gPad.GetBottomMargin()*0.15)
    ROOT.gPad.SetLeftMargin(ROOT.gPad.GetLeftMargin()*.4)
    ROOT.gPad.SetRightMargin(ROOT.gPad.GetRightMargin()*.05)
    ROOT.gPad.Update()
    c.Print("{2}/NP_{0}.{1}".format(name, extension, outplotdir))

def reduce_all(nuiss, yellow, green, axiss, excludes=None, includes=None):
    new_nuis = []
    for nuis,axis in zip(nuiss, axiss):
        if excludes is not None:
            new_nuis.append(ROOT.PU.reduceTGraphAxisExclude(nuis, axis, True, excludes))
        elif includes is not None:
            new_nuis.append(ROOT.PU.reduceTGraphAxisInclude(nuis, axis, True, includes))
    max_axis = new_nuis[0].GetXaxis().GetXmax()
    new_y = yellow.Clone(yellow.GetName()+"_reduced")
    new_g = green.Clone(yellow.GetName()+"_reduced")
    ROOT.PU.removeTGraphPointsAbove(new_y, True, max_axis)
    ROOT.PU.removeTGraphPointsAbove(new_g, True, max_axis)
    return [new_nuis, new_y, new_g]

def get_nuis_corr(tfile, is_region, is_conditional, poi=1):
    f = ROOT.TFile.Open(tfile)

    mu = str(poi)
    stub = is_region

    if is_region == "Global": 
        stub = stub + "Fit"
        f.cd("PlotsAfterGlobalFit")
    else:
        f.cd("PlotsAfterFitOnSubChannel")

    if is_conditional:
        if mu == "1":
            gDirectory.cd("conditionnal_MuIsEqualTo_1")
        if mu == "0": 
            gDirectory.cd("conditionnal_MuIsEqualTo_0")
        if is_region == "Global":
            p_nuis = gDirectory.Get("can_NuisPara_"+stub+"_conditionnal_mu"+mu)
            p_corr = gDirectory.Get("can_CorrMatrix_"+stub+"_conditionnal_mu"+mu)
        else:
            gDirectory.cd(stub)
            p_nuis = gDirectory.Get("can_NuisPara_"+stub+"_conditionnal_mu"+mu)
            p_corr = gDirectory.Get("can_CorrMatrix_"+stub+"_conditionnal_mu"+mu)
    else:
        if is_region == "Global":
            gDirectory.cd("unconditionnal")
        else:
            gDirectory.cd("unconditionnal/"+stub)
        p_nuis = gDirectory.Get("can_NuisPara_"+stub+"_unconditionnal_mu"+mu)
        p_corr = gDirectory.Get("can_CorrMatrix_"+stub+"_unconditionnal_mu"+mu)

    return p_nuis, p_corr


def initial_sort(can_pulls):
    p = can_pulls.GetListOfPrimitives().At(1)
    h = p.GetListOfPrimitives().At(0) 
    axis = h.GetXaxis()
    index = 0
    for i in range(0,len(p.GetListOfPrimitives())):#quick fix
    	if "TGraphAsymmErrors" in str(type(p.GetListOfPrimitives().At(i))): 
	        index = i
	        break
    nuis = p.GetListOfPrimitives().At(index) 
    # function pointers not supported in PyROOT... have to workaround
    gROOT.ProcessLine("TGraph* n = (TGraph*)"+str(ROOT.AddressOf(nuis)[0]))
    gROOT.ProcessLine("TAxis* a = (TAxis*)"+str(ROOT.AddressOf(axis)[0]))
    gROOT.ProcessLine("PU::sortTGraphAxis(n, a, true, PU::comp_sysNames)")
    ROOT.PU.setTGraphAxis(nuis, axis, True)
    return nuis


if __name__ == "__main__":
    basedir_fcc = sys.argv[1]+"/{0}" 
    is_region = sys.argv[2]
    is_conditionnal = False
    is_conditionnal = int(sys.argv[3])
    mu_conditionnal = int(sys.argv[4])

    outplotdir = sys.argv[5]

    versions = []
    for i in range(6, len(sys.argv)):
        versions.append(sys.argv[i])

    main(versions, basedir_fcc, is_region, is_conditionnal, mu_conditionnal, outplotdir)


