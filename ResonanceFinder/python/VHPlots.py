#!/usr/env python
from sys import argv, stdout
myargv = argv[:]

import ROOT
from ROOT import RooFit as RF

from math import sqrt, log, exp
from array import array
from RooFitUtil import *

from VHRegionsAndChannels import formatProcessName, formatRegionNameROOT, setStyle, cmpBkg

ROOT.RooMsgService.instance().setGlobalKillBelow(RF.WARNING)
ROOT.gROOT.ProcessLine(".L AtlasStyle.C+")
ROOT.SetAtlasStyle()


def plots(ws, rfr, prefix="", binHistFile=None):

    mc = ws.obj("ModelConfig")
    data = ws.data("obsData")
    combPdf = ws.pdf("combPdf")
    if not combPdf:
        combPdf = ws.pdf("simPdf")

    channelCat = combPdf.indexCat()
    channelCatName = channelCat.GetName()

    d = {}
    for chan in categories(channelCat):
        print "inspecting channel", chan

        chanName = chan.GetName()
        d[chanName] = {}

        pdftmp = combPdf.getPdf(chanName)
        datatmp = data.reduce("{0}=={1}::{2}".format(channelCatName,channelCatName,chanName))

        obs = pdftmp.getObservables(mc.GetObservables()).first()

        # do we have a file to rebin from?
        binHist = None
        if binHistFile:
            for k in binHistFile.GetListOfKeys():
                if k.GetName() in chanName:
                    binHist = binHistFile.Get(k.GetName())
                    break
                else:
                    continue

            if not binHist:
                print "cannot find appropriate rebin histogram for channel", \
                        chanName
                exit(-1)


        csigs = []
        cbkgs = []
        cbkgstemp = []
        for c in components(pdftmp, chanName):
            compname = c.GetName()
            print "looking at component", compname

            if "HVT" in compname:
                csigs.append(c)
            else:
                print "adding to bkgs."
                cbkgs.append(c)

        if not csigs:
            print "unable to find signal histogram for channel", \
                    chanName, "component", compname
            exit(-1)


        hbkgs = map(lambda c: compToHist(c, obs, rfr), cbkgs)
        hbkgs = map(setStyle, hbkgs)
        map(lambda h: h.SetName(prefix + h.GetName()), hbkgs)
        hbkgs.sort(cmpBkg)
        hbkgs.reverse()

        cbkg = sumComponents("bkgsum_"+chanName,
                "bkgsum_"+chanName, cbkgs)
        hbkg = setStyle(compToHist(cbkg, obs, rfr))

        csig = sumComponents("sigsum_"+chanName,
                "sigsum_"+chanName, csigs)
        hsig = setStyle(compToHist(csig, obs, rfr))

        if binHist:
            hbkg = scaleBins(hbkg, binHist)
        else:
            hbkg = scaleBins(hbkg, hbkg)

        hbkg.SetName(prefix + hbkg.GetName())


        hdata = datatmp.plotOn(obs.frame(),
                RF.DataError(ROOT.RooAbsData.Poisson)).getHist()
        hdata.SetName(prefix+"_data_"+chanName)

        hbkgs = map(lambda h: scaleBins(h, hbkg), hbkgs)
        hsig = scaleBins(hsig, hbkg)
        hdata = scaleBinsTG(hdata, hbkg)

        d[chanName]["hsig"] = hsig
        d[chanName]["hbkg"] = hbkg
        d[chanName]["hbkgs"] = hbkgs
        d[chanName]["hdata"] = hdata

    return d



def main(outfolder, wsfname, wsname, fitfname, binfname=None):
    fws = ROOT.TFile(wsfname)
    ws = fws.Get(wsname)

    f = ROOT.TFile.Open(fitfname)

    rfr = f.Get("fitresult_combPdf_obsData")
    if not rfr:
        rfr = f.Get("fitresult_simPdf_obsData")

    rfr = rfr.Clone()

    mc = ws.obj("ModelConfig")
    np = ROOT.RooArgSet(mc.GetNuisanceParameters())

    pois = mc.GetParametersOfInterest()
    for n in args(pois):
        np.add(n)

    # don't float mu in the plots
    np["mu"].setVal(5000)
    np["mu"].setError(0.0)

    ws.saveSnapshot("vars_initial", np)

    # add all const parameters of the RooFitResult to the floating
    # ones
    fpf = rfr.floatParsFinal().Clone()
    fpf.add(rfr.constPars())

    """
    iter = fpf.iterator()
    i = iter.Next()
    while i != None:
        name = i.GetName()
        if name.startswith("gamma_") \
                or name.startswith("alpha_") \
                or name.startswith("norm_"):

            n = np.find(name)

            if n:
                n.setVal(i.getVal())
                n.setError((i.getErrorHi() + i.getErrorLo()) / 2)
            else:
                print "WARNING: parameter", n, "not found in workspace!!!"
        i = iter.Next()
    """

    ROOT.RooAbsCollection.__assign__(np, fpf)

    # don't float mu in the plots
    np["mu"].setVal(5000)
    np["mu"].setError(0.0)

    ws.saveSnapshot("vars_final", np)



    if binfname != None:
        binf = ROOT.TFile.Open(binfname)
    else:
        binf = None


    ws.loadSnapshot("vars_final")
    dfinal = plots(ws, rfr, prefix="final", binHistFile=binf)

    ws.loadSnapshot("vars_initial")
    dinit = plots(ws, rfr, prefix="init", binHistFile=binf)

    c = ROOT.TCanvas("c", "c", 1000, 1000)
    for reg in dinit:
        c.cd()
        c.Clear()
        pmain = ROOT.TPad("pmain", "pmain", 0, 0.25, 1, 1)
        pmain.Draw()
        pmain.SetBottomMargin(0.02)
        pmain.SetLeftMargin(0.15)
        pmain.SetRightMargin(0.075)

        pratio = ROOT.TPad("pratio", "pratio", 0, 0, 1, 0.25)
        pratio.Draw()
        pratio.SetTopMargin(0.06)
        pratio.SetBottomMargin(0.4)
        pratio.SetLeftMargin(0.15)
        pratio.SetRightMargin(0.075)

        pmain.cd()
        bkgs = ROOT.THStack("s", "s")

        prefit = setStyle(dinit[reg]["hbkg"])
        signal = setStyle(dfinal[reg]["hsig"])
        uncert = setStyle(dfinal[reg]["hbkg"])
        map(lambda h: bkgs.Add(setStyle(h)), dfinal[reg]["hbkgs"])

        prefit.SetTitle("pre-fit")
        prefit.SetFillStyle(0)
        prefit.SetLineColor(ROOT.kBlue)
        prefit.SetLineWidth(4)
        prefit.SetLineStyle(2)

        #signal.Scale(10)
        #signal.SetTitle(signal.GetTitle() + " x 10")
        signal.SetTitle(signal.GetTitle())
        
        uncert.SetFillStyle(3004)
        name = uncert.GetName().replace("finalhist_bkgsum_", "")
        #name = name[:name.find("_file")]
               
        data = setStyle(dinit[reg]["hdata"])
        data.SetMarkerStyle(20)
        data.SetMarkerSize(1.75)



        bkgs.Draw("hist")
        allhists = [uncert, prefit, data, signal] + list(bkgs.GetHists())

        if "mVH" in name:
            map(lambda h: h.Scale(100), allhists)
            bkgs.GetYaxis().SetTitle("Events / 100 GeV")
        elif "mJet" in name:
            map(lambda h: h.Scale(25), allhists)
            bkgs.GetYaxis().SetTitle("Events / 25 GeV")
        elif "pTV" in name:
            map(lambda h: h.Scale(50), allhists)
            bkgs.GetYaxis().SetTitle("Events / 50 GeV")
        elif "mBB" in name:
            map(lambda h: h.Scale(50), allhists)
            bkgs.GetYaxis().SetTitle("Events / 10 GeV")


        maxHist = max(maxWithUncert(uncert), maxWithUncert(data))
        maxHist = max(maxHist, prefit.GetMaximum())

        minHist = minH(data, maxHist)*0.3
        bkgs.GetYaxis().SetTitleSize(0.05)
        bkgs.GetYaxis().SetTitleFont(42)

        bkgs.SetMaximum(maxHist*1.75)
        bkgs.GetXaxis().SetLabelSize(0)
        bkgs.GetXaxis().SetTitle("")
#        xax = bkgs.GetXaxis()
#        xax.SetLabelSize(0.05)
#        xax.SetTitleSize(0.05)
#        xax.SetTitleFont(42)
#
#
#
#        if "mJet" in name:
#            xax.SetTitle("m_{jet} [GeV]")
#        elif "mVH" in name:
#            if "vv" in name:
#                xax.SetTitle("m_{T,VH} [GeV]")
#            else:
#                xax.SetTitle("m_{VH} [GeV]")
#

        #bkgs.GetXaxis().SetRangeUser(70,150)
        bkgs.Draw("hist")
        bkgs.GetXaxis().SetNdivisions(505)

        #prefit.GetXaxis().SetRangeUser(70,150)
        prefit.Draw("histsame")
        #uncert.GetXaxis().SetRangeUser(70,150)
        uncert.Draw("e2same")
        signal.Draw("histsame")
        #data.GetXaxis().SetRangeUser(70,150)
        data.Draw("esame")   ####

        
        histopts = map(lambda h: (h, "F"), bkgs.GetHists())
        histopts.reverse()

        histopts = [(data, "EP"), (signal, "L")] + histopts \
                   + [(uncert, "F"), (prefit, "L")]

        leg = buildLegend(0.65, 0.3, 0.85, 0.9, histopts)
        leg.Draw()

        drawAtlasLabel(0.2, 0.85, suffix="Internal")
        # drawAtlasLabel(0.2, 0.85, suffix="")
        drawLumi(0.2, 0.76)

        rn = formatRegionNameROOT(name).split(", ", 1)
        drawText(0.2, 0.67, rn[0], size=0.04)
        if len(rn) > 1:
            drawText(0.2, 0.61, rn[1], size=0.04)


        pratio.cd()

        #uncert.GetXaxis().SetRangeUser(70,150)
        #data.GetXaxis().SetRangeUser(70,150)
        hbkg = stripUncert(uncert)
        #hbkg.GetXaxis().SetRangeUser(70,150)
        uncertratio = uncert.Clone()
        uncertratio.Divide(hbkg)

        dataratio = data.Clone()
        dataratio.Divide(hbkg)
        #dataratio.GetXaxis().SetRangeUser(70,150)
        #uncertratio.GetXaxis().SetRangeUser(70,150)

        uncertratio.Draw("e2")
        xax = uncertratio.GetXaxis()
        xax.SetNdivisions(505)
        xax.SetLabelSize(0.15)
        xax.SetTitleSize(0.20)
        xax.SetTitleOffset(0.8)
        xax.SetTickLength(0.06)

        if "mJet" in name:
            xax.SetTitle("m_{jet} [GeV]")
        elif "pTV" in name:
            xax.SetTitle("p_{T,V} [GeV]")
        #elif "mBB" in name:
            #xax.SetTitle("m_{BB} [GeV]")
        elif "mVH" in name:
            if "vv" in name:
                xax.SetTitle("m_{T,VH} [GeV]")
            else:
                xax.SetTitle("m_{VH} [GeV]")

        yax = uncertratio.GetYaxis()
        yax.SetRangeUser(0, 2.0)
        yax.SetTitle("Data / Pred")
        yax.SetLabelSize(0.15)
        yax.SetTitleSize(0.15)
        yax.SetTitleOffset(0.45)
        yax.SetNdivisions(505)
        ratioLine = ROOT.TLine(dataratio.GetBinLowEdge(1), 1,
                               dataratio.GetBinLowEdge(dataratio.GetNbinsX()+1), 1)
        ratioLine.SetLineColor(ROOT.kBlack)
        ratioLine.SetLineWidth(2)
        ratioLine.Draw("same")
        for iBin in xrange(1, dataratio.GetNbinsX()+1):
            if (dataratio.GetBinContent(iBin)) == 0 :
                dataratio.SetBinContent(iBin,-9999)

        dataratio.Draw("e0same") ####

        
        arrows = ratioArrows(dataratio, 2.0, 0.0)
        if arrows:
            arrows.Draw("epsame")



        pmain.SetLogy(False)

        #c.SaveAs(outfolder + "/" + name + ".png")
        c.SaveAs(outfolder + "/" + name + ".eps")
        c.SaveAs(outfolder + "/" + name + ".pdf")        
        #c.SaveAs(outfolder + "/" + name + ".C")

        pmain.cd()
        bkgs.SetMinimum(minHist)
        bkgs.SetMaximum(exp((log(maxHist) - log(minHist))*1.5))

        bkgs.Draw("hist")
        prefit.Draw("histsame")
        uncert.Draw("e2same")
        signal.Draw("histsame")
        data.Draw("esame")  ####
        leg.Draw()

        drawAtlasLabel(0.2, 0.85,suffix="Internal")
        drawLumi(0.2, 0.76)
        drawText(0.2, 0.67, rn[0], size=0.04)
        if len(rn) > 1:
            drawText(0.2, 0.61, rn[1], size=0.04)

        pmain.SetLogy(True)
        #pmain.SetLogx(True)
        #pratio.SetLogx(True)

        #c.SaveAs(outfolder + "/" + name + "_log.png")
        c.SaveAs(outfolder + "/" + name + "_log.eps")
        c.SaveAs(outfolder + "/" + name + "_log.pdf")       
        #c.SaveAs(outfolder + "/" + name + "_log.C")


        continue

    return


if __name__ == "__main__":
    main(argv[1], argv[2], argv[3],
            argv[4], argv[5] if len(argv) > 5 else None)
