#!/usr/env python
from sys import argv, stdout
myargv = argv[:]

import ROOT
from ROOT import RooFit as RF

from math import sqrt, log, exp
from array import array
from RooFitUtil import *

from VHRegionsAndChannels import formatProcessName, formatRegionNameROOT, setStyle, cmpBkg

ROOT.RooMsgService.instance().setGlobalKillBelow(RF.WARNING)
ROOT.gROOT.ProcessLine(".L AtlasStyle.C+")
ROOT.SetAtlasStyle()


def plots(ws, rfr, prefix="", binHistFile=None):

    mc = ws.obj("ModelConfig")
    data = ws.data("obsData")
    combPdf = ws.pdf("combPdf")
    if not combPdf:
        combPdf = ws.pdf("simPdf")

    channelCat = combPdf.indexCat()
    channelCatName = channelCat.GetName()

    d = {}
    for chan in categories(channelCat):

        chanName = chan.GetName()

        d[chanName] = {}

        pdftmp = combPdf.getPdf(chanName)
        datatmp = data.reduce("{0}=={1}::{2}".format(channelCatName,channelCatName,chanName))

        obs = pdftmp.getObservables(mc.GetObservables()).first()

        stepsize = obs.getBinning().averageBinWidth()

        obs_set = ROOT.RooArgSet(obs)

        # do we have a file to rebin from?
        binHist = None
        if binHistFile:
            for k in binHistFile.GetListOfKeys():
                if k.GetName() in chanName:
                    binHist = binHistFile.Get(k.GetName())
                    break
                else:
                    continue

            if not binHist:
                print "cannot find appropriate rebin histogram for channel", \
                        chanName
                exit(-1)


        csigs = []
        cbkgs = []
        cbkgstemp = []
 
        for c in components(pdftmp, chanName):
            compname = c.GetName()
            
            if "VH" in compname:
                continue

            if "TTbar" in compname:
                cbkgstemp.append(c)
                for c1 in components(pdftmp, chanName):
                    if "STop" in c1.GetName():
                        cbkgstemp.append(c1)
                c = sumComponents("L_x_TTbar"+chanName,
                                  "L_x_TTbar"+chanName, cbkgstemp)  
                
            
            if "qqWlvH125" in compname:
                continue
            if "SMZh" in compname:
                continue
            if "STop" in compname:
                continue    
            if "HVT" in compname:
                csigs.append(c)
            else:
                cbkgs.append(c)

        if not csigs:
            print "unable to find signal histogram for channel", \
                    chanName, "component", compname
            exit(-1)

        csig = sumComponents("sigsum_"+chanName,
                "sigsum_"+chanName, cbkgs)    

        hbkgs = map(lambda c: compToHist(c, obs, rfr), cbkgs)
        hbkgs = map(setStyle, hbkgs)
        map(lambda h: h.SetName(prefix + h.GetName()), hbkgs)
        hbkgs.sort(cmpBkg)
        hbkgs.reverse()
        
        cbkg = sumComponents("bkgsum_"+chanName,
                "bkgsum_"+chanName, cbkgs)

        hbkg = setStyle(compToHist(cbkg, obs, rfr))

        csig = sumComponents("sigsum_"+chanName,
                "sigsum_"+chanName, csigs)
        hsig = setStyle(compToHist(csig, obs, rfr))

        if binHist:
            hbkg = scaleBins(hbkg, binHist)
        else:
            hbkg = scaleBins(hbkg, hbkg)

        hbkg.SetName(prefix + hbkg.GetName())


        hdata = datatmp.plotOn(obs.frame(),
                RF.DataError(ROOT.RooAbsData.Poisson)).getHist()
        hdata.SetName(prefix+"_data_"+chanName)

        hbkgs = map(lambda h: scaleBins(h, hbkg), hbkgs)
        hsig = scaleBins(hsig, hbkg)
        hdata = scaleBinsTG(hdata, hbkg)

        if "mVH" in hbkg.GetName():
            def f(h):
                return forceXmax(h, 5e3)
        else:
            def f(h):
                return forceXmax(h, 300)

        d[chanName]["hsig"] = f(hsig)
        d[chanName]["hbkg"] = f(hbkg)
        d[chanName]["hbkgs"] = map(f, hbkgs)
        d[chanName]["hdata"] = f(hdata)

    return d



def main(outfolder, wsfname, wsname, fitfname, binfname=None):

    fws = ROOT.TFile(wsfname)
    ws = fws.Get(wsname)

    f = ROOT.TFile.Open(fitfname)
    rfr = f.Get("fitresult_combPdf_obsData").Clone()

    mc = ws.obj("ModelConfig")
    np = ROOT.RooArgSet(mc.GetNuisanceParameters())

    pois = mc.GetParametersOfInterest()
    for n in args(pois):
        np.add(n)

    # don't float mu in the plots
    np["mu"].setVal(1)
    np["mu"].setError(0.0)

    ws.saveSnapshot("vars_initial", np)


    # add all const parameters of the RooFitResult to the floating
    # ones
    fpf = rfr.floatParsFinal().Clone()
    fpf.add(rfr.constPars())

    iter = fpf.iterator()
    i = iter.Next()
    while i != None:
        n = np.find("alpha_" + i.GetName())
        if n:
            n.setVal(i.getVal())
            n.setError((i.getErrorHi() + i.getErrorLo()) / 2)
        else:
            print "WARNING: parameter", i.GetName(), "not found in workspace!!!"

        i = iter.Next()

    # don't float mu in the plots
    np["mu"].setVal(1)
    np["mu"].setError(0.0)


    ws.saveSnapshot("vars_final", np)

    if binfname != None:
        binf = ROOT.TFile.Open(binfname)
    else:
        binf = None


    ws.loadSnapshot("vars_initial")
    dinit = plots(ws, rfr, prefix="init", binHistFile=binf)

    ws.loadSnapshot("vars_final")
    dfinal = plots(ws, rfr, prefix="final", binHistFile=binf)


    d = {}
    for reg in dfinal:

        # skip over CRs
        if "0addtag" not in reg:
            continue

        if not d:
            # TODO
            # should deepcopy?
            d = dfinal[reg]
            d["prefit"] = dinit[reg]["hbkg"]
        else:
            d["hdata"].Add(dfinal[reg]["hdata"])
            d["hsig"].Add(dfinal[reg]["hsig"])
            d["hbkg"].Add(dfinal[reg]["hbkg"])
            d["prefit"].Add(dinit[reg]["hbkg"])
            for (h, h1) in zip(d["hbkgs"], dfinal[reg]["hbkgs"]):
                h.Add(h1)

        continue

    c = ROOT.TCanvas("c", "c", 1000, 1000)
    plot(c, d, outfolder)


def plot(c, d, outfolder):
    c.cd()
    c.Clear()
    pmain = ROOT.TPad("pmain", "pmain", 0, 0.25, 1, 1)
    pmain.Draw()
    pmain.SetBottomMargin(0.02)
    pmain.SetLeftMargin(0.15)
    pmain.SetRightMargin(0.075)

    pratio = ROOT.TPad("pratio", "pratio", 0, 0, 1, 0.25)
    pratio.Draw()
    pratio.SetTopMargin(0.06)
    pratio.SetBottomMargin(0.4)
    pratio.SetLeftMargin(0.15)
    pratio.SetRightMargin(0.075)

    pmain.cd()
    bkgs = ROOT.THStack("s", "s")

    prefit = setStyle(d["prefit"])
    signal = setStyle(d["hsig"])
    uncert = setStyle(d["hbkg"])
    map(lambda h: bkgs.Add(setStyle(h)), d["hbkgs"])

    prefit.SetTitle("pre-fit")
    prefit.SetFillStyle(0)
    prefit.SetLineColor(ROOT.kBlue)
    prefit.SetLineWidth(4)
    prefit.SetLineStyle(2)

    signal.Scale(200)
    signal.SetTitle(signal.GetTitle() + " x 200")

    uncert.SetFillStyle(3004)
    name = uncert.GetName().replace("finalhist_bkgsum_", "")
    name = name[:name.find("_")] + "_mJet"

    data = setStyle(d["hdata"])
    data.SetMarkerStyle(20)
    data.SetMarkerSize(1.75)

    bkgs.Draw("hist")
    allhists = [uncert, prefit, data, signal] + list(bkgs.GetHists())

    if "mVH" in name:
        map(lambda h: h.Scale(100), allhists)
        bkgs.GetYaxis().SetTitle("Events / 100 GeV")
    elif "mJet" in name:
        map(lambda h: h.Scale(25), allhists)
        bkgs.GetYaxis().SetTitle("Events / 25 GeV")

    maxHist = max(maxWithUncert(uncert), maxWithUncert(data))
    maxHist = max(maxHist, prefit.GetMaximum())

    minHist = minH(data, maxHist)*0.3
    bkgs.GetYaxis().SetTitleSize(0.05)
    bkgs.GetYaxis().SetTitleFont(42)
  
    bkgs.SetMaximum(maxHist*1.75)
    bkgs.GetXaxis().SetLabelSize(0)
    bkgs.GetXaxis().SetTitle("")

    bkgs.Draw("hist")
    bkgs.GetXaxis().SetNdivisions(505)


    prefit.Draw("histsame")
    uncert.Draw("e2same")
    signal.Draw("histsame")
    data.Draw("esame")
    
    histopts = map(lambda h: (h, "F"), bkgs.GetHists())
    histopts.reverse()

    histopts = [(data, "EP"), (signal, "L")] + histopts \
               + [(uncert, "F"), (prefit, "L")]

    leg = buildLegend(0.65, 0.28, 0.85, 0.9, histopts)
    leg.Draw()

    #drawAtlasLabel(0.2, 0.85, suffix="")
    drawAtlasLabel(0.2, 0.85, suffix="Preliminary")
    # drawAtlasLabel(0.2, 0.85, suffix="")
    drawLumi(0.2, 0.76)

    rn = formatRegionNameROOT(uncert.GetName()).split(", ")
    drawText(0.2, 0.67, rn[1] + ", #geq 1 b-tag", size=0.05)

    pratio.cd()

    hbkg = stripUncert(uncert)
    uncertratio = uncert.Clone()
    uncertratio.Divide(hbkg)

    dataratio = data.Clone()
    dataratio.Divide(hbkg)

    uncertratio.Draw("e2")
    xax = uncertratio.GetXaxis()
    xax.SetNdivisions(505)
    xax.SetLabelSize(0.15)
    xax.SetTitleSize(0.20)
    xax.SetTitleOffset(0.8)
    xax.SetTickLength(0.06)

    if "mJet" in name:
        xax.SetTitle("m_{jet} [GeV]")
    elif "mVH" in name:
        if "vv" in name:
            xax.SetTitle("m_{T,VH} [GeV]")
        else:
            xax.SetTitle("m_{VH} [GeV]")

    yax = uncertratio.GetYaxis()
    yax.SetRangeUser(0, 2.0)
    yax.SetTitle("Data / Pred")
    yax.SetLabelSize(0.15)
    yax.SetTitleSize(0.15)
    yax.SetTitleOffset(0.45)
    yax.SetNdivisions(505)
    ratioLine = ROOT.TLine(dataratio.GetBinLowEdge(1), 1,
                           dataratio.GetBinLowEdge(dataratio.GetNbinsX()+1), 1)
    ratioLine.SetLineColor(ROOT.kBlack)
    ratioLine.SetLineWidth(2)
    ratioLine.Draw("same")
    for iBin in xrange(1, dataratio.GetNbinsX()+1):
        if (dataratio.GetBinContent(iBin)) == 0 :
            dataratio.SetBinContent(iBin,-9999)
            
    dataratio.Draw("e0same")
#    dataratio.Draw("esame")

    arrows = ratioArrows(dataratio, 2.0, 0.0)
    if arrows:
        arrows.Draw("epsame")



    pmain.SetLogy(False)

    c.SaveAs(outfolder + "/" + name + ".png")
    c.SaveAs(outfolder + "/" + name + ".eps")
    c.SaveAs(outfolder + "/" + name + ".C")

    pmain.cd()
    bkgs.SetMinimum(minHist)
    bkgs.SetMaximum(exp((log(maxHist) - log(minHist))*1.5))

    bkgs.Draw("hist")
    prefit.Draw("histsame")
    uncert.Draw("e2same")
    signal.Draw("histsame")
    data.Draw("esame")
    leg.Draw()

    drawAtlasLabel(0.2, 0.85)
    drawLumi(0.2, 0.76)
    drawText(0.2, 0.67, rn[1] + ", >= 1 b-tag", size=0.04)

    pmain.SetLogy(True)

    c.SaveAs(outfolder + "/" + name + "_log.png")
    c.SaveAs(outfolder + "/" + name + "_log.eps")
    c.SaveAs(outfolder + "/" + name + "_log.C")

    return


if __name__ == "__main__":
    main(argv[1], argv[2], argv[3],
            argv[4], argv[5] if len(argv) > 5 else None)
