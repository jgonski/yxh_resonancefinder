# an example plotter for the lvqq analysis
# Valerio Ippolito - Harvard University

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))

from ROOT import RF as RF
from ROOT import PullPlotter as PullPlotter


if __name__ == '__main__':
  
  releaseDir = './test_v4'
  analysis = 'VVlvqq_HVT'
  outputWSTag = 'wtest01'

  releaseDir = './1ifb'
  analysis = 'VVlvqq_HVT_mc15'
  inputListTag = 'itest01'
  outputWSTag = 'wtest01'


  # retrieve results
  results = RF.StatisticalResultsCollection('lvqqtest')
  filename = '{releaseDir}/{analysis}/ws/results_{analysis}_{outputWSTag}.root'.format(releaseDir=releaseDir, analysis=analysis, outputWSTag=outputWSTag)
  results.retrieve(filename)


  # plot
  ROOT.gROOT.ProcessLine('.L AtlasStyle.C+')
  ROOT.SetAtlasStyle()
  plotter = RF.Plotter('lvqqtest', '.')
  plotter.setVarName('m_{HVT}')
  plotter.setVarUnit('GeV')

  k=1;
  BrZqq = 0.6991 ;
  BrWlv = (0.1075+0.1057+0.1125)
  Br=BrZqq*BrWlv ;

  plotter.setSpec(2600, Br, 0.0008401*k) # note the 1000x factor
  plotter.setSpec(2400, Br, 0.0013530*k)
  plotter.setSpec(2200, Br, 0.002225*k)
  plotter.setSpec(2000, Br, 0.003748*k)
  plotter.setSpec(1900, Br, 0.004905*k)
  plotter.setSpec(1800, Br, 0.006494*k)
  plotter.setSpec(1700, Br, 0.00866*k)
  plotter.setSpec(1600, Br, 0.01172*k)
  plotter.setSpec(1400, Br, 0.02216*k)
  plotter.setSpec(1000, Br, 0.1008*k)

  plotter.setOutputFormat(RF.Plotter.root)
  plotter.process(results)




