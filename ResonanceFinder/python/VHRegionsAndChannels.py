# cpollard@cern.ch
# for formatting channel and region names

# use formatProcessName() and formatRegionName()

import re
import ROOT

# name : color, fill style, line color, line width, line style
styles = { "$t\\bar{t}$": ("t#bar{t}",      ROOT.kOrange,   1001, ROOT.kBlack, 1, 1),
           "single top":  ("single top",    ROOT.kOrange-1, 1001, ROOT.kBlack, 1, 1),
           "diboson":     ("diboson",       ROOT.kGray+1,   1001, ROOT.kBlack, 1, 1),
           "$W+hf$":       ("W+hf",           ROOT.kGreen+3,  1001, ROOT.kBlack, 1, 1),
           "$W+hl$":       ("W+hl",           ROOT.kGreen+1,  1001, ROOT.kBlack, 1, 1),
           "$W+l$":       ("W+l",           ROOT.kGreen-9,  1001, ROOT.kBlack, 1, 1),
           "$Z+hf$":       ("Z+hf",           ROOT.kAzure-2,  1001, ROOT.kBlack, 1, 1),
           "$Z+hl$":       ("Z+hl",           ROOT.kAzure-4,  1001, ROOT.kBlack, 1, 1),
           "$Z+l$":       ("Z+l",           ROOT.kAzure-9,  1001, ROOT.kBlack, 1, 1),
           "SM VH":       ("SM VH",         ROOT.kViolet-9, 1001, ROOT.kBlack, 1, 1),
           "ttH":       ("ttH",         ROOT.kOrange+5, 1001, ROOT.kBlack, 1, 1),
           "ttV":       ("ttV",         ROOT.kOrange+6, 1001, ROOT.kBlack, 1, 1),
           "multijet":       ("multijet",   ROOT.kMagenta+4, 1001, ROOT.kBlack, 1, 1),
           "HVT":         ("1.5 TeV HVT",     ROOT.kRed,      0,    ROOT.kRed+1, 4, 9),
           "data":        ("data",          ROOT.kBlack,    0,    ROOT.kBlack, 2, 1),
           "bkgsum":      ("sumbkg",        ROOT.kBlue,     0,    ROOT.kBlue,  4, 2),
           "bkgsum":      ("uncertainty",   ROOT.kBlack,    0, ROOT.kBlack,  0, 0)
         }

bkgOrder = dict(map(lambda (x, y): (y, x),
        enumerate( [ "t#bar{t}"
                   , "single top"
                   , "diboson"
                   , "W+l"
                   , "W+hl"
                   , "W+hf"
                   , "Z+l"
                   , "Z+hl"
                   , "Z+hf"
                   , "SM VH"
                   , "multijet"
                   , "ttV"
                   , "ttH"
                   ]
                 )))

def cmpBkg(h1, h2):
    return cmp(bkgOrder[h1.GetTitle()], bkgOrder[h2.GetTitle()])


def setStyle(h):
    s = formatProcessName(h.GetName())
    (t, c, fs, lc, lw, ls) = styles[s]
    h.SetTitle(t)
    h.SetFillColor(c)
    h.SetFillStyle(fs)
    h.SetLineColor(lc)
    h.SetLineWidth(lw)
    h.SetLineStyle(ls)
    h.SetMarkerStyle(0)

    return h

lBkgProcessConvList = map(
        lambda (s, v): (re.compile(s), v),
        [
            ("TTbar", "$t\\bar{t}$"),
            ("ttbar", "$t\\bar{t}$"),
            ("Stop", "single top"),
            ("STop", "single top"),
            ("Diboson", "diboson"),
            ("VV", "diboson"),
            ("Wjets", "$W+$jets"),
            ("SMVH", "SM VH"),
            ("SMZh", "SM VH"),
            ("H125", "SM VH"),
            ("VH125", "SM VH"),
            ("W\\+Jets", "$W+$Jets"),
            ("Wb", "$W+hf$"),
            ("Wc", "$W+hl$"),
            ("Wl", "$W+l$"),
            ("Whf", "$W+hf$"),
            ("Wblcl", "$W+hl$"),
            ("Wclbl", "$W+hl$"),
            ("Zjets", "$Z+$jets"),
            ("Z\\+Jets", "$Z+$Jets"),
            ("Zb", "$Z+hf$"),
            ("Zc", "$Z+hl$"),
            ("Zhf", "$Z+hf$"),
            ("Zblcl", "$Z+hl$"),
            ("Zclbl", "$Z+hl$"),
            ("Zl", "$Z+l$"),
            ("multijet", "multijet"),
            ("MJ", "multijet"),
            ("ttH", "ttH"),
            ("ttV","ttV"),
            ("HVT", "HVT"),
            ("sigsum", "HVT"),
            ("bkgsum", "bkgsum"),
            ("data", "data")
        ]
        )

lSigProcessConvList = map(
        lambda x:
                (re.compile("HVT.*%s" % x), "%.1f TeV HVT" % (x/1000.0)),
                # this needs to be reversed to avoid matching a 500
                # GeV HVT to a 2500 GeV HVT.
                reversed(range(500, 5000, 100))
        )


# should be read from config somewhere?
lConvProcessName = lBkgProcessConvList + lSigProcessConvList


lMHRegions = [ ("0_500ptv_SR", "75 GeV < m_{H} < 145 GeV, p_{T,V} < 500"),
               ("500ptv_SR", "75 GeV < m_{H} < 145 GeV, p_{T,V} > 500"),
               ("SR", "75 GeV < m_{H} < 145 GeV"),
               ("mBBcr", "m_{H} < 75 GeV || m_{H} > 145 GeV"),
               ("mBBincl", "m_{BB} inclusive"),
               ("topemucr", "t#bar{t} CR")
             ]
       # ("lowmBBcr", "$m_{H} < 75~{\\rm GeV}$"),
       # ("highmBBcr", "$m_{H} > 145~{\\rm GeV}$"),
       
lTagRegions = [
        ("1tag2pjet", "1 b-tag resolved"),
        ("2tag2pjet", "2 b-tags resolved"),
        ("1tag1pfat0pjet", "1 b-tag merged"),
        ("2tag1pfat0pjet", "2 b-tags merged"),
        ("1ptag2pjet", "1+2 b-tags resolved"),
        ("1ptag1pfat0pjet", "1+2 b-tags merged")
        ]

lAddTagRegions = [ 
                  ("noaddbjetsr", "")
                , ("topaddbjetcr", "t#bar{t} CR")
                 ]

lLepChannels = [ #("llJ", "$\\ell\\ell J$")
              # , ("lvJ", "$\\ell\\nu J$")
              # , ("vvJ", "$\\nu\\nu J$")
               ]


lMHRegionsROOT = [("0_500ptv_SR", "75 GeV < m_{H} < 145 GeV, p_{T,V} < 500"),
                  ("500ptv_SR", "75 GeV < m_{H} < 145 GeV, p_{T,V} > 500"),
                  ("SR", "75 GeV < m_{H} < 145 GeV"),
                  ("mBBcr", "m_{H}<75 GeV || m_{H}>145 GeV"),
                  ("mBBincl", "m_{BB} inclusive"),
                  ("topemucr", "t#bar{t} CR")
                 ]
           #, ("lowMH", "m_{jet} < 75 GeV")
           #, ("highMH", "m_{jet} > 145 GeV")
           
lTagRegionsROOT = [("1tag2pjet", "1 b-tag resolved"),
                   ("2tag2pjet", "2 b-tags resolved"),
                   ("1tag1pfat0pjet", "1 b-tag merged"),
                   ("2tag1pfat0pjet", "2 b-tags merged"),
                   ("1ptag2pjet", "1+2 b-tags resolved"),
                   ("1ptag1pfat0pjet", "1+2 b-tags merged")
                  ]


lAddTagRegionsROOT = [ ("noaddbjetsr", "")
                     , ("topaddbjetcr", "t#bar{t} CR")
                    # , ("topaddbjetsr", "t#bar{t} CR")
                    # , ("topemucr", "t#bar{t} CR")
                    # , ("topemucr_noaddbjetsr", "t#bar{t} CR")
                     ]

lLepChannelsROOT = [ #("llJ", "llJ")
                  # , ("lvJ", "l#nuJ")
                  # , ("vvJ", "#nu#nuJ")
                   ]


def combineRE(xs):
    return re.compile(".*".join([""] + xs + [""]))


# UGLY
#def makeRegionConvList(channels, mhregs, tagregs, addtagregs):
#    l = []
#    for chan, chans in channels:
#        for mhr, mhrs in mhregs:
#            for tr, trs in tagregs:
#                for atr, atrs in addtagregs:
#                    regex = combineRE([chan, tr, atr, mhr])
#                    lab = ", ".join(filter(lambda s: s != "",
#                                [mhrs, chans, trs, atrs]))
#                    l.append((regex, lab))
#                    continue
#                continue
#            continue
#        continue
#    return l


def makeRegionConvList(channels, mhregs, tagregs, addtagregs):
    l = []
    for mhr, mhrs in mhregs:
        for tr, trs in tagregs:
            regex = combineRE([tr,  mhr])
            lab = ", ".join(filter(lambda s: s != "",
                    [mhrs, trs]))
            l.append((regex, lab))
            continue
        continue
    return l

lConvRegionName = \
        makeRegionConvList(lLepChannels, lMHRegions, lTagRegions,
            lAddTagRegions)

lConvRegionNameROOT = \
        makeRegionConvList(lLepChannelsROOT, lMHRegionsROOT,
                lTagRegionsROOT, lAddTagRegionsROOT)


# converts a nasty name string into a nice one via regex matching
def formatNameWithList(s, l):
    for (k, v) in l:
        if re.search(k, s):
            return v

    return s


def formatProcessName(s):
    return formatNameWithList(s, lConvProcessName)

def formatRegionName(s):
    return formatNameWithList(s, lConvRegionName)

def formatRegionNameROOT(s):
    return formatNameWithList(s, lConvRegionNameROOT)
