#python VHPlots.py mVHPlots/ /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_2000/HVT/ws/HVT_HVTZHllqq2000_fullsyst_CRfit_AZhBins2_121016_ZjSherpa221_PRSR_optCutsResolved_nopTV.root \
#    combined \
    /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_2000/HVT/ws/diagnostics/sbfit_HVT_fullsyst_CRfit_AZhBins2_121016_ZjSherpa221_PRSR_optCutsResolved_nopTV_2000p000.root \
#    /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_2000/HVT/ws/diagnostics/varBins_HVT_fullsyst_CRfit_AZhBins2_121016_ZjSherpa221_PRSR_optCutsResolved_nopTV.root > mVH.log


#python VHPlots.py pTVPlots/ /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_2000/HVT/ws/HVT_HVTZHllqq2000_fullsyst_CRfit_pTVfit_121016_nopTV.root \
#    combined \
#    /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_2000/HVT/ws/diagnostics/sbfit_HVT_fullsyst_CRfit_pTVfit_121016_nopTV_2000p000.root \
#    /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_2000/HVT/ws/diagnostics/varBins_HVT_fullsyst_CRfit_pTVfit_121016_nopTV.root > pTV.log


python VHPlots.py mVHPlots_151116/ /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_1500/HVT/ws/HVT_HVTZHllqq1500_fullsyst_151116_PRSR_0070pb.root \
    combined\
    /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_1500/HVT/ws/diagnostics/sbfit_HVT_fullsyst_151116_PRSR_0070pb_1500p000.root \
    /lustre/user/tlenz/DibosonStatsFramework/run2_LLBB_PRSR_1500/HVT/ws/diagnostics/varBins_HVT_fullsyst_151116_PRSR_0070pb.root
