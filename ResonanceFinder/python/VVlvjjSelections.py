class VVlvjjSelection:
    def __init__(self):
        self.Selection = ""
# SR WW
SRWWHP = VVlvjjSelection()
SRWWHP.Selection = "passSRWWHP"
SRWWLP = VVlvjjSelection()
SRWWLP.Selection = "passSRWWLP80"  #"passSRWWLP"
SRWWHPVBF = VVlvjjSelection()
#SRWWHPVBF.Selection = "passSRWWHPVBF"
SRWWHPVBF.Selection = "passSRWWHPVBF && !(runNumber==361092 && eventNumber==4401593)"
SRWWLPVBF = VVlvjjSelection()
SRWWLPVBF.Selection = "passSRWWLP80VBF"
# SR WZ
SRWZHP = VVlvjjSelection()
SRWZHP.Selection = "passSRWZHP"
SRWZLP = VVlvjjSelection()
SRWZLP.Selection = "passSRWZLP80" #"passSRWZLP"
SRWZHPVBF = VVlvjjSelection()
SRWZHPVBF.Selection = "passSRWZHPVBF"
SRWZLPVBF = VVlvjjSelection()
SRWZLPVBF.Selection = "passSRWZLP80VBF"
# SR WW/WZ
SRCombHP = VVlvjjSelection()
SRCombHP.Selection = "(passSRWWHP || passSRWZHP)"
SRCombLP = VVlvjjSelection()
SRCombLP.Selection = "(passSRWWLP80 || passSRWZLP80)" # "(passSRWWLP || passSRWZLP)"
SRCombHPVBF = VVlvjjSelection()
SRCombHPVBF.Selection = "(passSRWWHPVBF || passSRWZHPVBF)"
SRCombLPVBF = VVlvjjSelection()
SRCombLPVBF.Selection = "(passSRWWLP80VBF || passSRWZLP80VBF)"
# CR HP
SidebandHP = VVlvjjSelection()
SidebandHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80"
BtagHP = VVlvjjSelection()
BtagHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHP"
# CR HP VBF
SidebandHPVBF = VVlvjjSelection()
SidebandHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80VBF"
BtagHPVBF = VVlvjjSelection()
BtagHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHPVBF"
# CR LP
SidebandLP = VVlvjjSelection()
SidebandLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80 && !(runNumber==364168 && eventNumber==5111385)"
BtagLP = VVlvjjSelection()
BtagLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80"
#CR LP VBF
SidebandLPVBF = VVlvjjSelection()
SidebandLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80VBF  && !(runNumber==364168 && eventNumber==5111385)"
BtagLPVBF = VVlvjjSelection()
BtagLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80VBF"

SRWWResolved = VVlvjjSelection()
SRWWResolved.Selection = "pass_ggFResolved_WW_SR &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SRWWResolvedVBF = VVlvjjSelection()
SRWWResolvedVBF.Selection = "pass_VBFResolved_WW_SR &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SRWZResolved = VVlvjjSelection()
SRWZResolved.Selection = "pass_ggFResolved_WZ_SR && WleppT/lvjjmass_constZ>0.35 && WhadpT/lvjjmass_constZ>0.35 && !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SRWZResolvedVBF = VVlvjjSelection()
SRWZResolvedVBF.Selection = "pass_VBFResolved_WZ_SR && WleppT/lvjjmass_constZ>0.3 && WhadpT/lvjjmass_constZ>0.3 && !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)

SidebandHP = VVlvjjSelection()
SidebandHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80"
BtagHP = VVlvjjSelection()
BtagHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHP"
SidebandLP = VVlvjjSelection()
SidebandLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80"
BtagLP = VVlvjjSelection()
BtagLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80"

SidebandHPVBF = VVlvjjSelection()
SidebandHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80VBF"
BtagHPVBF = VVlvjjSelection()
BtagHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHPVBF"
SidebandLPVBF = VVlvjjSelection()
SidebandLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80VBF"
BtagLPVBF = VVlvjjSelection()
BtagLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80VBF"

SidebandResolved = VVlvjjSelection()
SidebandResolved.Selection = "pass_ggFResolved_WCR && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
BtagResolved = VVlvjjSelection()
BtagResolved.Selection = "pass_ggFResolved_TopCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SidebandResolvedVBF = VVlvjjSelection()
SidebandResolvedVBF.Selection = "pass_VBFResolved_WCR && !(runNumber==410000 && eventNumber==29499870) && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
BtagResolvedVBF = VVlvjjSelection()
BtagResolvedVBF.Selection = "pass_VBFResolved_TopCR && !(runNumber==361092 && eventNumber==3701027) && !(runNumber==364182 && eventNumber==1965740) && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
