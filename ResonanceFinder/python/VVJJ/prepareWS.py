#usage: python prepareWS_paper2017.py -s WZ -i  HVT  

#Config
doInjection = False 
doObserved = True 
doExternalPdf = False 
producePull = False 
doToys = False


def createDirectoryStructure(input_dir, releaseDir, analysis, inputListTag, outputWSTag, replaceRule):
  import os
  import shutil
  import glob

  # create directories
  newdir = '{releaseDir}/{analysis}/data/{inputListTag}'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  diagnosticsdir = '{releaseDir}/{analysis}/ws/diagnostics'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  tmpdir = '{releaseDir}/{analysis}/tmp'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)


  # first, we remove the previously existing directories
  dirs = [newdir, diagnosticsdir, tmpdir]
  for dir in dirs:
    if os.path.exists(dir):
      shutil.rmtree(dir)

  # we use makedirs, which takes care of creating intermediate directories
  os.makedirs(newdir)
  os.makedirs(diagnosticsdir)
  os.makedirs(tmpdir)
  
  # copy input trees from input location
  files = glob.glob(os.path.join(input_dir, '*.*'))
  for file in files:
    file_short = file.split('/')[-1] # e.g. take only data_SR.root
    if file_short not in replaceRule:
      shutil.copy2(file, newdir)
    else:
      if replaceRule[file_short] == None:
        print 'createDirectoryStructure is NOT copying file %s' % file
      else:
        replacement = '%s/%s' % (newdir, replaceRule[file_short])
        print 'createDirectoryStructure is RENAMING file %s to file %s' % (file, replacement)
        shutil.copy2(file, replacement)

  

if __name__ == '__main__':
  from argparse import ArgumentParser
  parser = ArgumentParser(description='retrieve RooStats limits from HistFitter outputs', add_help=True)
  parser.add_argument('-s', '--selection', type=str, dest='selection', help='Selection criteria', metavar='sel', choices=['WW', 'WZ', 'ZZ', 'WZorWW', 'WWorZZ'], required=True)
  parser.add_argument('-i', '--interpretation', type=str, dest='interpretation', help='Signal interpretation', metavar='sel', choices=['HVT', 'GraviC05', 'Gravi', 'Wprime', 'Radion'], required=True)

  options = parser.parse_args()



  print 'Running with selection=%s interpretation=%s' % (options.selection, options.interpretation)

  import ROOT
  ROOT.gROOT.SetBatch(True)

  from ROOT import RF as RF
  from ROOT import PullPlotter as PullPlotter

  # which analysis to run
  selection = options.selection
  interpretation = options.interpretation

  treeLumi = 140 #lumi fb-1 UPDATE 
  newLumi = 140  #fb-1

  #2019 paper
  inputPredir = '/afs/cern.ch/work/r/rjansky/public/4Kalliopi/FullRun2Inputs/'# UPDATE input trees
  analysisType = '' 
  inputListTag = '' 
  hasNtrk = False

  #UPDATE output name
  outputWSTag = 'Jan31_140ifb_test' 



  signal_mask = '%s_{mass}' % interpretation 
  # option parsing
  analysis = 'VVJJ_{interpretation}_{selection}'.format(interpretation = interpretation.upper(), selection = selection.upper()) 
  inputdir = '%s/%s/%s' % (inputPredir, analysisType, selection)
  releaseDir = 'results_%s_%sfb' % (outputWSTag, str(newLumi).replace('.', 'p')) 


  # append output name
  outputWSTag = '%s_%s_%s' % (outputWSTag, selection, interpretation)
  if doObserved : outputWSTag = '%s_obs' % outputWSTag
  else : outputWSTag = '%s_exp' % outputWSTag
  if 'QCD' in inputdir: outputWSTag = '%s_QCD' % outputWSTag
  if hasNtrk : outputWSTag = '%s_wNtrk' % outputWSTag
  else: outputWSTag = '%s_noNtrk' % outputWSTag


  # specific analysis settings UPDATE : fit bins (e.g. 67 bins) and mass range from 1300 TeV to 8000 TeV
  nbins = 67 # mJJ binning
  min_mjj = 1300
  max_mjj = 8000 
    
  muInject = 2.0 # injection settings
  massInject = '2000'
  
  #UPDATE luminosity systematics 
  lumiSys = 0.02 # relative luminosity uncertainty, on signals
  ntrkSys = 0.12 # relative ntrk uncertainty, on signals - ntrk 30

  
  #pdfSys = 0.04 # relative PDF uncertainty, on signals
  pdfSys={
    1000 : 0.01,
    1100 : 0.01,
    1200 : 0.01,
    1300 : 0.01,
    1400 : 0.01,
    1500 : 0.02,
    1600 : 0.02,
    1700 : 0.02,
    1800 : 0.03,
    1900 : 0.03,
    2000 : 0.03,
    2200 : 0.04,
    2400 : 0.04,
    2600 : 0.04,
    2800 : 0.06,
    3000 : 0.06,
    3200 : 0.06,
    3400 : 0.07,
    3500 : 0.08,
    3600 : 0.08,
    3800 : 0.08,
    4000 : 0.08,
    4200 : 0.09,
    4400 : 0.10,
    4500 : 0.10,
    4600 : 0.10,
    4800 : 0.11,
    5000 : 0.12,
    6000 : 0.14,
    }
  
  #UPDATE: parameters for fit .... small impact 
  if selection == 'WZ':
    xi_val = 4.393 
    if hasNtrk: xi_val = 7.519 #ntrk30 ichep 15.5
  elif selection == 'WW':
    xi_val = 3.907
    if hasNtrk: xi_val = 7.128
    #else: raise RuntimeError('xi value for WW without Ntrk is not implemented')
  elif selection == 'ZZ':
    xi_val = 6.009
    if hasNtrk: xi_val = 7.468
    #else: raise RuntimeError('xi value for ZZ without Ntrk is not implemented')
  elif selection == 'WZorWW':
    xi_val = 4.578
    if hasNtrk: xi_val = 7.394 
    #else: raise RuntimeError('xi value for ZZ without Ntrk is not implemented')
  elif selection == 'WWorZZ':
    xi_val = 4.2955
    if hasNtrk: xi_val = 7.449 
    #else: raise RuntimeError('xi value for ZZ without Ntrk is not implemented')
  else:
    raise RuntimeError('xi value for selection %s is not implemented' % selection)

  if ( doInjection == True ) :
    outputWSTag = '%s_siginj_mu%s' % (outputWSTag, str(muInject).replace('.', 'p'))
 

  # mass scan settings UPDATE input mass points 
  if selection == 'WZ':
    if interpretation.lower() == 'hvt': # HVT WZ
      masses = [ 1300, 1400, 1500, 1700, 1800, 1900, 2000, 2200, 2400, 2600, 2800, 3000, 3200, 3400, 3500, 3600, 3800, 4000, 4200, 4400, 4500, 4600, 4800, 5000, 6000 ] 
    else:
      raise RuntimeError('Unknown sel=%s int=%s' % (selection, interpretation))
  elif selection == 'WW':
    if interpretation.lower() == 'hvt':
      # HVT WW
      masses = [ 1300, 1400, 1500, 1600, 1700, 1800, 2000, 2400, 2600, 2800, 3000, 3500, 4000, 4500, 5000] 
    elif 'gravi' in interpretation.lower():
      # GRAVI WW
      masses = [1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400, 2600, 2800, 3000, 3500, 4000, 4500, 5000 ] 
    elif interpretation.lower() == 'radion':
      # Radion WW
      masses = [2000, 3000, 4000, 5000, 6000]
    else:
      raise RuntimeError('Unknown sel=%s int=%s' % (selection, interpretation))
  elif selection == 'ZZ' and 'gravi' in interpretation.lower(): 
    # GRAVI ZZ
      masses = [1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400, 2600, 2800, 3000, 3500, 4000, 4500, 5000 ]
  elif selection == 'ZZ' and interpretation.lower() == 'radion':
    # Radion ZZ
      masses = [2000, 3000, 4000, 5000, 6000]
  elif selection == 'WZorWW' and interpretation.lower() == 'hvt':
    # HVT WWorWZ
      masses = [ 1300, 1400, 1500, 1700, 1800, 2000, 2400, 2600, 2800, 3000, 3500, 4000, 4500, 5000] 
  elif selection == 'WWorZZ' and 'gravi' in interpretation.lower():
    # Gravi WWorZZ
      masses = [ 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400, 2600, 2800, 3000, 3500, 4000, 4500, 5000 ]
  elif selection == 'WWorZZ' and interpretation.lower() == 'radion':
    # Radion WWorZZ
      masses = [2000, 3000, 4000, 5000, 6000]
  else:
    raise RuntimeError('Unknown sel=%s int=%s' % (selection, interpretation))


  # mu scan settings
  if interpretation.lower() == 'hvt' and selection == 'WZ':
    poi_setups = { 
      1100 : [75, 0, 20],
      1200 : [75, 0, 20],
      1300 : [75, 0, 20],
      1400 : [75, 0, 20],
      1500 : [75, 0, 20],
      1600 : [75, 0, 20],
      1700 : [75, 0, 20],
      1800 : [60, 0, 20],
      1900 : [60, 0, 20],
      2000 : [60, 0, 20],
      2100 : [60, 0, 20],
      2200 : [60, 0, 20],
      2300 : [60, 0, 50],
      2400 : [60, 0, 50],
      2500 : [60, 0, 50],
      2600 : [60, 0, 50],
      2700 : [60, 0, 50],
      2800 : [60, 0, 50],
      2900 : [60, 0, 50],
      3000 : [60, 0, 50],
      3500 : [60, 0, 50],
      4000 : [60, 0, 50],
      4500 : [60, 0, 100],
      5000 : [60, 0, 200],
    }
  elif interpretation.lower() == 'hvt' and (selection == 'WW' or selection == 'WZorWW'): # for the OR we use the best range
    poi_setups = { 
      1100 : [75, 0, 20],
      1200 : [75, 0, 40],
      1300 : [75, 0, 40],
      1400 : [75, 0, 20],
      1500 : [75, 0, 20],
      1600 : [75, 0, 20],
      1700 : [75, 0, 20],
      1800 : [60, 0, 20],
      1900 : [60, 0, 20],
      2000 : [60, 0, 20],
      2100 : [60, 0, 20],
      2200 : [60, 0, 20],
      2300 : [60, 0, 40],
      2400 : [60, 0, 40],
      2500 : [60, 0, 40],
      2600 : [60, 0, 40],
      2700 : [60, 0, 40],
      2800 : [60, 0, 40],
      2900 : [60, 0, 40],
      3000 : [60, 0, 40],
      3500 : [60, 0, 50],
      4000 : [60, 0, 50],
      4500 : [60, 0, 100],
      5000 : [60, 0, 200],
    }
  elif 'gravi' in interpretation.lower() and (selection == 'WW' or selection == 'WWorZZ'): # for the OR we use the best range
    poi_setups = { 
      1100 : [75, 0, 20],
      1200 : [75, 0, 10],
      1300 : [75, 0, 20],
      1400 : [75, 0, 20],
      1500 : [75, 0, 20],
      1600 : [75, 0, 25],
      1700 : [75, 0, 50],
      1800 : [60, 0, 50],
      1900 : [60, 0, 50],
      2000 : [60, 0, 50],
      2100 : [60, 0, 75],
      2200 : [60, 0, 100],
      2300 : [60, 0, 100],
      2400 : [60, 0, 500],
      2500 : [60, 0, 500],
      2600 : [60, 0, 500],
      2700 : [60, 0, 500],
      2800 : [60, 0, 500],
      2900 : [60, 0, 500],
      3000 : [60, 0, 500],
      3500 : [60, 0, 1000],
      4000 : [60, 0, 10],
      4500 : [60, 0, 20],
      5000 : [60, 0, 100],
    }
  elif 'gravi' in interpretation.lower() and selection == 'ZZ':
    poi_setups = { 
      1100 : [75, 0, 50],
      1200 : [75, 0, 50],
      1300 : [75, 0, 50],
      1400 : [75, 0, 50],
      1500 : [75, 0, 50],
      1600 : [75, 0, 50],
      1700 : [75, 0, 100],
      1800 : [60, 0, 100],
      1900 : [60, 0, 100],
      2000 : [60, 0, 100],
      2100 : [60, 0, 150],
      2200 : [60, 0, 150],
      2300 : [60, 0, 200],
      2400 : [60, 0, 200],
      2500 : [60, 0, 250],
      2600 : [60, 0, 350],
      2700 : [60, 0, 450],
      2800 : [60, 0, 450],
      2900 : [60, 0, 550],
      3000 : [60, 0, 600],
      3500 : [60, 0, 2000],
      4000 : [60, 0, 20],
      4500 : [60, 0, 50],
      5000 : [60, 0, 200],
    }
  elif interpretation.lower() == 'radion': 
    poi_setups = {  # optimised by yy 
      1100 : [75, 0, 20],
      1200 : [75, 0, 10],
      1300 : [75, 0, 20],
      1400 : [75, 0, 20],
      1500 : [75, 0, 20],
      1600 : [75, 0, 25],
      1700 : [75, 0, 50],
      1800 : [60, 0, 50],
      1900 : [60, 0, 50],
      2000 : [60, 0, 50],
      2100 : [60, 0, 75],
      2200 : [60, 0, 100],
      2300 : [60, 0, 100],
      2400 : [60, 0, 500],
      2500 : [60, 0, 500],
      2600 : [60, 0, 500],
      2700 : [60, 0, 500],
      2800 : [60, 0, 500],
      2900 : [60, 0, 500],
      3000 : [60, 0, 500],
      3500 : [60, 0, 1000],
      4000 : [60, 0, 10],
      4500 : [60, 0, 20],
      5000 : [60, 0, 100]
    }
  else:
    raise RuntimeError('You forgot to implement a proper POI scan')

  # pull plot settings
  pullMasses = [3000]#,4000,5000]#[2000]

  # here we make sure multi-jet background is used instead of data when needed, and deleted otherwise
  # doObserved == False: replace data_SR.root with background_SR.root
  # doObserved == True: delete background_SR.root
  replaceRule = {}
  if doObserved:
    replaceRule['background_SR.root'] = None # this means: don't copy background_SR.root
  else:
    replaceRule['data_SR.root'] = None
    replaceRule['background_SR.root'] = 'data_SR.root' # this means: copy background_SR.root and rename it to data_SR.root
  createDirectoryStructure(inputdir, releaseDir, analysis, inputListTag, outputWSTag, replaceRule)
  
  ###
  vvjj = RF.VVJJAnalysisRunner(analysis)
  vvjj.setTreeObs('x') 
  vvjj.setTreeWeight('weight')
  vvjj.setLumiRescale(treeLumi, newLumi)
  vvjj.setNbins(nbins)
  vvjj.setObsMin(min_mjj)
  vvjj.setObsMax(max_mjj)
  vvjj.setReleaseDir(releaseDir)
  vvjj.setInputListTag(inputListTag)
  vvjj.setOutputWSTag(outputWSTag)
  vvjj.doLimit(False) 
  vvjj.doPValue(False) 
  vvjj.doPull(producePull)
  vvjj.setPOI('mu')
  vvjj.addChannel('SR', '') # not using selection criteria

  #vvjj.addResolNP('ATLAS_FATJET_BosonTag') # to obtain down variation automatically as 2*nominal-up !
  #vvjj.addResolNP('ATLAS_FATJET_JPTS')
  #vvjj.addResolNP('ATLAS_FATJET_JPTR')
  # create a new channel (will automatically add data)


  ###
  # (dijet mass) used in this formula
  if not doExternalPdf:
    fitfunction = "CEXPR::b('pow(1 - x/sqrts, ATLAS_qqqq_bkg_p2-xi*ATLAS_qqqq_bkg_p3) * pow(x/sqrts, -ATLAS_qqqq_bkg_p3)', {x[%.0f,%f],sqrts[13000],xi[%.3f],ATLAS_qqqq_bkg_p2[5.28833e+01,-100,100],ATLAS_qqqq_bkg_p3[-1.72355e+00,-100,100]})" %(min_mjj, max_mjj, xi_val)
    vvjj.setFitFunction(fitfunction, "x") 
  else:
    # in principle we do not need this in case we use the external PDF
    # but in this way the files under ws/diagnostics/histos* will have the
    # correct envelope
    fitfunction = "Background::b(x[%f,%f],ATLAS_qqqq_bkg_p2[5.28833e+01,-100,100],ATLAS_qqqq_bkg_p3[-1.72355e+00,-100,100],xi[%.3f],1)" %(min_mjj, max_mjj, xi_val) 
    vvjj.setFitFunction(fitfunction, "x") 


  # loop over signals
  for mass in masses:
    thisSig = signal_mask.format(mass=mass)
    vvjj.channel('SR').addSample(thisSig)
    vvjj.channel('SR').sample(thisSig).multiplyBy('mu', 0.0, -1e3, 1e4)
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_FATJET_BosonTag')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_FATJET_JPTS')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_FATJET_JPTR')
    vvjj.channel('SR').sample(thisSig).multiplyBy('ATLAS_Lumi', 1.0, 1-lumiSys, 1+lumiSys, RF.MultiplicativeFactor.GAUSSIAN)


    # set the normalisation uncertainties 
    if hasNtrk:
      vvjj.channel('SR').sample(thisSig).multiplyBy('ATLAS_qqqq_sig_ntrk', 1.0, 1-ntrkSys, 1+ntrkSys, RF.MultiplicativeFactor.GAUSSIAN)

    if interpretation.lower()!='gravi' :
      vvjj.channel('SR').sample(thisSig).multiplyBy('ATLAS_qqqq_pdf', 1.0, 1-pdfSys[mass]/2.0, 1+pdfSys[mass]/2.0, RF.MultiplicativeFactor.GAUSSIAN)

    isrFSRSys=0.03
    if interpretation.lower()=='gravi' :
      isrFSRSys=0.06
    
    vvjj.channel('SR').sample(thisSig).multiplyBy('ATLAS_qqqq_isrfsr', 1.0, 1-isrFSRSys, 1+isrFSRSys, RF.MultiplicativeFactor.GAUSSIAN)


    # declare signal mass value
    vvjj.addSignal(thisSig, mass)
    #
    # add background 
    #
  vvjj.channel('SR').addSample('background')
  vvjj.channel('SR').sample('background').multiplyBy('ATLAS_norm_qqqq_bkg', 1.0, 0, 1000)

  if not doExternalPdf:
    vvjj.channel('SR').sample('background').addVariation('fit')
  else:
    # copy and load background from Run-1 analysis
    import os
    ROOT.gROOT.ProcessLine('.L Background.cxx+')
    background_external_pdf = "Background::b(x[%f,%f],ATLAS_qqqq_bkg_p2[5.28833e+01,-100,100],ATLAS_qqqq_bkg_p3[-1.72355e+00,-100,100],xi[%.3f],1)" %(min_mjj, max_mjj, xi_val) 
    vvjj.channel("SR").sample("background").setFitFunction(background_external_pdf, "x")

  if doInjection:
    vvjj.setInjectionSample(signal_mask.format(mass=massInject))
    vvjj.setInjectionStrength(muInject)
  if doObserved:
    if doInjection:
      raise RuntimeError('Injection presumes you do not compute observed limits, in any case...')
    vvjj.setDoObserved(True)
  else:
    vvjj.setDoObserved(False)


  # setup limit runner
  vvjj.limitRunner().setPOIScan(100, 0, 100) # nsteps, min, max
  for mass in poi_setups:
    # deal with this signal in an optimised way
    vvjj.limitRunner().setPOIScan(mass, poi_setups[mass][0], poi_setups[mass][1], poi_setups[mass][2])

  # optional: use toys instead of asymptotics
  if doToys:
    vvjj.limitTool().setCalcType(RF.Frequentist)
    vvjj.limitTool().setTestStatType(RF.PL1sided)
    vvjj.limitTool().setNToys(5000)
    vvjj.limitTool().hypoTestInvTool().SetParameter('UseProof', True)
    vvjj.limitTool().hypoTestInvTool().SetParameter('GenerateBinned', True) # speeds up

  vvjj.run()

