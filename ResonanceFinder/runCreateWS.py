import os

hyps=[
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WZ -i HVT",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WWorWZ -i HVT",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WW -i HVT",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WW -i Gravi",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WWorZZ -i Gravi",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s ZZ -i Gravi",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WW -i GraviC05",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WWorZZ -i GraviC05",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s ZZ -i GraviC05",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WW -i Scalar",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s WWorZZ -i Scalar",
    "python VVJJConfig/prepareWS_paper2017_testNP.py -s ZZ -i Scalar",
]

#setup
commandSetup="source ResonanceFinder/setup_RF.sh"
os.system(commandSetup)

#compile
commandCompile="rc find_packages ; rc compile"
os.system(commandCompile)

for hyp in hyps :
    os.system(hyp)
