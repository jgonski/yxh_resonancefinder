#include <TFile.h>
#include <TList.h>
#include <TKey.h>
#include <iostream>
#include <fstream>
#include "ResonanceFinder/VHAnalysisRunner.h"
#include "ResonanceFinder/WtbLeptonicAnalysisRunner.h"
#include "ResonanceFinder/WtbHadronicAnalysisRunner.h"
#include "ResonanceFinder/VHqqbbAnalysisRunner.h"
#include "ResonanceFinder/YXHAnalysisRunner.h"
#include "ResonanceFinder/TaggerSFAnalysisRunner.h"
#include "ResonanceFinder/PullPlotter.h"
#include "ResonanceFinder/SystematicsSmoother.h"

using namespace RF;
using namespace std;

void tokenize(const string& str,
	      vector<string>& tokens,
	      const string& delimiters = " ")
{
  // Skip delimiters at beginning.
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (string::npos != pos || string::npos != lastPos)
    {
      // Found a token, add it to the vector.
      tokens.push_back(str.substr(lastPos, pos - lastPos));
      // Skip delimiters.  Note the "not_of"
      lastPos = str.find_first_not_of(delimiters, pos);
      // Find next "non-delimiter"
      pos = str.find_first_of(delimiters, lastPos);
    }
}

string trim(string const& source, char const* delims = " \t\r\n") {

  string result(source);

  string::size_type index = result.find_last_not_of(delims);

  if(index != string::npos)
    result.erase(++index);

  index = result.find_first_not_of(delims);
  if(index != string::npos)
    result.erase(0, index);
  else
    result.erase();
  return result;
}

class Configuration {

public:
  Configuration(string configFile){
    _configOK = false;

    _processorType = "";
    _cxaodSource = false;

    _doPull = false;
    _doLimit = false;
    _doPValue = false;
    _runFromWS = false;
    _produceWS = false;
    _setAsimovData = false;

    _doSmoothing = false;
    _splitShapeSystNorm = false;

    _calcType = RF::CalculatorType::Asymptotic;
    _testStat = RF::TestStatType::PL1sided;

    _dataTag = "data";
    _nominalTag = "nominal";
    _systUpTag = "_up";
    _systDnTag = "_down";

    _dataScale = 1;
    _lumiScale = 1;
    _glSigScale = 1;

    _pruneThresh = 0;
    _statThresh = 0.05;

    _useOverflow = false;
    _useUnderflow = false;
    _useProof = false;
    _verbose = false;

    _allowLargePrior = false;

    _nToys = 1000;
    _nWorkers = 5;

    parseConfig(configFile);
    if(_configOK) cout << "Configuration valid" << endl;
    else cout << "Configuration INVALID" << endl;
  }

  bool configStatus() { return _configOK; }

  void print(){

    cout << endl;
    cout << "Processor Type: " << _processorType << endl;

    cout << "Directory Configuration:" << endl;
    cout << "------------------------------------------------" << endl;
    if(_runFromWS){
      cout << "Source Workspace: " << _wsFileName << endl;
    }
    else{
      if(_histoDir.size()==0 && _cxaodFileName.size()>0){
	cout << "Source Histo File: " << _cxaodFileName << endl;
	_cxaodSource = true;
      }
      else if(_histoDir.size()>0 && _cxaodFileName.size()==0)
	cout << "Source Histo Directory: " << _histoDir << endl;
      else cout << "!!Error!! No Input File Specified!" << endl;
    }


    cout << "Analysis Tag: " << _analysisName << endl;
    cout << "Release Tag: " << _releaseTag << endl;
    cout << "Input Tag: " << _inputTag << endl;
    cout << "Output Tag: " << _outputTag << endl;
    cout << "Output Directory: " << _outDir << endl;
    cout << "Data tag: " << _dataTag << endl;
    cout << "Nominal tag: " << _nominalTag << endl;
    cout << "Syst Up tag: " << _systUpTag << endl;
    cout << "Syst Dn tag: " << _systDnTag << endl;
    cout << "MC Statistics Threshold: " << _statThresh << endl;
    cout << "Include overflow bin: " << _useOverflow << endl;
    cout << "Include underflow bin: " << _useUnderflow << endl;

    cout << endl << "Calculator Configuration:" << endl;
    cout << "------------------------------------------------" << endl;
    cout << "Run Limits: " << _doLimit << endl;
    cout << "Run Pvalues: " << _doPValue << endl;
    cout << "Run Pulls: " << _doPull << endl;
    cout << "Produce WS: " << _produceWS << endl;
    cout << "Calculator Type: " << _calcType << endl;
    cout << "Test Stat Type: " << _testStat << endl;
    cout << "# MC Toy Trials: " << _nToys << endl;
    if(_useProof) cout << "==>Using Proof with " << _nWorkers << " worker nodes" << endl;
    else cout << "==>Not using Proof" << endl;

    if(!_setAsimovData) cout << "==>Using Observed Data" << endl;
    else cout << "==>Using Asimov Data" << endl;

    cout << endl << "Mass Scan Info:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_massList.size(); i++) cout << "Mass: " << _massList[i] << endl;

    cout <<endl << "Regions/Channels Defined:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_regionNames.size(); i++) cout << "Region: " << _regionNames[i] << endl;
    for(auto &rb : _rebinFactors)
      cout << "Rebinning Factor: " << rb.first << " " << rb.second << endl;


    for(auto &mr : _mergeRegions){
      cout<<"Merge Region: "<<mr.first<<" with ";
      for(auto &tst : mr.second){
	cout<< tst << ", " ;
      }
      cout<<" MergedRegionName: "<<_mergedRegionName[mr.first]<<", ";
      cout<<" Flag: "<<_mergeRegionFlag[mr.first]<<endl;
    }

    for(auto &rr : _renameRegion){
      cout<<"Rename Region: "<<rr.first<<" to " << rr.second<<endl;
    }

    for(auto &lbound : _lowerBound)
      cout << "Lower Bound: " << lbound.first << "=" << lbound.second << endl;

    for(auto &ubound : _upperBound)
      cout << "Upper Bound: " << ubound.first << "=" << ubound.second << endl;


    cout << endl << "Signals Defined:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_sigNames.size(); i++) cout << "Signal: " << _sigNames[i] << endl;

    cout << endl << "Backgrounds Defined:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_bkgdNames.size(); i++) cout << "Background: " << _bkgdNames[i] << endl;
    cout << endl;
    for(auto &mr : _mergeBkgds){
      cout<<"Create Merged Bkgd: "<<mr.first<<" with ";
      for(auto &tst : mr.second){
	cout<< tst << ", " ;
      }
      cout << endl;
    }



    cout << endl << "Shape Systematics Defined:" << endl;
    cout << "------------------------------------------------" << endl;
    cout << "Applying Smoothing: " << _doSmoothing << endl;
    cout << "Split Norm Variation from Systs: " << _splitShapeSystNorm << endl;
    cout << "Pruning Threshold: " << _pruneThresh << endl;
    cout << "Allow large prior: " << _allowLargePrior << endl;
    for(auto &syst : _shapeSystNames) cout << "Systematic: " << syst.first << ", flag=" << syst.second << endl;

    cout << endl << "Normalization Systematics Defined:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_normSystNames.size(); i++) cout << "Systematic: " << _normSystNames[i] << endl;

    cout << endl << "Floating Systematics Defined:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_floatSystNames.size(); i++) cout << "Systematic: " << _floatSystNames[i] << endl;

    cout << endl << "Parameters of Interest:" << endl;
    for(uint i=0; i<_poiNames.size(); i++) cout << _poiNames[i] << " :  [" << _poiLoRange[i] << " , " << _poiHiRange[i] << "]" << endl;

    cout << endl << "Parameter of Interest Scans:" << endl;
    cout << "------------------------------------------------" << endl;
    for(uint i=0; i<_poiMass.size(); i++) cout << "Mass = " << _poiMass[i] << " :  " << _poiSteps[i] << " from " << _poiScanLo[i] << " to " << _poiScanHi[i] << endl;

    cout << endl << "Scaling Factors:" << endl;
    cout << "------------------------------------------------" << endl;
    cout << "Luminosity SF: " << _lumiScale << endl;
    cout << "Data SF: " << _dataScale << endl;
    cout << "Global Signal SF: " << _glSigScale << endl;
    for( auto &ks : _sigScales)
      cout << "Signal SF: " << ks.first << ":" << ks.second << endl;

    for( auto &kb : _bkgdScales)
      cout << "Bkgd SF: " << kb.first << ":" << kb.second << endl;


  }

public:

  vector<string> _sigNames;
  vector<string> _bkgdNames;
  vector<string> _allSampleNames;
  vector<string> _normSystNames;
  vector<TString> _shapeToNormSystNames;
  vector<string> _floatSystNames;
  vector<string> _regionNames;
  vector<TString> _regionNamesTest;

  string _histoDir;// = locConfig[0];
  string _processorType; // = "VHleptonic", "WtbLeptonic", "WtbHadronic", "VHHadronic", "TaggerSF"
  string _analysisName;// = locConfig[1]; //"WtbL";
  string _releaseTag;// = locConfig[2]; //"test_v1";
  string _inputTag;// = "itest01";
  string _outputTag;// = "wtest01";
  string _outDir;
  string _topDir;
  string _diagDir;
  string _wsFileName;
  string _cxaodFileName;
  string _dataTag;
  string _nominalTag;

  string _systUpTag;
  string _systDnTag;

  vector<string> _poiNames;
  vector<string> _poiApplyToSig;
  vector<double> _poiLoRange;
  vector<double> _poiHiRange;

  vector<double> _massList;
  vector<double> _poiMass;
  vector<int> _poiSteps;
  vector<double> _poiScanLo;
  vector<double> _poiScanHi;

  map<TString, int> _shapeSystNames;
  map<TString, double> _sigScales;
  map<TString, double> _bkgdScales;
  map<TString, int> _rebinFactors;
  map<TString, vector<TString> > _mergeBkgds;
  map<TString, vector<TString> > _mergeRegions;
  map<TString,TString> _mergedRegionName;
  map<TString, int> _mergeRegionFlag;
  map<TString, TString > _renameRegion;
  map<TString, double> _upperBound;
  map<TString, double> _lowerBound;
  map<TString, int> _histMap;
  map<TString, bool> _lowStatHistos;
  map<TString, vector<TString> > _pruneMap;
  map<TString, TH1F*> _rebinTemplates;

  double _glSigScale;
  double _dataScale;
  double _lumiScale;
  double _pruneThresh;
  double _statThresh;

  bool _doSmoothing;
  bool _splitShapeSystNorm;
  bool _doPull;
  bool _doLimit;
  bool _doPValue;
  bool _runFromWS;
  bool _produceWS;
  bool _setAsimovData;
  bool _useOverflow;
  bool _useUnderflow;
  bool _useProof;
  bool _verbose;
  bool _allowLargePrior;
  bool _cxaodSource;

  int _nToys;
  int _nWorkers;

  RF::CalculatorType _calcType;
  RF::TestStatType _testStat;

private:

  bool _configOK;

  void parseScale(string const& value, bool isSignal){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() != 2){ cout << "Error parsing scaling factor" << endl; return; }

    if(isSignal && (_sigScales.find(results[0]) == _sigScales.end())){
      if((results[0] == string("all")) || (results[0] == string("All"))) _glSigScale = atof(results[1].c_str());
      else _sigScales[results[0]] = atof(results[1].c_str());
    }
    else if(!isSignal && (_bkgdScales.find(results[0]) == _bkgdScales.end())){
      _bkgdScales[results[0]] = atof(results[1].c_str());
    }
    return;
  }

  void parsePoiName(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() == 3){
    _poiNames.push_back(results[0].c_str());
    _poiApplyToSig.push_back("All");  // apply to all signals by default
    _poiLoRange.push_back(atof(results[1].c_str()));
    _poiHiRange.push_back(atof(results[2].c_str()));
    }else if(results.size() == 4){
    _poiNames.push_back(results[0].c_str());
    _poiApplyToSig.push_back(results[1].c_str());
    _poiLoRange.push_back(atof(results[1].c_str()));
    _poiHiRange.push_back(atof(results[2].c_str()));
    }else{
       cout << "Error parsing POI Name" << endl; return;
    }


    return;
  }

  void parsePoiScan(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() != 4){ cout << "Error parsing POI Scan Setup" << endl; return; }

    _poiMass.push_back(atof(results[0].c_str()));
    _poiSteps.push_back(atoi(results[1].c_str()));
    _poiScanLo.push_back(atof(results[2].c_str()));
    _poiScanHi.push_back(atof(results[3].c_str()));

    return;
  }

  void parseBoundary(bool isUpper, string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() != 2){ cout << "Error parsing Boundary Setup" << endl; return; }

    if(isUpper)
      _upperBound[results[0]] = atof(results[1].c_str());
    else _lowerBound[results[0]] = atof(results[1].c_str());


    return;
  }

  void parseRebinFactor(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() != 2){ cout << "Error parsing Rebinning Factor" << endl; return; }

    _rebinFactors[results[0].c_str()] = atoi(results[1].c_str());

    return;
  }

  void parseShapeSysts(string const& value){
    vector<string> results;
    tokenize(value,results,":");

    int flag = 0;

    if(results.size()==2)
      flag = atoi(results[1].c_str());

    _shapeSystNames[results[0].c_str()] = flag;

    return;
  }

  void parseRenameRegionCommands(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() < 2){ cout << "Error parsing Rename Commands" << endl; return; }

    for(uint i=1; i<(results.size()); i++){

      _renameRegion[results[0].c_str()] = TString(results[i]);

    }
    return;
  }


  void parseMergeRegionCommands(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() < 3){ cout << "Error parsing Merge Commands" << endl; return; }

    _mergeRegionFlag[results[1].c_str()]=atoi(results[0].c_str());

    for(uint i=2; i<(results.size()); i++){

      if(TString(results[i]).Contains("MergedRegion")) break;
      _mergeRegions[results[1].c_str()].push_back(TString(results[i]));

    }

    if(TString(results[results.size()-1]).Contains("MergedRegion")){
      vector<string> mer_results;
      tokenize(results[results.size()-1],mer_results,"=");
      _mergedRegionName[results[1].c_str()]=TString(mer_results[1]);
    }else _mergedRegionName[results[1].c_str()]="unspecified";

    return;
  }

  void parseMergeBkgdCommands(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() < 3){ cout << "Error parsing Merge Commands" << endl; return; }

    if(_mergeBkgds.find(results[0].c_str()) != _mergeBkgds.end()) {
      cout << "Error parsing merge commands: already have this new background: " << results[0].c_str() << endl;
      return;
    }
    for(uint i=1; i<(results.size()); i++){
      _mergeBkgds[results[0].c_str()].push_back(TString(results[i]));
    }

    return;
  }


  void parseMassList(string const& value){
    vector<string> results;
    tokenize(value,results,":");
    if(results.size() == 0){ cout << "Error parsing mass list" << endl; return; }

    for(uint i=0; i<results.size(); i++){
      _massList.push_back(atof(results[i].c_str()));
    }

    return;
  }

  void parseConfig(string const& configFile){

    ifstream file(configFile.c_str());

    string line; string name; string value;
    string inSection;  int posEqual;

    while (getline(file,line)) {

      if (! line.length()) continue;

      if (line[0] == '#') continue;
      if (line[0] == '!') continue;
      /*
      if (line[0] == '[') {
	inSection=trim(line.substr(1,line.find(']')-1));
	continue;
      }
      */
      posEqual=line.find('=');
      name  = trim(line.substr(0,posEqual));
      value = trim(line.substr(posEqual+1));

      if(name == string("Signal")) _sigNames.push_back(value);
      else if(name == string("Bkgd")) _bkgdNames.push_back(value);
      else if(name == string("ShapeSyst")) parseShapeSysts(value);
      else if(name == string("NormSyst")) _normSystNames.push_back(value);
      else if(name == string("FloatSyst")) _floatSystNames.push_back(value);
      else if(name == string("Region")) _regionNames.push_back(value);
      else if(name == string("Channel")) _regionNames.push_back(value);
      else if(name == string("HistDir")) _histoDir = value;
      else if(name == string("LowerBound")) parseBoundary(0,value);
      else if(name == string("UpperBound")) parseBoundary(1,value);
      else if(name == string("UseProof")) _useProof = atoi(value.c_str());
      else if(name == string("Verbose")) _verbose = atoi(value.c_str());
      else if(name == string("nMCToys")) _nToys = atoi(value.c_str());
      else if(name == string("nProofWorkers")) _nWorkers = atoi(value.c_str());
      else if(name == string("IncludeOverflow")) _useOverflow = atoi(value.c_str());
      else if(name == string("IncludeUnderflow")) _useUnderflow = atoi(value.c_str());
      else if(name == string("PruneThreshold")) _pruneThresh= atof(value.c_str());
      else if(name == string("MCStatThreshold")) _statThresh= atof(value.c_str());
      else if(name == string("Release")) _releaseTag = value;
      else if(name == string("InputTag")) _inputTag = value;
      else if(name == string("OutputTag")) _outputTag = value;
      else if(name == string("ProcessorType")) _processorType = value;
      else if(name == string("NominalTag")) _nominalTag = value;
      else if(name == string("SystUpTag")) _systUpTag = value;
      else if(name == string("SystDnTag")) _systDnTag = value;
      else if(name == string("Analysis")) _analysisName = value;
      else if(name == string("DataTag")) _dataTag = value;
      else if(name == string("WorkspaceFile")) _wsFileName = value;
      else if(name == string("CxAODReaderFile")) _cxaodFileName = value;
      else if(name == string("UseAsimovData")) _setAsimovData = atoi(value.c_str());
      else if(name == string("RunPull")) _doPull = atoi(value.c_str());
      else if(name == string("RunLimit")) _doLimit = atoi(value.c_str());
      else if(name == string("RunPValue")) _doPValue = atoi(value.c_str());
      else if(name == string("RunFromWS")) _runFromWS = atoi(value.c_str());
      else if(name == string("ProduceWS")) _produceWS = atoi(value.c_str());
      else if(name == string("SignalScale")) parseScale(value,1);
      else if(name == string("BkgdScale")) parseScale(value,0);
      else if(name == string("DataScale")) _dataScale = atof(value.c_str());
      else if(name == string("LumiScale")) _lumiScale = atof(value.c_str());
      else if(name == string("PoiScan")) parsePoiScan(value);
      else if(name == string("PoiName")) parsePoiName(value);
      else if(name == string("MassList")) parseMassList(value);
      else if(name == string("RebinFactor")) parseRebinFactor(value);
      else if(name == string("MergeRegion")) parseMergeRegionCommands(value);
      else if(name == string("RenameRegion")) parseRenameRegionCommands(value);
      else if(name == string("MergeBkgd")) parseMergeBkgdCommands(value);
      else if(name == string("DoSmoothing")) _doSmoothing = atoi(value.c_str());
      else if(name == string("SplitShapeSystNorm")) _splitShapeSystNorm = atoi(value.c_str());
      else if(name == string("AllowLargePrior")) _allowLargePrior = atoi(value.c_str());
      else if(name == string("CalculatorType")){
	int type = atoi(value.c_str());
	if(type==0) _calcType = RF::CalculatorType::Frequentist;
	else if(type==1) _calcType = RF::CalculatorType::Hybrid;
	else if(type==2) _calcType = RF::CalculatorType::Asymptotic;
	else if(type==3) _calcType = RF::CalculatorType::AsymptoticWithNominalAsimov;
      }
      else if(name == string("TestStatType")){
	int type = atoi(value.c_str());
	if(type==0) _testStat = RF::TestStatType::LEP;
	else if(type==1) _testStat = RF::TestStatType::Tevatron;
	else if(type==2) _testStat = RF::TestStatType::PL2sided;
	else if(type==3) _testStat = RF::TestStatType::PL1sided;
	else if(type==4) _testStat = RF::TestStatType::PLsigned;
	else if(type==5) _testStat = RF::TestStatType::MLE;
	else if(type==6) _testStat = RF::TestStatType::Nobs;
      }
    }


    _topDir = string(_releaseTag).append("/").append(_analysisName);
    _outDir = string(_topDir).append("/data/").append(_inputTag);
    _diagDir = string(_topDir).append("/ws/diagnostics/");

    if(_massList.size()>0) _configOK = true;

    if(!_doPull && !_doLimit && !_doPValue && !_produceWS){
      _configOK = false;
      cout << "Configuration failed:  you didn't specify a job type!" << endl;
    }

    for(auto chan : _regionNames){
      TString schan = TString(chan.c_str());

      if(schan.BeginsWith("Ch") || schan.BeginsWith("ch") || schan.BeginsWith("CH")) schan.Remove(0,2);
      if(schan.BeginsWith("Chan") || schan.BeginsWith("chan") || schan.BeginsWith("CHAN")) schan.Remove(0,4);
      //      TString out("_");
      //      out.Append(schan);
      _regionNamesTest.push_back(schan);
    }

    for(auto aSig : _sigNames){
      for(auto aMass : _massList){
	string thisSigName = aSig;
	thisSigName.append(to_string((int)(aMass)));
	_allSampleNames.push_back(thisSigName);
      }
    }
    _allSampleNames.insert(_allSampleNames.end(),_bkgdNames.begin(),_bkgdNames.end());
  }

};


/// Declare functions that will be used in the following code
void createPruneMap(Configuration& config);
int testSyst(TString systname, Configuration& config, TString region, TString sample, TH1F* nom, const TH1F* syst);
TH1F* getRebinnedHisto(TH1F* histo, TH1F* templHist);
void getHistMap(Configuration& config);
void splitShapeSystNorm(Configuration& config, TString region , TString sample, const TH1F* nom, TH1F* syst);
bool checkNaN(const TH1F* histo);


bool checkNaN(const TH1F* histo){
  if(!histo) return false;
  for(int i=0; i<=histo->GetNbinsX()+1; i++) if(TMath::IsNaN(histo->GetBinContent(i))) return true;
  return false;
}

void splitShapeSystNorm(Configuration& config, TString ichan, TString isamp, const TH1F* nom, TH1F* syst){

  if(!nom || !syst) return;

  if(syst->Integral()<1e-4) return;

  float normFact = (syst->Integral() - nom->Integral()) / (nom->Integral()+1e-9);
  if(fabs(normFact) < 1E-3) return;

  bool isUpVari = false;
  bool found = false;
  TString systName = syst->GetName();
  string normstring = to_string(normFact);


  found = false;
  TString uTag(config._systUpTag.c_str());
  if(systName.Contains(uTag)){ found = true; isUpVari = true; systName.ReplaceAll(uTag,"");}
  if(!found){
    TString dTag(config._systDnTag.c_str());
    if(systName.Contains(dTag)){ found = true; isUpVari = false; systName.ReplaceAll(dTag,"");}
  }
  if(!found) return; //strange, didn't have this one?

  if(config._cxaodSource){
   for(auto region : config._regionNamesTest){
      if (systName.Contains(TString(region).Append("_"))) { ichan = region; found = true; systName.ReplaceAll(TString(ichan).Append("_"),"");}

      if(found) break;
    }
    if(!found) return; //strange, didn't have this one?

    found = false;
    for(auto sample : config._allSampleNames){
      if(systName.Contains(TString(sample.c_str()).Append("_"))){ isamp = TString(sample.c_str()); found = true; systName.ReplaceAll(TString(isamp).Append("_"),"");}

      if(found) break;
    }
    if(!found) return; //strange, didn't have this one?
  }

  //if we're here, we've found all the info we need and the only thing left in the name is the variation.

  //apply syst sculpting?
  // 1 = smooth only
  // 2 = remove normalization
  // 3 = smooth and remove norm
  // 4 = average up/down
  // 5 = average and smooth
  // 6 = average and remove norm
  // 7 = average, smooth, and remove norm
  found = false;
  for(auto &syst : config._shapeSystNames){ //are we actually calling for this syst?  if not, ignore
    //check to see if the shape syst is intended to be normalized
    if(systName == syst.first){ found = true; break;}
  }


  if(found){

    //Scale to nominal rate to remove normalization effects
    syst->Scale(nom->Integral()/syst->Integral());

    //create a norm syst for this guy!
    TString systOut = ichan.Append(":").Append(isamp).Append(":").Append(systName).Append(":");

    //does this already exist?
    found = false;
    int fIdx = 0;
    for(uint i=0; i<config._shapeToNormSystNames.size(); i++){
      if(TString(config._shapeToNormSystNames[i]).Contains(systOut)){ found = true; fIdx = i;}
      if(found) break;
    }
    if(found){
      if(isUpVari) config._shapeToNormSystNames[fIdx].ReplaceAll("placeholderUp",normstring.c_str());
      else config._shapeToNormSystNames[fIdx].ReplaceAll("placeholderDown",normstring.c_str());
    }
    else{ //guess not, let's create it
      if(isUpVari) systOut.Append(normstring.c_str()).Append(":placeholderDown");
      else systOut.Append("placeholderUp:").Append(normstring.c_str());
      config._shapeToNormSystNames.push_back(systOut);
    }
  }

  return;
}


TH1F* getRebinnedHisto(TH1F* histo, TH1F* templHist){
  templHist->Scale(0);
  int bin= 0;
  double binErr1= 0;
  double binErr2= 0;
  for(int i=0; i<=histo->GetNbinsX(); i++){
    bin = templHist->FindBin(histo->GetBinCenter(i));
    binErr1 = templHist->GetBinError(bin);
    binErr2 = histo->GetBinError(i);
    templHist->AddBinContent(bin,histo->GetBinContent(i));
    templHist->SetBinError(bin,sqrt(binErr1*binErr1 + binErr2*binErr2));
  }
  return (TH1F*)(templHist->Clone(TString(histo->GetName()).Append("_clone")));
}

int testSyst(TString systName, Configuration& config, TString fRegion, TString fSample, TH1F* nom, TH1F* syst){

  //return -1 if the systematic histo doesn't exist...or if the nominal doesn't exist
  //return 0 if the systematic gets pruned
  //return 1 if the systematic passes all tests


  if(!nom) return -1;
  if(nom->Integral()<=0) return -1;

  //Quick check to see if the nominal histo has large stat error, for reporting purposes
  double error = 0;
  double statRatio = 1/nom->IntegralAndError(1,nom->GetNbinsX(),error);
  statRatio *= error;
  if(statRatio>0.50 && config._lowStatHistos.find(nom->GetName())==config._lowStatHistos.end()) config._lowStatHistos[nom->GetName()] = true;

  if(!syst) return -1;


  if(checkNaN(syst)){ if(config._verbose){cout << "Variation has NaN content, rejecting " << syst->GetName() << endl;} return 0;} //prune these
  if(fabs(syst->Integral())<1e-6){ if(config._verbose){cout << "Variation has zero integral, rejecting " << syst->GetName() << endl;} return 0;} //prune these

  TString region = "null";
  for(auto regI : config._regionNamesTest){
    if(TString(nom->GetName()).Contains(regI)) region = regI;
  }

  if(config._splitShapeSystNorm) splitShapeSystNorm(config, fRegion, fSample, nom, syst);

  TH1F* tNom = nom;
  TH1F* tSys = syst;
  bool rebinned = false;

  if(config._rebinTemplates.find(region) != config._rebinTemplates.end()){
    tSys = getRebinnedHisto(syst,config._rebinTemplates[region]);
    tNom = getRebinnedHisto(nom,config._rebinTemplates[region]);
    rebinned = true;
  }


  //After all other manipulations, apply systematics smoothing if requested
  if(config._doSmoothing){
    //apply syst sculpting?
    // 1 = smooth only
    // 2 = remove normalization
    // 3 = smooth and remove norm
    // 4 = average up/down
    // 5 = average and smooth
    // 6 = average and remove norm
    // 7 = average, smooth, and remove norm
    for(auto &vari : config._shapeSystNames){ //individual flags
      if(vari.first == systName){
	if(vari.second == 1 || vari.second==3 || vari.second==5 || vari.second==7){
	  RF::SystematicsSmoother SysS;
	  SysS.setNumberOfLocalExtrema(1); //1: allows parabolic shape for mVH
	  SysS.doApplyAveraging(false); //false: no averaging of adjacent bins
	  SysS.smoothOneHistogram(tNom,tSys);
	}
      }
    }
  }

  double nPrn = 0;
  double nHi = 0;
  double nOcc = 0;
  double inom = 0;
  double ratio = 0;
  for(uint i=1; i<=(uint)(tNom->GetNbinsX()); i++){
    inom = tNom->GetBinContent(i);
    if(inom<1e-6) continue; //let's only worry about bins with significant content

    nOcc++;
    ratio = fabs((tSys->GetBinContent(i)-inom)/(inom+1e-9));

    if(ratio>config._pruneThresh) nPrn++;
    if(ratio>0.9) nHi++;
  }

  if(rebinned){
    tNom->Delete();
    tSys->Delete();
  }



  //if at least 33% of occupied bins don't satisfy the pruning threshold, prune this variation
  //but if the global ratio is greater than the threshold, we can keep it (or vice versa)
  //  nPrn /= (nOcc+1e-9);

  nHi /= (nOcc+1e-9);

  //Now to harmonize with WSMaker, require at least 2 bins above the nominal threshold
  if(nPrn<2){
    if(config._verbose) cout << "MultiProc::Pruning, Prune Threshold Test failed for " << syst->GetName() << endl;
    return 0;
  }
  if( !(config._allowLargePrior) && (nHi>0.50) ){
    if(config._verbose) cout << "MultiProc::Pruning, 100% Error Test failed for " << syst->GetName() << endl;
    return 0;
  }

  return 1;
}

void getHistMap(Configuration& config){

  //need to walk through all known regions, samples and variations to determine
  // which ones are present, in order to test for pruning
  TString nomName;
  TString uTag(config._systUpTag.c_str());
  TString dTag(config._systDnTag.c_str());
  TString nomTag(config._nominalTag.c_str());
  TString syst;
  TString uName;
  TString dName;

  for(auto region : config._regionNamesTest){
    for(auto sample : config._allSampleNames){

      TFile* ftest = new TFile(TString(config._outDir.c_str()).Append("/").Append(sample.c_str()).Append("_").Append(region).Append(".root"));

      nomName = TString(sample.c_str()).Append("_").Append(region);

      if(ftest) config._histMap[nomName]=1;
      else{
	cout << "syst check: no file" << endl;
	config._histMap[nomName]=0;
	continue;
      }

      TH1F* nom = (TH1F*)ftest->Get(nomTag);
      if(!nom){
	//Nominal doesn't exist
	ftest->Close(); delete ftest;
	config._histMap[nomName]=0;
	continue;
      }

      for(auto systIter : config._shapeSystNames){
	syst = systIter.first;

	uName = nomName;
	uName.Append("_").Append(syst);
	dName = uName;
	uName.Append(uTag);
	dName.Append(dTag);

	TH1F* up = (TH1F*)ftest->Get(TString(syst).Append(uTag));
	TH1F* dn = (TH1F*)ftest->Get(TString(syst).Append(dTag));

	config._histMap[uName] = testSyst(syst,config,region, sample, nom,up);
	config._histMap[dName] = testSyst(syst,config,region, sample, nom,dn);
      }

      ftest->Close();
      delete ftest;
    }
  }

  createPruneMap(config);

  return;
}

void getCxAODhistMap(Configuration& config, VbaseAnalysisRunner* iAna){

  TFile* ftest = new TFile(config._cxaodFileName.c_str());
  if(!ftest) return;

  TH1F* nomHist=0;
  TH1F* syst=0;

  TString name;
  TString nomName;
  TString uTag(config._systUpTag.c_str());
  TString dTag(config._systDnTag.c_str());
  TString nomTag(config._nominalTag.c_str());
  TString fRegion;

  TKey* key;
  bool found, isSyst, isSignal;

  map<TString, TH1F*> bkgdTotals;

  TDirectory* ndir = ftest->GetDirectory("");
  if(!ndir){ cout << "Houston, we have a problem" << endl; assert(false);}
  TList* nomKeys = ndir->GetListOfKeys();
  TIter nextNom(nomKeys);

  while((key = (TKey*)nextNom())){
    if(key->GetCycle()!=1) continue;
    name = key->GetName();
    nomName = name;
    if(name.Contains("data")) continue;

    isSyst = false;
    if(name.Contains(uTag)){
      isSyst = true;
      nomName.ReplaceAll(uTag,"");
    }
    else if(name.Contains(dTag)){
      isSyst = true;
      nomName.ReplaceAll(dTag,"");
    }

    //Check to see if we have a valid region
    found = false;
    fRegion = "";
    for(auto region : config._regionNamesTest){
      if(name.Contains(region)){ found = true; fRegion = region;}
      if(found) break;
    }
    if(!found) continue;

    for(auto region : config._regionNames){
      if(TString(region.c_str()).Contains(fRegion)){ fRegion = region;}
    }

    //Check to see if we have a valid variation
    found = false;
    TString systName;
    for(auto systI : config._shapeSystNames){
      if(name.Contains(systI.first)){
	found = true;
	systName = systI.first;
	nomName.ReplaceAll(systI.first,"");
      }
      if(found) break;
    }

    isSignal = true;
    for(auto bkgI : config._bkgdNames){
      if(nomName.Contains(bkgI.c_str())){ isSignal = false; break;}
    }

    if(isSyst && !found) continue;
    if(config._histMap.find(name) != config._histMap.end()) continue;

    if(!isSyst){
      nomHist = dynamic_cast<TH1F*>(key->ReadObj());
      bool isNan = checkNaN(nomHist);
      if(isNan) cout << "Warning: NaN found for nominal histo " << nomHist->GetName() << endl;

      if(!isSignal){
	if(bkgdTotals.find(fRegion) == bkgdTotals.end()){
	  bkgdTotals[fRegion] = (TH1F*)(nomHist->Clone(TString("clone").Append(fRegion)));
	}
	else bkgdTotals[fRegion]->Add(nomHist);
      }

      if(nomHist && !isNan) config._histMap[name] = 1;
      else config._histMap[name] = 0;
    }
    else{
      //==1 if the syst is ok
      //==0 if the syst is pruned
      //==-1 if the syst doesn't exist
      nomHist = (TH1F*)(ftest->Get(nomName));
      syst = dynamic_cast<TH1F*>(key->ReadObj());
      if(!syst) continue;//this should never happen, but just checking
      config._histMap[name] = testSyst(systName, config,"","",nomHist,syst);
    }
  }

  vector<double> fBins;
  int rbFlag;
  for(auto region : bkgdTotals){
    if(config._rebinFactors.find(region.first) != config._rebinFactors.end()) rbFlag = config._rebinFactors[region.first];
    else{ cout << "Could not find rebin factor for: " << region.first << endl;  continue;}
    cout << "Total Bkgd Yield for " << region.first << ": " << region.second->Integral() << endl;
    config._rebinTemplates[region.first] = iAna->rebinHisto(rbFlag,region.first,region.second,fBins);
  }

  TDirectory* sdir = ftest->GetDirectory("Systematics");
  if(!sdir) sdir = ftest->GetDirectory("systematics");

  if(sdir){
    TList* systKeys = sdir->GetListOfKeys();
    TIter nextSyst(systKeys);

    while((key = (TKey*)nextSyst())){
      name = key->GetName();
      nomName = name;

      if(key->GetCycle()!=1) continue;
      if(name.Contains("data")) continue;

      if(name.Contains(uTag)){
	isSyst = true;
	nomName.ReplaceAll(uTag,"");
      }
      else if(name.Contains(dTag)){
	isSyst = true;
	nomName.ReplaceAll(dTag,"");
      }
      else if(name.Contains(nomTag)){
	isSyst = false;
      }
      else continue;

      if(!isSyst) continue; //why do we have nominal in the systematics directory??


      //Check to see if we have a valid variation
      found = false;
      TString systName;
      for(auto systI : config._shapeSystNames){
	TString t1(systI.first);
	TString t2 = t1;
	t1.Append(uTag);
	t2.Append(dTag);
	if(name.Contains(t1) || name.Contains(t2)){
	  found=true;
	  systName = systI.first;
	  nomName.ReplaceAll(TString("_").Append(systI.first),"");
	}
	if(found) break;
      }
      if(!found) continue;

      found = false;
      //Check to see if we have a valid sample
      for(auto samp : config._allSampleNames){
	if(name.Contains(TString(samp.c_str()).Append("_"))){ found = true; }
	if(found) break;
      }
      if(!found) continue;

      //Check to see if we have a valid region
      found = false;
      for(auto region : config._regionNamesTest){
	if(name.Contains(region)){ found = true; }
	if(found) break;
      }
      if(!found) continue;

      //Do we have this already? Check after the syst/region tests to save time
      if(config._histMap.find(name) != config._histMap.end()) continue;

      //==1 if the syst is ok
      //==0 if the syst is pruned
      //==-1 if the syst doesn't exist
      syst = dynamic_cast<TH1F*>(key->ReadObj());
      if(!syst) continue;//this should never happen, but just checking

      nomHist = (TH1F*)(ndir->Get(nomName));
      bool isNan = checkNaN(nomHist);
      if(!nomHist){ cout << "Could not find: " << nomName << endl; continue;}
      if(isNan){ cout << "NaN found for : " << nomName << endl; continue;}

      config._histMap[name] = testSyst(systName,config,"","",nomHist,syst);
    }
  }


  ftest->Close();
  delete ftest;

  createPruneMap(config);

  return;
}

void createPruneMap(Configuration& config){
  //Now let's establish a map of combined bkgd per region to valid systematics for that region
  //If a syst is pruned for all bkgds, we prune it globally.  If any systs
  //persist for any background, we keep the syst globally.
  TString upTag(config._systUpTag.c_str());
  TString dnTag(config._systDnTag.c_str());
  TString uName, dName, name;
  map<TString, int>::iterator iter;

  for(auto region : config._regionNamesTest){//regions
    vector<TString> systsToKeep;
    vector<TString> systsPruned;

    for(auto syst : config._shapeSystNames){//systnames
      bool keep = false;
      bool present = false;

      for(auto isamp : config._allSampleNames){
	if(keep) break;
	name = TString(isamp.c_str());
	name.Append("_").Append(region).Append("_").Append(syst.first);
	uName = name;
	dName = name;
	uName.Append(upTag);
	dName.Append(dnTag);

	iter = config._histMap.find(uName);
	if(iter!=config._histMap.end()){
	  present = true;
	  if(iter->second==1) keep= true;
	}
	iter = config._histMap.find(dName);
	if(iter!=config._histMap.end()){
	  present = true;
	  if(iter->second==1) keep= true;
	}
      }

      if(keep) systsToKeep.push_back(syst.first);
      else if(present) systsPruned.push_back(syst.first);
    }
    config._pruneMap[region] = systsToKeep;

    if(systsPruned.size()>0){
      cout << endl << "**********************************************" << endl;
      cout << "   List of systematics pruned in region: " << region << endl;
      cout << "**********************************************" << endl;

      for(auto& syst: systsPruned){
	cout << "Syst: " << syst << endl;
      }
      cout << "**********************************************" << endl;
    }
  }

  return;//end of function
}

bool nominalExists(Configuration &config, string region, string sample){

  TString schan = region.c_str();
  if(schan.BeginsWith("Ch") || schan.BeginsWith("ch") || schan.BeginsWith("CH")) schan.Remove(0,2);
  if(schan.BeginsWith("Chan") || schan.BeginsWith("chan") || schan.BeginsWith("CHAN")) schan.Remove(0,4);

  TString samp = TString(sample.c_str()).Append("_").Append(schan);
  int retval = 0;
  if(config._histMap.find(samp) == config._histMap.end()) retval = 0;
  else retval = config._histMap[samp];

  if(retval==0)  cout << "Nominal for " << region << " " << sample << " doesn't exist" << endl;

  return retval;
}

int includeSyst(Configuration &config, string region, string sample, string syst){
  TString schan = region.c_str();
  if(schan.BeginsWith("Ch") || schan.BeginsWith("ch") || schan.BeginsWith("CH")) schan.Remove(0,2);
  if(schan.BeginsWith("Chan") || schan.BeginsWith("chan") || schan.BeginsWith("CHAN")) schan.Remove(0,4);

  if(config._pruneMap.find(schan.Data()) == config._pruneMap.end()) return -1; //we don't have this region?

  TString upTag(config._systUpTag.c_str());
  TString dnTag(config._systDnTag.c_str());
  TString nomTag(config._nominalTag.c_str());
  TString samp = TString(sample.c_str()).Append("_").Append(schan).Append("_").Append(syst.c_str());

  TString upN = samp;
  TString dnN = samp;
  upN.Append(upTag);
  dnN.Append(dnTag);

  ///Check the pruneMap to see if we should keep this syst in this region
  bool keepRegion = false;
  for(auto name : config._pruneMap[schan.Data()]){
    if(keepRegion) break;
    if(name == TString(syst.c_str())) keepRegion = true;
  }

  if(!keepRegion) return -1;
  else{
    //we reached this point because for this region, we're supposed to include this syst
    //but we have to figure out if we have any actual histograms
    int hasDn = -1; //assume they both don't exist to start
    int hasUp = -1;

    ///Check the histomap to get the status of this syst hist
    map<TString, int>::iterator iter = config._histMap.find(dnN);
    if(iter != config._histMap.end()){
      hasDn = (*iter).second;
    }

    iter = config._histMap.find(upN);
    if(iter != config._histMap.end()){
      hasUp = (*iter).second;
    }

    // 1 == syst exists and wasn't pruned
    // 0 == syst exists and was pruned
    //-1 == syst doesn't exist
    if(hasUp==1 && hasDn==1) return 2; //both variations exist
    //    else if((hasUp==0 && hasDn==1) || (hasUp==1 && hasDn==0) ) return -1; //one is pruned, one is not
    else if((hasUp==0 && hasDn==1) || (hasUp==1 && hasDn==0) ) return 2; //one is pruned, one is not
    else if((hasDn==-1 && hasUp==1) || (hasUp==-1 && hasDn==1)) return 1; //only one exists, so this should be one-sided
    else if(hasDn==-1 && hasUp==-1) return -1; //neither exist
    else if(hasDn==0 && hasUp==0) return -1; //both pruned
    else return -1; //both are pruned and this sample isn't merged
  }

  return -1;//should never get here, but just in case
}

void applyNormSyst(string systString, RF::Sample& sample, string sampleName, string regionName, bool isFloat){

  vector<string> tokens;
  tokenize(systString, tokens, ":");

  if(tokens.size()!=5){ cout << "applyNormSyst:  Error, incorrect argument length! " << tokens.size() << " , " << systString << endl; return; }

  TString regionTest(regionName.c_str());
  TString regionSamp(tokens[0].c_str());
  if((!(regionTest.Contains(regionSamp))) && (tokens[0]!=string("ALL")) && (tokens[0]!=string("All"))) return;
  if((tokens[1]!=sampleName) && (tokens[1]!=string("ALL")) && (tokens[1]!=string("All"))) return;

  if(TString(tokens[3].c_str()).Contains("placeholder")) tokens[3] = tokens[4];
  if(TString(tokens[4].c_str()).Contains("placeholder")) tokens[4] = tokens[3];

  if(TString(tokens[3].c_str()).Contains("placeholder")){ cout << "Placeholder Error: " << systString << endl; return;}
  if(TString(tokens[4].c_str()).Contains("placeholder")){ cout << "Placeholder Error: " << systString << endl; return;}


  double systUp = atof(tokens[3].c_str());
  double systDn = atof(tokens[4].c_str());

  //Apply Averaging
  if(!isFloat){
    bool isInverted = false;
    if(systDn>0 && systUp<0) isInverted = true;
    systUp = fabs(systDn)+fabs(systUp);
    systUp /= 2.0;
    systDn = systUp;
    if(isInverted){
      systUp *=-1;
      systDn *=-1;
    }
  }

  string systName = tokens[2];

  if(isFloat){
    //    sample.multiplyBy(systName.c_str(), 1.0, systUp, systDn);
    sample.multiplyBy(systName.c_str(), 1.0, systUp, systDn, RF::MultiplicativeFactor::ConstraintType::FREE);
  }
  else{
    sample.multiplyBy(systName.c_str(), 1.0, 1.0-systDn, 1.0+systUp, RF::MultiplicativeFactor::ConstraintType::LOGNORMAL);
    //sample.multiplyBy(systName.c_str(), 1.0, 1.0-systDn, 1.0+systUp, RF::MultiplicativeFactor::ConstraintType::GAUSSIAN);
  }
  return;
}



void createDirectories(Configuration& myCfg){

  string dirA = string(myCfg._releaseTag).append("/").append(myCfg._analysisName).append("/data/").append(myCfg._inputTag);
  string dirB = string(myCfg._releaseTag).append("/").append(myCfg._analysisName).append("/ws/diagnostics/");
  string dirC = string(myCfg._releaseTag).append("/").append(myCfg._analysisName).append("/tmp/");
  string mkdir = "mkdir -p ";

  string tmp = dirA;
  tmp.insert(0,mkdir);
  dirB.insert(0,mkdir);
  dirC.insert(0,mkdir);

  int resA = system(tmp.c_str());
  int resB = system(dirB.c_str());
  int resC = system(dirC.c_str());

  int resD = 0;
  if(!myCfg._runFromWS && myCfg._histoDir.size()>0){
    tmp = string("cp ").append(myCfg._histoDir).append("/*.root ").append(dirA);
    resD = system(tmp.c_str());
    cout << "Directory creation result: " << resA << "/" << resB << "/" << resC << "/" << resD << endl;
  }
  else
    cout << "Directory creation result: " << resA << "/" << resB << "/" << resC << endl;

  return;
}

void usage(void){
  cout << endl << "*****************************************************************************************************" << endl;
  cout << "Usage: MultiProcessor <config file>" << endl;
  cout << "Read the MultiProcessor.README file to learn about the configuration file settings " << endl;
  cout << "*****************************************************************************************************" << endl << endl;
}

int main(int argc, char **argv){

  const char* inConfig = "";
  if(argc>1){
    inConfig = argv[1];
  }
  else{ usage(); return 0;}

  Configuration myConfig(inConfig);
  myConfig.print();

  if(!myConfig.configStatus()){
    cout << "Configuration Failed: Check your config file!" << endl;
    return 0;
  }

  createDirectories(myConfig);

  VbaseAnalysisRunner* iAna = 0;
  if(myConfig._processorType == "VHLeptonic"){
    iAna = new VHAnalysisRunner(myConfig._analysisName);
  }
  else if(myConfig._processorType == "WtbLeptonic"){
    iAna = new WtbLeptonicAnalysisRunner(myConfig._analysisName);
  }
  else if(myConfig._processorType == "WtbHadronic"){
    iAna = new WtbHadronicAnalysisRunner(myConfig._analysisName);
  }
  else if(myConfig._processorType == "VHHadronic"){
    iAna = new VHqqbbAnalysisRunner(myConfig._analysisName);
  }
  else if(myConfig._processorType == "YXH"){
    iAna = new YXHAnalysisRunner(myConfig._analysisName);
  }
  else if(myConfig._processorType == "TaggerSF"){
    iAna = new TaggerSFAnalysisRunner(myConfig._analysisName);
  }

  if(!iAna){
    cout << "MultiProcessor: Failed to assign Analysis Class" << endl;
    return 0;
  }

  iAna->setReleaseDir(myConfig._releaseTag);
  iAna->setInputListTag(myConfig._inputTag);
  iAna->setOutputWSTag(myConfig._outputTag);
  iAna->setTagNominal(myConfig._nominalTag);
  iAna->setTagData(myConfig._dataTag);
  iAna->setTagUp(myConfig._systUpTag);
  iAna->setTagDown(myConfig._systDnTag);

  //set up the calculation
  iAna->doPull(myConfig._doPull);
  iAna->doLimit(myConfig._doLimit);
  iAna->doPValue(myConfig._doPValue);
  iAna->setCalculatorType(myConfig._calcType);
  iAna->setTestStatType(myConfig._testStat);
  iAna->useProof(myConfig._useProof);
  iAna->setNToys(myConfig._nToys);
  iAna->setNWorkers(myConfig._nWorkers);
  //Set asimovData to TRUE for data = bkgd_sum
  iAna->setDoObserved(!(myConfig._setAsimovData));


  //Add custom POI scans
  for(uint i=0; i<myConfig._poiMass.size(); i++){
    iAna->limitRunner().setPOIScan(myConfig._poiMass[i],myConfig._poiSteps[i],myConfig._poiScanLo[i],myConfig._poiScanHi[i]);
  }

  if(myConfig._runFromWS){
    //If we already have the WS, we can just use it to run immediately
    iAna->runFromFile(myConfig._wsFileName);
  }
  else{

    //If we don't have the WS, just build from the histogram inputs
    if(myConfig._cxaodSource){
      iAna->setCxAODInputPath(myConfig._cxaodFileName);
      getCxAODhistMap(myConfig,iAna);
    }
    else{
      getHistMap(myConfig);
    }

    //For each channel/region listed, create a channel
    for(uint rr=0; rr<myConfig._regionNames.size(); rr++){
      iAna->addChannel(myConfig._regionNames[rr],"");

      //Turn on stat errors as NPs
      iAna->channel(myConfig._regionNames[rr]).setStatErrorThreshold(myConfig._statThresh); //ignore any MC stat errs less than 5%

      //Add each signal listed, for each mass in the specified range
      for(uint ss=0; ss<myConfig._sigNames.size(); ss++){
	for(uint mm=0; mm<myConfig._massList.size(); mm++){

	  string thisSigName = myConfig._sigNames[ss];
	  thisSigName.append(to_string((int)(myConfig._massList[mm])));

	  if(nominalExists(myConfig,myConfig._regionNames[rr],thisSigName)){
	    iAna->channel(myConfig._regionNames[rr]).addSample(thisSigName);
	  }
	  else continue;

	  //Allow the signal strength to vary over [-100,1e6]
	  for(uint pn =0; pn<myConfig._poiNames.size(); pn++){
      bool addHere = myConfig._poiApplyToSig[pn] == "All";
      addHere |= myConfig._sigNames[ss] == myConfig._poiApplyToSig[pn];
      if (addHere) {
  	    iAna->channel(myConfig._regionNames[rr]).sample(thisSigName).multiplyBy(myConfig._poiNames[pn], 1.0, myConfig._poiLoRange[pn], myConfig._poiHiRange[pn]);
      }
	  }

	  //Turn on MC stat errs for this sample
	  iAna->channel(myConfig._regionNames[rr]).sample(thisSigName).setUseStatError(false);

	  //Add shape systematics from TH1 format
	  for(auto &syst : myConfig._shapeSystNames){
	    int retval = includeSyst(myConfig, myConfig._regionNames[rr], thisSigName, syst.first.Data());
	    if(retval==1){
	      iAna->addOneSideVariation(syst.first.Data(), myConfig._systUpTag, myConfig._systDnTag);
	    }
	    if(retval>0) iAna->channel(myConfig._regionNames[rr]).sample(thisSigName).addVariation(syst.first.Data(), myConfig._systUpTag, myConfig._systDnTag);
	  }

	  //Add flat, Log-Normal Prior systematics from specified list
	  for(auto syst : myConfig._normSystNames){
	    applyNormSyst(syst, iAna->channel(myConfig._regionNames[rr]).sample(thisSigName), myConfig._sigNames[ss], myConfig._regionNames[rr],0);
	  }
	  for(auto syst : myConfig._shapeToNormSystNames){
	    applyNormSyst(syst.Data(), iAna->channel(myConfig._regionNames[rr]).sample(thisSigName), myConfig._sigNames[ss], myConfig._regionNames[rr],0);
	  }

	  //Add flat, floating systematics from specified list
	  for(uint syst=0; syst<myConfig._floatSystNames.size(); syst++){
	    applyNormSyst(myConfig._floatSystNames[syst], iAna->channel(myConfig._regionNames[rr]).sample(thisSigName), myConfig._sigNames[ss], myConfig._regionNames[rr],1);
	  }

	  //turn off stat errors for the signal
	  iAna->channel(myConfig._regionNames[rr]).sample(thisSigName).setUseStatError(false);

	  //Register this signal
	  iAna->addSignal(thisSigName,myConfig._massList[mm]);
	}
      }

      //Add each background listed
      for(uint bb=0; bb<myConfig._bkgdNames.size(); bb++){
	if(nominalExists(myConfig,myConfig._regionNames[rr],myConfig._bkgdNames[bb])){
	  iAna->channel(myConfig._regionNames[rr]).addSample(myConfig._bkgdNames[bb]);
	}
	else continue;

	//Turn on MC stat errs for this sample
	iAna->channel(myConfig._regionNames[rr]).sample(myConfig._bkgdNames[bb]).setUseStatError(true);

	//Add shape systematics from TH1 format
	for(auto &syst : myConfig._shapeSystNames){
	  int retval = includeSyst(myConfig, myConfig._regionNames[rr], myConfig._bkgdNames[bb], syst.first.Data());
	  if(retval==1){
	    iAna->addOneSideVariation(syst.first.Data(), myConfig._systUpTag, myConfig._systDnTag);
	  }
	  if(retval>0) iAna->channel(myConfig._regionNames[rr]).sample(myConfig._bkgdNames[bb]).addVariation(syst.first.Data(), myConfig._systUpTag, myConfig._systDnTag);
	}

	//Add flat, Log-Normal Prior systematics from specified list
	for(auto syst : myConfig._normSystNames){
	  applyNormSyst(syst, iAna->channel(myConfig._regionNames[rr]).sample(myConfig._bkgdNames[bb]), myConfig._bkgdNames[bb], myConfig._regionNames[rr],0);
	}
	for(auto syst : myConfig._shapeToNormSystNames){
	  applyNormSyst(syst.Data(), iAna->channel(myConfig._regionNames[rr]).sample(myConfig._bkgdNames[bb]), myConfig._bkgdNames[bb], myConfig._regionNames[rr],0);
	}

	//Add flat, floating systematics from specified list
	for(uint syst=0; syst<myConfig._floatSystNames.size(); syst++){
	  applyNormSyst(myConfig._floatSystNames[syst], iAna->channel(myConfig._regionNames[rr]).sample(myConfig._bkgdNames[bb]), myConfig._bkgdNames[bb], myConfig._regionNames[rr],1);
	}

      }
    }

    //Will we always use mu?  Maybe
    iAna->clearPOI();
    for(uint pn =0; pn<myConfig._poiNames.size(); pn++){
      iAna->addPOI(myConfig._poiNames[pn]);
    }

    //Apply any scale factors the user has specified
    for( auto &ks : myConfig._sigScales) iAna->scaleSignal(ks.second,ks.first);
    for( auto &kb : myConfig._bkgdScales) iAna->scaleBkgd(kb.second,kb.first);
    if(myConfig._dataScale!=1) iAna->scaleData(myConfig._dataScale);
    if(myConfig._lumiScale!=1) iAna->setLumiRescale(1.0, myConfig._lumiScale);
    if(myConfig._glSigScale!=1) iAna->scaleSignal(myConfig._glSigScale, "All");

    //Any rebinning needed?
    for(auto &rb : myConfig._rebinFactors)
      iAna->setRebinFactor(rb.first, rb.second);

    //Merge regions?
    for(auto &mCom : myConfig._mergeRegions) {
      iAna->setMergeRegionCommand(mCom.first,mCom.second,myConfig._mergeRegionFlag[mCom.first],myConfig._mergedRegionName[mCom.first]);
    }

    //Rename regions?
    for(auto &mCom : myConfig._renameRegion) {
      iAna->setRenameRegionCommand(mCom.first,mCom.second);
    }

    //Merge backgrounds
    for(auto &mCom : myConfig._mergeBkgds) {
      iAna->setMergeBkgdCommand(mCom.first,mCom.second);
    }

    //Limit the histogram region?
    for(auto uBound : myConfig._upperBound)
      iAna->setHistoUpperBound(uBound.first, uBound.second);

    for(auto lBound : myConfig._lowerBound)
      iAna->setHistoLowerBound(lBound.first, lBound.second);

    //Include over/underflow?
    iAna->setIncludeOverflow(myConfig._useOverflow);
    iAna->setIncludeUnderflow(myConfig._useUnderflow);

    //apply syst sculpting?
    // 1 = smooth only
    // 2 = remove normalization
    // 3 = smooth and remove norm
    // 4 = average up/down
    // 5 = average and smooth
    // 6 = average and remove norm
    // 7 = average, smooth, and remove norm
    iAna->doApplySmoothing(myConfig._doSmoothing); //global flag
    for(auto &syst : myConfig._shapeSystNames){ //individual flags
      if(syst.second==1 || syst.second==3 || syst.second==5 || syst.second==7)
	iAna->setSystSmoothFlag(syst.first, 1);

      if(syst.second==2 || syst.second==3 || syst.second==6 || syst.second==7)
	iAna->setSystNormFlag(syst.first, 1);

      if(syst.second==4 || syst.second==5 || syst.second==6 || syst.second==7)
	iAna->addAveragedVariation(syst.first.Data(), myConfig._systUpTag, myConfig._systDnTag);
    }

    //Report low stats histos
    cout << endl << "************************************************" << endl;
    cout << "  Histograms with stat error > 50% of integral" << endl;
    cout << "************************************************" << endl;
    for(auto hist : myConfig._lowStatHistos)
      cout << hist.first << endl;
    cout << "************************************************" << endl;


    //If we've specified a calculation, do it!
    if(myConfig._doPull || myConfig._doLimit || myConfig._doPValue) iAna->run();

    if(myConfig._doPull){
      PullPlotter pPlot("vhPullPlots",myConfig._diagDir);
      for(uint mm=0; mm<myConfig._massList.size(); mm++){
	pPlot.addOneMassPoint(myConfig._massList[mm]);
      }
      pPlot.setOutputFormat(PullPlotter::OutputFormat::pdf);
      pPlot.process(iAna->getStatResults());
    }

    //If we're just making a WS, then just do that
    if(myConfig._produceWS){
      if(myConfig._sigNames.size() == 1 ) iAna->produceWS();
      else iAna->produceWS_CombinedSignals();
    }
  }

  return 0;
}
