#ifndef __RF_VbaseAnalysisRunner_h__
#define __RF_VbaseAnalysisRunner_h__

///
/// \brief VbaseAnalysisRunner - base class for W'/Z' analysisrunners, derived from VH usage
/// \author Wade Fisher
/// \date June 22 2015
///
/// VbaseAnalysisRunner is a base class only
///

#include "ResonanceFinder/IBinnedAnalysisRunner.h"

namespace RF {
   class VbaseAnalysisRunner : public IBinnedAnalysisRunner {
   public:
      VbaseAnalysisRunner(TString analysis);
      ~VbaseAnalysisRunner();

      virtual void configInputGetter()=0;
      virtual void manipulate()=0;

      void setTagNominal(TString tag);
      void setTagData(TString tag);
      void setTagUp(TString tag);
      void setTagDown(TString tag);

      void setRebinFactor(TString chan, int factor);
      void setMergeRegionCommand(TString chan, vector<TString> mer_chans, int flag, TString merged_chan_name);
      void setMergeBkgdCommand(TString mergedname, vector<TString> mer_bkgds);
      void setRenameRegionCommand(TString chan, TString new_chan);

      void doApplySmoothing(bool doSmooth);
      void setSystSmoothFlag(TString syst, int flag);
      void setSystNormFlag(TString syst, int flag);

      void setIncludeOverflow(bool use);
      void setIncludeUnderflow(bool use);

      void setHistoUpperBound(TString chan, double value);
      void setHistoLowerBound(TString chan, double value);

      void scaleSignal(double sf,TString name);
      void scaleBkgd(double sf, TString name);
      void scaleData(double sf);
      void setLumiRescale(Double_t oldLumi, Double_t newLumi);      

      void addOneSideVariation(TString variation, TString tagUp, TString tagDn);
      void addAveragedVariation(TString variation, TString tagUp, TString tagDn);

      void setCxAODInputPath(TString path);

      map<TString, bool> getSystNames();

      RF::Histo_t* rebinHisto(int flag, TString chan, Histo_t* totBkgd, vector<double>& fBinEdges);


  protected:
      void rebinHistos();
      RF::Histo_t* autoRebin(int flag, TString chan, RF::HistoCollection& hc, RF::HistoMap_t& hm);
      virtual bool createBin(int bin, int lastbin, TString chan, int flag, Histo_t* totBkgd)=0;
      void applyHistoCorrections();
      void manageHistos();

      void generateTotBkgds();
      void mergeBkgds();

      void renameRegions();//
      void recalculateNormSysts(Sample& addSamp, double addInt, Sample& totSamp, double combInt);
      bool regionHasBeenRenamed(TString chan);

      virtual void mergeRegions()=0;
      virtual bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin)=0;
      virtual void mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map)=0;

      //Variables used to interface with the CxAOD input format
      TString m_CxAODInputPath;
      bool m_useCxAODformat;

      map<TString, double> m_sigScales;
      map<TString, double> m_bkgdScales;
      map<TString, double> m_rebinFactors;
      map<TString, vector<TString>>  m_mergeRegions;
      map<TString, int>  m_mergeRegionFlag;
      map<TString,TString> m_mergedRegionName;
      map<TString, TString> m_renameRegion;
      map<TString, bool> m_systNames;
      map<TString, vector<TString>>  m_mergeBkgds;

      map<TString, int> m_systSmoothFlags;
      map<TString, int> m_systNormFlags;

      map<TString, double> m_upperBound;
      map<TString, double> m_lowerBound;

      vector<TString> m_onesidevar;
      vector<TString> m_onesidetagUp;
      vector<TString> m_onesidetagDn;

      vector<TString> m_avgvar;
      vector<TString> m_avgtagUp;
      vector<TString> m_avgtagDn;

      double m_dataScale;
      double m_oldLumi;
      double m_newLumi;
      
      bool m_doSmooth;
      bool m_useOverflow;
      bool m_useUnderflow;
  };
}

#endif
