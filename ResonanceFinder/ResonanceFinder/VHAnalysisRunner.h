#ifndef __RF_VHAnalysisRunner_h__
#define __RF_VHAnalysisRunner_h__

///
/// \brief VHAnalysisRunner - binned histogram class for the VH analysis
/// \author Wade Fisher
/// \date June 22 2015
///
/// VHAnalysisRunner runs the VH analysis using histograms
///

#include "ResonanceFinder/VbaseAnalysisRunner.h"

namespace RF {
   class VHAnalysisRunner : public VbaseAnalysisRunner {
   public:
      VHAnalysisRunner(TString analysis);
      ~VHAnalysisRunner();
      
      void configInputGetter();
      void manipulate();
      
  private:
      
      bool createBin(int bin, int lasbin, TString chan, int flag, Histo_t* totBkgd);
      double HistReso(double mVH, int region, double minBin, float xMassReso);
      int rebinRegion(TString channel);
      
      void mergeRegions();
      void mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map);
      bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin);
      bool regionHasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map);


  };
}

#endif
