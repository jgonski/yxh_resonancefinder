#ifndef __RF_AverageVariationAdder_h__
#define __RF_AverageVariationAdder_h__

///
/// \brief AverageVariationAdder - average two variations in HistoCollection
/// \author Wade Fisher -- Michigan State
/// \date March 9 2017
///
/// AverageVariationAdder is a simple class which takes an HistoCollection as an input,
/// and averages the up and down variations, then symmetrizes them:
///     hUp->Add(hDown,-1);
///     hUp->Scale(0.5);
///     hDown = (Histo_t*)(hUp->Clone(name));
///     hDown->Scale(-1)

#include "ResonanceFinder/IHistoManipulator.h"

namespace RF {
   class AverageVariationAdder : public IHistoManipulator {
   public:
      AverageVariationAdder();
      ~AverageVariationAdder();

      void setVariation(TString name_up, TString name_down);
      TString variation_up() const;
      TString variation_down() const;

      virtual void manipulate(HistoCollection &hc);

   private:
      TString m_variation_up;
      TString m_variation_down;
   };
}

#endif
