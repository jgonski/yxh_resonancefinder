
#ifndef __RF_WtbHadronicAnalysisRunner_h__
#define __RF_WtbHadronicAnalysisRunner_h__

///
/// \brief WtbHadronicAnalysisRunner - binned histogram class for the WtbHadronic analysis
/// \author Wade Fisher
/// \date June 22 2015
///
/// WtbHadronicAnalysisRunner runs the WtbHadronic analysis using histograms
///

#include "ResonanceFinder/VbaseAnalysisRunner.h"

namespace RF {
   class WtbHadronicAnalysisRunner : public VbaseAnalysisRunner {
   public:
      WtbHadronicAnalysisRunner(TString analysis);
      ~WtbHadronicAnalysisRunner();
      
      void configInputGetter();
      void manipulate();
      
  private:
      
      bool createBin(int bin, int lasbin, TString chan, int flag, Histo_t* totBkgd);
      double HistReso(double mass, int region, double minBin);
      int rebinRegion(TString channel);
      
      void mergeRegions();
      void mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map);
      bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin);
      bool regionHasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map);

  };
}

#endif
