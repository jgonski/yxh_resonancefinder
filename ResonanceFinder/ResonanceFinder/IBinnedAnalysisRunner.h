#ifndef __RF_IBinnedAnalysisRunner_h__
#define __RF_IBinnedAnalysisRunner_h__

///
/// \brief IBinnedAnalysisRunner - interface class for BinnedAnalysisRunner objects
/// \author Valerio Ippolito - Harvard University
/// \date Mar 23, 2015
///
/// IBinnedAnalysisRunner is an extension of IAnalysisRunner which deals with binned
/// workspace production. It relies on IHistoCollector.
///

#include "ResonanceFinder/IAnalysisRunner.h"

#include "ResonanceFinder/IHistoCollector.h"
#include "ResonanceFinder/BinnedWorkspaceBuilder.h"

namespace RF {
   class IBinnedAnalysisRunner : public IAnalysisRunner {
   public:
      inline IBinnedAnalysisRunner(TString analysis, IWorkspaceBuilder *workspaceBuilder, IHistoCollector *histoCollector = nullptr);
      inline IBinnedAnalysisRunner(TString analysis, IHistoCollector *histoCollector = nullptr);
      inline virtual ~IBinnedAnalysisRunner() = 0;

   protected:
      inline void getInput();
      virtual void configInputGetter() = 0;
      virtual void manipulate() = 0;
      inline virtual void configWSBuilder(TString signal);

      inline void reportYields();
      inline void graphSignalEff();
      inline void graphSignalEff(map<TString,double>& sigScales);

      inline HistoCollection &histoCollection();
      IHistoCollector *m_histoCollector;
   };
}

RF::IBinnedAnalysisRunner::IBinnedAnalysisRunner(TString analysis, RF::IWorkspaceBuilder *workspaceBuilder, RF::IHistoCollector *histoCollector): IAnalysisRunner(analysis, workspaceBuilder), m_histoCollector(histoCollector)
{
  TH1::SetDefaultSumw2(); // needed for all histogram errors to make sense
}

RF::IBinnedAnalysisRunner::IBinnedAnalysisRunner(TString analysis, RF::IHistoCollector *histoCollector) : IAnalysisRunner(analysis, new RF::BinnedWorkspaceBuilder("")), m_histoCollector(histoCollector)
{
  TH1::SetDefaultSumw2(); // needed for all histogram errors to make sense
}

RF::IBinnedAnalysisRunner::~IBinnedAnalysisRunner()
{
   if (m_histoCollector) delete m_histoCollector;
}

void RF::IBinnedAnalysisRunner::getInput()
{
   // first, get standard HC
   TString input_txt_path = releaseDir() + "/" + analysis() + "/data/" + inputListTag();
   m_histoCollector->setSource(input_txt_path);

   configInputGetter();

   for (auto chan : channels()) {
      for (auto sample : channel(chan).samples()) {
         m_histoCollector->addSample(chan, sample);
      }
   }

   // then, manipulate it
   manipulate();

   // now, the HC is final
   reportYields();

   // persist it to file for diagnostics
   TString output_diagnostic_path = getDiagnosticsDir();
   m_histoCollector->getHistoCollection().persist(output_diagnostic_path + "/histos_" + analysis() + "_" + outputWSTag() + ".root");
}

RF::HistoCollection &RF::IBinnedAnalysisRunner::histoCollection()
{
   return m_histoCollector->getHistoCollection();
}

void RF::IBinnedAnalysisRunner::configWSBuilder(TString signal)
{
   RF::BinnedWorkspaceBuilder *bwb = dynamic_cast<RF::BinnedWorkspaceBuilder*>(m_workspaceBuilder);
   bwb->setName(signal);
   bwb->setHistoCollection(histoCollection());
   bwb->setOutputFilePrefix(releaseDir() + "/" + analysis() + "/tmp/" + outputWSTag() + "/" + signal + "_test");
}

void RF::IBinnedAnalysisRunner::graphSignalEff(){
  map<TString, double> aMap;
  graphSignalEff(aMap);
}

void RF::IBinnedAnalysisRunner::graphSignalEff(map<TString,double>& sigScales){
  RF::HistoCollection &hc = histoCollection();
  unordered_set<string> tmpNames;
  unordered_set<double> tmpMasses;
  for( auto sig : signalMap()){
    TString aSig = sig.first;
    aSig.ReplaceAll(TString::Itoa((int)sig.second,10),"");
    tmpNames.insert(aSig.Data());
    tmpMasses.insert(sig.second);
  }
  vector<TString> sigNames;
  vector<double> allMasses;
  for(auto kv : tmpNames) sigNames.push_back(kv.c_str());
  for(auto kv : tmpMasses) allMasses.push_back(kv);
  sort(sigNames.begin(),sigNames.end());
  sort(allMasses.begin(),allMasses.end());
  
  TString output_diagnostic_path = getDiagnosticsDir();
  TFile* fout = new TFile(output_diagnostic_path + "/signalEfficiencyGraphs.root","RECREATE");
  vector<Double_t> vMass;
  vector<Double_t> vEff;
  for( auto chan : hc.channels()){
    for( auto sig : sigNames){
      vMass.clear(); vEff.clear();
      for(auto mass : allMasses){
	TString tSamp = sig;
	tSamp.Append(TString::Itoa((int)mass,10));
	
	double norm = 1;
	if(sigScales.find(tSamp) != sigScales.end()) norm = sigScales[tSamp];
	
	Histo_t* aHist = hc.getHistogram(chan,tSamp,"nominal");
	if(aHist){
	  vMass.push_back(mass);
	  vEff.push_back(aHist->Integral()/(norm+1e-9));
	}
      }
      if(vMass.size()>0){
	TGraph gr(vMass.size(),&vMass[0],&vEff[0]);
	TString grname = chan;
	grname.Append("_").Append(sig);
	gr.SetName(grname);
	gr.SetTitle(grname);
	gr.Write();
      }
    }
  }
  fout->Write();
  fout->Close();
  delete fout;
  
}

void RF::IBinnedAnalysisRunner::reportYields(){

  RF::HistoCollection &hc = histoCollection();
  cout << endl<< endl;
  for( auto chan : hc.channels()){

    cout << "*******************************************" << endl;
    cout << "  Yield Report for Channel " << chan << endl;
    cout << "*******************************************" << endl;
    
    Histo_t* totBkgd = 0;
    for( auto samp : hc.samples(chan)){
      if(samp == hc.tagData()) continue;
      if(samp.Contains("totalBkgd")) continue;
      Histo_t* aHist = hc.getHistogram(chan,samp,"nominal");

      if(aHist){
	double tot, totErr;
	tot = aHist->IntegralAndError(1,aHist->GetNbinsX(),totErr);
	cout << samp << ": \t" << tot << " +/- " << totErr << endl;

	//Check if this is a signal
	for(auto sig : signals()){
	  if(samp.EqualTo(sig)) continue;
	  if(samp.Contains("totalBkgd")) continue;
	  if(totBkgd == 0) totBkgd = (Histo_t*)(aHist->Clone(TString("totBkgd").Append(chan)));
	  else totBkgd->Add(aHist);
	}
      }
      else cout << "No valid histo for " << samp<<endl;
    }
    if(totBkgd) cout << "Total Bkgd: \t" << totBkgd->Integral() << endl;
    cout << "Data: \t" << hc.getData(chan)->Integral() << endl;
  }
  cout << endl;
}

#endif
