#ifndef __RF_TaggerSFAnalysisRunner_h__
#define __RF_TaggerSFAnalysisRunner_h__


// TaggerSFAnalysisRunner class is based on VHAnalysisRunner class


///
/// \brief VHAnalysisRunner - binned histogram class for the VH analysis
/// \author Wade Fisher
/// \date June 22 2015
///
/// VHAnalysisRunner runs the VH analysis using histograms
///

#include "ResonanceFinder/VbaseAnalysisRunner.h"

namespace RF {
   class TaggerSFAnalysisRunner : public VbaseAnalysisRunner {
   public:
      TaggerSFAnalysisRunner(TString analysis);
      ~TaggerSFAnalysisRunner();
      
      void configInputGetter();
      void manipulate();
      
  private:
      
      bool createBin(int bin, int lasbin, TString chan, int flag, Histo_t* totBkgd);
      
      void mergeRegions();
      void mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map);
      bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin);
      bool regionHasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map);


  };
}

#endif
