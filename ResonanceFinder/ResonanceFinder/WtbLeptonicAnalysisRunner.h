
#ifndef __RF_WtbLeptonicAnalysisRunner_h__
#define __RF_WtbLeptonicAnalysisRunner_h__

///
/// \brief WtbLeptonicAnalysisRunner - binned histogram class for the WtbLeptonic analysis
/// \author Wade Fisher
/// \date June 22 2015
///
/// WtbLeptonicAnalysisRunner runs the WtbLeptonic analysis using histograms
///

#include "ResonanceFinder/VbaseAnalysisRunner.h"

namespace RF {
   class WtbLeptonicAnalysisRunner : public VbaseAnalysisRunner {
   public:
      WtbLeptonicAnalysisRunner(TString analysis);
      ~WtbLeptonicAnalysisRunner();
      
      void configInputGetter();
      void manipulate();
      
  private:
      
      bool createBin(int bin, int lasbin, TString chan, int flag, Histo_t* totBkgd);
      double HistReso(double mVH, int region, double minBin);
      int rebinRegion(TString channel);
      
      void mergeRegions();
      void mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map);
      bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin);
      bool regionHasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map);

  };
}

#endif
