#ifndef __RF_VVlvqqAnalysisRunner_h__
#define __RF_VVlvqqAnalysisRunner_h__

///
/// \brief VVlvqqAnalysisRunner - runner for the lvqq analysis
/// \author Valerio Ippolito - Harvard University
/// \date Apr 18, 2015
///
/// VVlvqqAnalysisRunner runs the VVlvqq analysis
///

#include "ResonanceFinder/IBinnedTreeAnalysisRunner.h"

#include <RooAbsPdf.h>
#include <RooRealVar.h>

namespace RF {
   class VVlvqqAnalysisRunner : public IBinnedTreeAnalysisRunner {
   public:
      VVlvqqAnalysisRunner(TString analysis);
      ~VVlvqqAnalysisRunner();

      void doApplySmoothing(bool doSmooth);
      void setSystSmoothFlag(TString syst, int flag);
      bool m_doSmooth;
      map<TString, int> m_systSmoothFlags;
      void setLumiRescale(Double_t oldLumi, Double_t newLumi);
      void setChannelFitFunction( TString channel, TString name, TString obs);
      void setChannelFitFunction(TString channel, RooAbsPdf *pdf, RooRealVar *obs); 
      virtual void addOneSideVariation(TString variation);
      vector<TString> m_onesidevar;
      virtual void manipulate();
      map<TString, TString> m_ChFunctionMap;
      map<TString, TString> m_ChObsMap;
      map<TString, RooAbsPdf*> m_pdf;
      map<TString, RooRealVar*> m_obs;
      bool m_useStrings;
   private: 
      double m_oldLumi;
      double m_newLumi;

   };
}

#endif

