#ifndef __RF_YXHAnalysisRunner_h__
#define __RF_YXHAnalysisRunner_h__

///
/// \brief YXHAnalysisRunner - binned histogram class for the VH analysis
/// \author Wade Fisher
/// \date June 22 2015
///
/// YXHAnalysisRunner runs the VH analysis using histograms
///

#include "ResonanceFinder/VbaseAnalysisRunner.h"

namespace RF {
   class YXHAnalysisRunner : public VbaseAnalysisRunner {
   public:
      YXHAnalysisRunner(TString analysis);
      ~YXHAnalysisRunner();

      void configInputGetter();
      void manipulate();


  private:
      void rebinHistosCustom();
      
      //double HVTreso(int region, double mVH, double minBin);

      //tmp will need to be removed!!
      //double HistReso(double mVH, int region, double minBin);
      //int rebinRegion(TString channel);
      bool createBin(int bin, int lasbin, TString chan, int flag, Histo_t* totBkgd);
      void mergeRegions();
      void mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map);
      bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin);
      //bool regionHasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map);
      
   
  };
}

#endif
