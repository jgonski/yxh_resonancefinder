#ifndef __RF_CxAODHistoCollector_h__
#define __RF_CxAODHistoCollector_h__

///
/// \brief CxAODHistoCollector - a class ordering a list of histograms into a HistoCollection
/// \author Wade Fisher
/// \date June 23, 2016
///
/// CxAODHistoCollector implements IHistoCollector for input from histograms.
///
/// This HistoCollector deviates from the traditional sense to allow interfacing of the CxAOD histogram file input format.
///

#include "ResonanceFinder/IHistoCollector.h"
#include "TKey.h"

namespace RF {
   class CxAODHistoCollector : public IHistoCollector {
   public:
      CxAODHistoCollector();
      CxAODHistoCollector(TString path, HistoCollection& hc);
      ~CxAODHistoCollector();

      virtual void addSample(TString channel, TString sample);
      virtual void addSample(TString channel, TString sample, TString variation, TString hname, TString fname);
      
      void collectHistograms();
      inline void setFilePath(TString path){ m_filePath = path; }
  private:
      void matchKey(TKey* key);

      TString m_filePath;
   };
}

#endif

//
