#include "ResonanceFinder/VbaseAnalysisRunner.h"
#include "ResonanceFinder/SelectiveHistoCollector.h"
#include "ResonanceFinder/CxAODHistoCollector.h"
#include "ResonanceFinder/HistoLumiRescaler.h"
#include "ResonanceFinder/DownVariationAdder.h"
#include "ResonanceFinder/AverageVariationAdder.h"
#include "ResonanceFinder/SystematicsSmoother.h"

RF::VbaseAnalysisRunner::VbaseAnalysisRunner(TString analysis) : RF::IBinnedAnalysisRunner(analysis, new RF::SelectiveHistoCollector())
{  
  m_dataScale = 1;
  m_oldLumi = 1;
  m_newLumi = 1;

  m_useUnderflow = 0;
  m_useOverflow = 1;

  m_doSmooth = false;
  m_useCxAODformat = false;
}

RF::VbaseAnalysisRunner::~VbaseAnalysisRunner()
{
}

void RF::VbaseAnalysisRunner::setCxAODInputPath(TString path){
  if(path.Length()<2) return;
  m_CxAODInputPath = path;
  m_useCxAODformat = true;

  RF::CxAODHistoCollector *chc = new CxAODHistoCollector(path, histoCollection());
  if(m_histoCollector) { delete m_histoCollector; m_histoCollector = NULL; }
  m_histoCollector = chc;
}

map<TString, bool> RF::VbaseAnalysisRunner::getSystNames(){
  map<TString, bool> systNames = m_systNames;
  return systNames;
}

void RF::VbaseAnalysisRunner::setRebinFactor(TString chan, int factor) { m_rebinFactors[chan] = factor; }

void RF::VbaseAnalysisRunner::setMergeRegionCommand(TString reg, vector<TString> mer_reg, int flag, TString merged_reg_name) {

  m_mergeRegions[reg]=mer_reg;
  m_mergeRegionFlag[reg]=flag;
  m_mergedRegionName[reg]=merged_reg_name;
}

void RF::VbaseAnalysisRunner::setMergeBkgdCommand(TString name, vector<TString> to_merge){
  m_mergeBkgds[name] = to_merge;
}

void RF::VbaseAnalysisRunner::setRenameRegionCommand(TString reg, TString new_reg) {
  m_renameRegion[reg]=new_reg;
}

void RF::VbaseAnalysisRunner::doApplySmoothing(bool doSmooth) { m_doSmooth = doSmooth; }

void RF::VbaseAnalysisRunner::setSystSmoothFlag(TString syst, int flag){
  m_systSmoothFlags[syst] =  flag;
}

void RF::VbaseAnalysisRunner::setSystNormFlag(TString syst, int flag){
  m_systNormFlags[syst] =  flag;
}

void RF::VbaseAnalysisRunner::setLumiRescale(Double_t oldLumi, Double_t newLumi) {
  m_oldLumi = oldLumi;
  m_newLumi = newLumi;
}

void RF::VbaseAnalysisRunner::setIncludeOverflow(bool use){ m_useOverflow = use; }

void RF::VbaseAnalysisRunner::setIncludeUnderflow(bool use){ m_useUnderflow = use; }

void RF::VbaseAnalysisRunner::setHistoUpperBound(TString chan, double value){
  m_upperBound[chan] = value;
}

void RF::VbaseAnalysisRunner::setHistoLowerBound(TString chan, double value){
  m_lowerBound[chan] = value;
}

void RF::VbaseAnalysisRunner::addOneSideVariation(TString variation, TString tagUp, TString tagDn)
{

  if (variation != "") {
    bool alreadyAdded =  std::find(m_onesidevar.begin(), m_onesidevar.end(), variation) != m_onesidevar.end();
    if (!alreadyAdded) {      
      m_onesidevar.push_back(variation);
      m_onesidetagUp.push_back(tagUp);
      m_onesidetagDn.push_back(tagDn);
    }    
  }
  return ;
}

void RF::VbaseAnalysisRunner::addAveragedVariation(TString variation, TString tagUp, TString tagDn)
{

  if (variation != "") {
    bool alreadyAdded =  std::find(m_avgvar.begin(), m_avgvar.end(), variation) != m_avgvar.end();
    if (!alreadyAdded) {      
      m_avgvar.push_back(variation);
      m_avgtagUp.push_back(tagUp);
      m_avgtagDn.push_back(tagDn);
    }    
  }
  return ;
}

void RF::VbaseAnalysisRunner::setTagData(TString tag)
{
  histoCollection().setTagData(tag);
}

void RF::VbaseAnalysisRunner::setTagNominal(TString tag)
{
  histoCollection().setTagNominal(tag);
}

void RF::VbaseAnalysisRunner::setTagUp(TString tag)
{
  histoCollection().setTagUp(tag);
}

void RF::VbaseAnalysisRunner::setTagDown(TString tag)
{
  histoCollection().setTagDown(tag);
}

void RF::VbaseAnalysisRunner::scaleSignal(double sf, TString name){
  if(sf == 1) return;
  if(name.IsNull()) return;

  for( auto sig : signals()){
    if(sig==name){
      if(m_sigScales.find(sig) == m_sigScales.end()){
	m_sigScales[sig] = sf;
      }
      else cout << "Error: already have a scale for this signal: " << name << endl;
    }
  }

  if((name==TString("All")) || (name==TString("all"))){
    if(m_sigScales.find(name) == m_sigScales.end()){
      m_sigScales[name] = sf;   
    }   
  }

}

void RF::VbaseAnalysisRunner::scaleBkgd(double sf, TString name){
  if(sf == 1) return;
  if(name.IsNull()) return;

  if(m_bkgdScales.find(name) == m_bkgdScales.end()) m_bkgdScales[name] = sf;
  else cout << "Error: already have a scale for this background: " << name << endl;

}

void RF::VbaseAnalysisRunner::scaleData(double sf){
  m_dataScale = sf;
}

void RF::VbaseAnalysisRunner::manageHistos(){

  RF::HistoCollection &hc = histoCollection();

  //Address missing histograms and deal with empty variation histograms
  vector<TString> missing;
  for( auto iter : m_histoCollector->samples()){
    TString chan = iter.first;
    //create a template for this channel...I assume they all have the same binning for a given channel
    Histo_t* tHist = 0;
    //find a histogram that exists
    for( auto samp : iter.second){
      if(tHist) continue;
      Histo_t* aHist = hc.getHistogram(chan,samp,hc.tagNominal());
      if(aHist){
	tHist = (Histo_t*)(aHist->Clone("template"));
	tHist->Scale(0);
      }
    }
    if(!tHist){
      cout << "Warning: no valid histograms for channel " << chan << endl;
      continue; //No valid histos for this channel?
    }

    //loop over samples and find missing ones
    for( auto samp : iter.second){
      //if data is not present, we have a legitimate problem.  but it's not filled if it's empty?
      //      if(samp == hc.tagData()) continue;
      
      Histo_t* ihist = 0;      
      if(samp == hc.tagData()){
	ihist = hc.getData(chan);
      }
      else{
	ihist = hc.getHistogram(chan, samp, hc.tagNominal());
      }

      //If the nominal histogram doesn't exist, make it
      if(!ihist){
	TString iname = samp;
	iname.Append("_").Append(chan);
	ihist = (Histo_t*)(tHist->Clone(iname));
	ihist->SetName(iname);
	ihist->SetTitle(iname);
	missing.push_back(iname);

	if(samp == hc.tagData()){
	  hc.addData(ihist,chan);
	}
	else{ 
	  hc.addHistogram(ihist,chan,samp,hc.tagNominal());
	}
      }
      if(samp == hc.tagData()) continue;

      vector<TString> variations = hc.variations(chan, samp);
      Histo_t* nomHist = hc.getHistogram(chan, samp, hc.tagNominal()); //because this exists for sure now
      for(auto ivari : variations){
	if(ivari == hc.tagNominal()) continue;

	ihist = hc.getHistogram(chan,samp,ivari);
	//If the variation histogram doesn't exist, make it and set equal to the nominal
	bool ok = ihist!=NULL;
	if(ok) ok |= ihist->Integral()<1e-6; //if the variation integral is nearly zero or is negative, set it to the nominal.
	if(!ok){
	  TString iname = samp;	  
	  iname.Append("_").Append(chan);
	  iname.Append("_").Append(ivari);
	  if(ivari.Contains(hc.tagUp())) iname.Append(hc.tagUp());
	  else if(ivari.Contains(hc.tagDown())) iname.Append(hc.tagDown());
	  else continue; //???

	  ihist = (Histo_t*)(nomHist->Clone(iname));
	  ihist->SetName(iname);
	  ihist->SetTitle(iname);
	  hc.addHistogram(ihist,chan,samp,ivari);
	  missing.push_back(iname);
	}
	
      }
      
    }
  }
  
  //announce any histos we filled in.
  cout << endl << "***<<<***>>>***!!!***<<<***>>>***!!!***<<<***>>>***" << endl;
  cout << "\t VbaseAnalysisRunner::manipulate()" << endl;
  cout << "\t Missing nominal histograms generated: " << endl;
  for(auto name : missing) cout << name << endl;
  cout << "***<<<***>>>***!!!***<<<***>>>***!!!***<<<***>>>***" << endl << endl;

  int idx = 0;
  for (auto kv : m_onesidevar) {
    // add down variations
    if (kv != "") {
      RF::DownVariationAdder dva;
      dva.setVariation(kv + m_onesidetagUp[idx], kv + m_onesidetagDn[idx]);
      dva.manipulate(hc);
    }
    idx++;
  }

  idx = 0;
  for (auto kv : m_avgvar) {
    // add averaged variations
    if (kv != "") {
      RF::AverageVariationAdder ava;
      ava.setVariation(kv + m_avgtagUp[idx], kv + m_avgtagDn[idx]);
      ava.manipulate(hc);
    }
    idx++;
  }
  

}

void RF::VbaseAnalysisRunner::applyHistoCorrections(){

  RF::HistoCollection &hc = histoCollection();

  // first, rescale to luminosity
  if (m_oldLumi != m_newLumi) {
    RF::HistoLumiRescaler hlr;
    hlr.setLumi(m_oldLumi, m_newLumi);
    hlr.manipulate(hc);
  }


  for (auto &kv : hc.histos()){

    //Address negative/NaN bins                                                                                                                                                           
    double sumNeg = 0;
    double sumTotI = 0;
    double totErrI = 0;
    sumTotI = kv.second->IntegralAndError(1,kv.second->GetNbinsX(),totErrI);
    
    for(int bb=1; bb<=kv.second->GetNbinsX(); bb++){
      if(kv.second->GetBinContent(bb)<0){
        sumNeg += kv.second->GetBinContent(bb);
        kv.second->SetBinContent(bb,0);
        kv.second->SetBinError(bb,0);
      }
    }
    for(int bb=0; bb<=kv.second->GetNbinsX()+1; bb++){
      if(TMath::IsNaN(kv.second->GetBinContent(bb))){
        kv.second->SetBinContent(bb,0);
        kv.second->SetBinError(bb,0);
      }
    }

    //Correct total rate and stat error to match initial histo
    double sumTotF = 0;
    double totErrF = 0;
    sumTotF = kv.second->IntegralAndError(1,kv.second->GetNbinsX(),totErrF);
    if(sumTotI>0 && sumNeg!=0 && sumTotF>0){
      for(int bb=1; bb<=kv.second->GetNbinsX(); bb++){
	kv.second->SetBinContent(bb,kv.second->GetBinContent(bb)*sumTotI/sumTotF);
	kv.second->SetBinError(bb,kv.second->GetBinError(bb)*totErrI/totErrF);
      }
    }
    else if(sumTotI<0) kv.second->Scale(0);

    // apply data scale if it's data                                                                                                                                                  
    if(kv.first.Contains(hc.tagData())) kv.second->Scale(m_dataScale);

    for (auto &ks : m_sigScales){
      if(kv.first.Contains(ks.first)) kv.second->Scale(ks.second);
      else if((ks.first == TString("All")) || (ks.first == TString("all"))){
        for(auto sig : signals()){
          if(kv.first.Contains(sig)){ //this is a signal, oK to scale                                                                                                                  
            kv.second->Scale(ks.second);
          }
        }
      }
    }

    // apply any bkgd scales                                                                                                                                                           
    for (auto &kb : m_bkgdScales){
      for(auto &kc : this->m_channels){
        TString searchStr = kc.first;
        searchStr = searchStr + "_" + kb.first + "_";
        if(kv.first.Contains(searchStr)){
          kv.second->Scale(kb.second);
        }
      }
    }
  }

  //Before smoothing, renomalize any systematics that have been flagged
  TString ichan, isamp, ivar;
  for(auto &kv : hc.histos()){
    Histo_t* hNom = 0;

    if(kv.first.Contains(hc.tagNominal())){
      hNom = kv.second;
      hc.getHistoNameTokens(kv.first, ichan, isamp, ivar);
    }
    else continue;
    
    //look up each syst
    for(auto &syst : m_systNormFlags){
      if(syst.second != 1) continue; 

      Histo_t* hVarU = hc.getHistogram(ichan, isamp, TString(syst.first).Append(hc.tagUp()));
      Histo_t* hVarD = hc.getHistogram(ichan, isamp, TString(syst.first).Append(hc.tagDown()));

      //set normalization equal to nominal
      if(hVarU) hVarU->Scale(hNom->Integral()/(hVarU->Integral()+1e-9));
      if(hVarD) hVarD->Scale(hNom->Integral()/(hVarD->Integral()+1e-9));
    }
  }

  //After all other manipulations, apply systematics smoothing if requested
  if(m_doSmooth){
    RF::SystematicsSmoother SysS;
    SysS.setNumberOfLocalExtrema(1); //1: allows parabolic shape for mVH
    SysS.doApplyAveraging(false); //false: no averaging of adjacent bins
    SysS.setSmoothFlags(m_systSmoothFlags); //provide list of smoothing flags
    SysS.manipulate(hc);
  }  

}

void RF::VbaseAnalysisRunner::rebinHistos(){
  RF::HistoCollection &hc = histoCollection();
  RF::HistoMap_t &hm = hc.histos();

  //Perform uniform rebinnings, as requested
  for( auto &hist : hm){
    for( auto &rb : m_rebinFactors){
      if(rb.second>1 && hist.first.Contains(rb.first)) hist.second->Rebin(rb.second);
    }
    
    //merge overflow if requested
    if(m_useOverflow){
      int lbin = hist.second->GetNbinsX();
      hist.second->AddBinContent(lbin,hist.second->GetBinContent(lbin+1));
      hist.second->SetBinError(lbin, sqrt(pow(hist.second->GetBinError(lbin),2) + pow(hist.second->GetBinError(lbin+1),2)));
      hist.second->SetBinContent(lbin+1,0);
    }
    
    //merge underflow if requested
    if(m_useUnderflow){
      int lbin = 1;
      hist.second->AddBinContent(lbin,hist.second->GetBinContent(lbin-1));
      hist.second->SetBinError(lbin, sqrt(pow(hist.second->GetBinError(lbin),2) + pow(hist.second->GetBinError(lbin-1),2)));
      hist.second->SetBinContent(lbin-1,0);
    }    
  }  

  //Apply upperbound requests
  for( auto &uBound : m_upperBound){
    for( auto samp : hc.samples(uBound.first)){
      vector<TString> variations = hc.variations(uBound.first, samp);
      if(samp == hc.tagData()) variations.push_back("data");
      for( auto var : variations){
	Histo_t* hist = 0;

	if(var == "data") hist = hc.getData(uBound.first); 
	else hist = hc.getHistogram(uBound.first, samp, var);	         

	int ubin = hist->FindBin(uBound.second)-1;
	double sum = 0;
	double sumE = 0;
	for(int bb=ubin+1; bb<=hist->GetNbinsX()+1; bb++){
	  sum += hist->GetBinContent(bb);
	  sumE += pow(hist->GetBinError(bb),2);
	  hist->SetBinContent(bb,0);
	  hist->SetBinError(bb,0);
	}

	if(m_useOverflow){
	  hist->AddBinContent(ubin,sum);
	  hist->SetBinError(ubin,sqrt(sumE + pow(hist->GetBinError(ubin),2)));      
	}
	else{
	  hist->SetBinContent(hist->GetNbinsX()+1,sum);
	  hist->SetBinError(hist->GetNbinsX()+1,sqrt(sumE));
	}
      }
    }
  }

  //Apply lowerbound requests
  for( auto &lbound : m_lowerBound){
    for( auto samp : hc.samples(lbound.first)){
      vector<TString> variations = hc.variations(lbound.first, samp);
      if(samp == hc.tagData()) variations.push_back("data");
      for( auto var : variations){
	Histo_t* hist = 0;

	if(var == "data") hist = hc.getData(lbound.first); 
	else hist = hc.getHistogram(lbound.first, samp, var);	         
	
	int lbin = hist->FindBin(lbound.second)+1;
	double sum = 0;
	double sumE = 0;
	for(int bb=1; bb<=lbin-1; bb++){
	  sum += hist->GetBinContent(bb);
	  sumE += pow(hist->GetBinError(bb),2);
	  hist->SetBinContent(bb,0);
	  hist->SetBinError(bb,0);
	}
	
	if(m_useUnderflow){
	  hist->AddBinContent(lbin,sum);
	  hist->SetBinError(lbin,sqrt(sumE + pow(hist->GetBinError(lbin),2)));      
	}
	else{
	  hist->SetBinContent(0,sum);
	  hist->SetBinError(0,sqrt(sumE));
	}

      }
    }
  }
  
  //Perform variable-size rebinnings
  if(m_rebinFactors.size()>0){
    
    vector<Histo_t*> templates;
    for( auto &rb : m_rebinFactors){
      if(rb.second<1) templates.push_back(autoRebin(rb.second, rb.first, hc, hm));
    }
    
    //create storage container for template histograms
    TString nickname = analysis() + "_" + outputWSTag();  
    TString outHistFileName = getDiagnosticsDir() + "/varBins_" + nickname + ".root";
    TFile* outHistFile = new TFile(outHistFileName,"RECREATE");
    for(auto hh : templates) if(hh){ hh->Write();}
    outHistFile->Write();
    outHistFile->Close();
  }
  
}
  

void RF::VbaseAnalysisRunner::generateTotBkgds(){

  RF::HistoCollection &hc = histoCollection();

  ///Sum up the total backgrounds, careful to not include signals or data
  for( auto chan : hc.channels()){
    Histo_t* totBkgd=0;
  
    for( auto &samp : hc.samples(chan)){
      Histo_t* hist = hc.getHistogram(chan, samp, hc.tagNominal());
      if(samp == hc.tagData()) continue;
      
      if(!hist){ cout << "VbaseAnalysisRunner::generateTotBkgds, Error accessing known histogram: " << samp << endl; continue;}
      
      bool keep=true;
      
      for(auto sig : signals()){
	if(samp.Contains(sig)){ //this is a signal, so don't include it
	  keep = false;
	}
      }
      
      if(samp.Contains(hc.tagData())) keep = false;
      
      if(!keep) continue;
      
      if(totBkgd==0) totBkgd = dynamic_cast<RF::Histo_t*>(hist->Clone(TString(chan).Append("cloneBkgd")));
      else totBkgd->Add(hist);
      
    }
    if(totBkgd){
      hc.addHistogram(totBkgd,chan,"totalBkgd",hc.tagNominal());
    }
    else{ cout << "VbaseAnalysisRunner::generateTotBkgds, Error: no total bkgd histo for " << chan << endl; }
  }
}

RF::Histo_t* RF::VbaseAnalysisRunner::rebinHisto(int flag, TString chan, Histo_t* totBkgd, vector<double>& fBinEdges){
  if(!totBkgd) return NULL;

  fBinEdges.clear();

  //use the total background rate to find bin edges
  vector<double> binEdges;
  double uBound = totBkgd->GetXaxis()->GetXmax();
  if(m_upperBound.find(chan) == m_upperBound.end()){
    binEdges.push_back(totBkgd->GetXaxis()->GetXmax());
  }  
  else{
    binEdges.push_back(m_upperBound[chan]);
    uBound = m_upperBound[chan];
  }

  int maxBin = totBkgd->GetNbinsX();
  if(m_useOverflow) maxBin += 1;
  int lastBin = maxBin;
  for(int bb=maxBin; bb>=1; bb--){
    if(totBkgd->GetBinLowEdge(bb)>uBound) continue;
    if(createBin(bb,lastBin,chan,flag,totBkgd)){
      binEdges.push_back(totBkgd->GetBinLowEdge(bb));
      lastBin = bb;
    }
  }

  //invert convenient top->bottom bin edge ordering
  //  vector<double> fBinEdges;
  cout << endl  << "*********************************************" << endl;
  cout << "||  Variable bin edges for channel " << chan << endl;
  cout << "*********************************************" << endl;
  for(int i=binEdges.size()-1; i>=0; i--){
    fBinEdges.push_back(binEdges[i]);
    cout << "Bin Edge: " << " " << binEdges[i] << endl;
  }
  cout << "******************************************************" << endl << endl;
  
  //Create template to ease bin assignments
  Histo_t* templ = new Histo_t(chan,chan,fBinEdges.size()-1,&fBinEdges[0]);

  return templ;
}

void RF::VbaseAnalysisRunner::recalculateNormSysts(Sample& addSamp, double addInt, Sample& sampCombine, double combInt){
  double min = 0;
  double max = 0;
  
  //Find the ones that are in both
  for(auto addFact : addSamp.factors()){
    bool common = false;
    for(auto combFact : sampCombine.factors()){
      //This one is in both, so average the effect on both
      if(addFact == combFact){
	min = (addSamp.factor(addFact).min()*addInt + sampCombine.factor(combFact).min()*combInt)/(addInt+combInt);
	max = (addSamp.factor(addFact).max()*addInt + sampCombine.factor(combFact).max()*combInt)/(addInt+combInt);
	sampCombine.replaceFactor(combFact,sampCombine.factor(combFact).value(),min,max);
	common = true;
	break;
      }
    }

    //This one is not in the common sample, so add just in the new sample's fraction
    if(!common){
      min = (addSamp.factor(addFact).min()*addInt+combInt)/(addInt+combInt);
      max = (addSamp.factor(addFact).max()*addInt+combInt)/(addInt+combInt);
      sampCombine.multiplyBy(addFact,addSamp.factor(addFact).value(),min,max,RF::MultiplicativeFactor::ConstraintType::LOGNORMAL);
    }
  }
  
  for(auto combFact : sampCombine.factors()){
    bool common = false;
    for(auto addFact : addSamp.factors()){
      //This one is in both, so average the effect on both
      if(addFact == combFact){
	common = true;
	break;
      }
    }
    
    //This one is not in the new sample, so add just in the old sample's fraction
    if(!common){
      min = (sampCombine.factor(combFact).min()*combInt+addInt)/(addInt+combInt);
      max = (sampCombine.factor(combFact).max()*combInt+addInt)/(addInt+combInt);
      sampCombine.replaceFactor(combFact,sampCombine.factor(combFact).value(),min,max);	
    }
  }

  return;
}

RF::Histo_t* RF::VbaseAnalysisRunner::autoRebin(int flag, TString chan, RF::HistoCollection& hc, RF::HistoMap_t& hm){

  Histo_t* totBkgd=hc.getHistogram(chan,"totalBkgd",hc.tagNominal());

  if(!totBkgd){
    cout << "VbaseAnalysisRunner::autoRebin, Error: no total bkgd histo for " << chan << endl; 
    return NULL;
  }
  /*
  //use the total background rate to find bin edges
  vector<double> binEdges;
  double uBound = totBkgd->GetXaxis()->GetXmax();
  if(m_upperBound.find(chan) == m_upperBound.end()){
    binEdges.push_back(totBkgd->GetXaxis()->GetXmax());
  }  
  else{
    binEdges.push_back(m_upperBound[chan]);
    uBound = m_upperBound[chan];
  }

  int maxBin = totBkgd->GetNbinsX();
  if(m_useOverflow) maxBin += 1;
  int lastBin = maxBin;
  for(int bb=maxBin; bb>=1; bb--){
    if(totBkgd->GetBinLowEdge(bb)>uBound) continue;
    if(createBin(bb,lastBin,chan,flag,totBkgd)){
      binEdges.push_back(totBkgd->GetBinLowEdge(bb));
      lastBin = bb;
    }
  }

  //invert convenient top->bottom bin edge ordering
  vector<double> fBinEdges;
  cout << endl  << "*********************************************" << endl;
  cout << "||  Variable bin edges for channel " << chan << endl;
  cout << "*********************************************" << endl;
  for(int i=binEdges.size()-1; i>=0; i--){
    fBinEdges.push_back(binEdges[i]);
    cout << "Bin Edge: " << " " << binEdges[i] << endl;
  }
  cout << "******************************************************" << endl << endl;
  //Create template to ease bin assignments
  Histo_t* templ = new Histo_t(chan,chan,fBinEdges.size()-1,&fBinEdges[0]);
  */
  vector<double> fBinEdges;
  Histo_t* templ = rebinHisto(flag, chan, totBkgd,fBinEdges);

  //Now rebin and replace
  for( auto &kh : hm){
    if(!kh.first.Contains(chan)) continue;
    Histo_t* hist = kh.second;
    
    TString hname = hist->GetName();
    TString htitle = hist->GetTitle();
    hist->SetName("tmp");

    //Create histogram with uniform bins
    Histo_t* hNew = new Histo_t(hname,htitle,fBinEdges.size()-1,0,1);
    
    //Fill new histogram
    for(int bb=1; bb<=hist->GetNbinsX(); bb++){
      int iBin = templ->FindBin(hist->GetBinCenter(bb));
      double ee = hist->GetBinError(bb);
      double oo = hNew->GetBinError(iBin);
      hNew->AddBinContent(iBin, hist->GetBinContent(bb));
      hNew->SetBinError(iBin,sqrt(ee*ee+oo*oo));
    }

    hm[kh.first] = hNew;
  }

  return templ;
}

void RF::VbaseAnalysisRunner::mergeBkgds(){
  
  if(m_mergeBkgds.size()==0) return;//nothing to do here
  
  RF::HistoCollection &hc = histoCollection();
  RF::HistoMap_t &hm = hc.histos();
  
  std::map<TString,Histo_t*> hist_map; 
  
  map<TString, map<TString, unordered_set<string>>> newChanVarMap;
  //collect a list of variations for each sample that need to be present, in case any were pruned or otherwise
  for(auto chan : hc.channels()){

    map<TString, unordered_set<string>> tChanMap;

    for(auto mergedName : m_mergeBkgds){	  
      unordered_set<string> reqVars; //list of variations present in any bkgd going into the merged bkgd
      for(auto addName : mergedName.second){
	for(auto samp : hc.samples(chan)){            
	  if(samp.EqualTo(addName)){ //we need to collect this one	    
	    for(auto vari : hc.variations(chan, samp)){
	      reqVars.insert(vari.Data());
	    }
	  }
	}
	
	tChanMap[mergedName.first] = reqVars;
      }   
    }
    newChanVarMap[chan] = tChanMap;
  }
  
  //need to get the associated channel names before removing channels...
  vector<TString> chNames;
  vector<TString> sampNames;
  vector<TString> variNames;
  //create new channels with new backgrounds
  //delete histos from histomap when merged, replace with new
  for(auto chan : hc.channels()){
    cout << "Merging backgrounds for " << chan << endl;    
    map<TString, unordered_set<string>> tSampVarMap = newChanVarMap[chan];

    for(auto samp : hc.samples(chan)){            
      bool doMergeSample = false;
      TString sMergeName = "";
      for(auto vari : hc.variations(chan, samp)){
	bool vMatched = false;	
	Histo_t* iHist = hc.getHistogram(chan,samp,vari);
	
	for(auto mergedName : m_mergeBkgds){	  
	  for(auto addName : mergedName.second){
	    if(samp.EqualTo(addName)){ //we need to collect this one
	      vMatched = true;
	      doMergeSample = true;
	      sMergeName = mergedName.first;
	      TString iname = hc.getHistoName(chan,mergedName.first,vari);
	      if(hist_map.find(iname) == hist_map.end()){ //haven't started summing this bkgd, so add it
		hist_map[iname] = (Histo_t*)(iHist->Clone(iname));
		chNames.push_back(chan);
		sampNames.push_back(mergedName.first);
		variNames.push_back(vari);

		if(vari == hc.tagNominal()){
		  Sample inSamp = (this->m_channels)[chan].sample(samp);
		  inSamp.setName(mergedName.first);
		  (this->m_channels)[chan].addSample(inSamp);
		  (this->m_channels)[chan].removeSample(samp);
		  hc.removeSample(chan,samp);
		}
	      }
	      else{ 
		if(vari == hc.tagNominal()){
		  recalculateNormSysts((this->m_channels)[chan].sample(samp),iHist->Integral(),(this->m_channels)[chan].sample(mergedName.first),hist_map[iname]->Integral());
		  (this->m_channels)[chan].removeSample(samp);
		  hc.removeSample(chan,samp);
		}
		hist_map[iname]->Add(iHist); 
	      }
	    }
	  }	
	}

	TString key = hc.getHistoName(chan,samp,vari);

	if(!vMatched){ //this variation wasn't asked to be merged
	  hist_map[key] = iHist;
	  chNames.push_back(chan);
	  sampNames.push_back(samp);
	  variNames.push_back(vari);
	}
      
	key.Remove(0,2);
	hm.erase(key);//erase them all...we'll put them back	
      }
      //now we need to see if we're missing any variations in the master list for this sample
      if(doMergeSample){
	Histo_t* iHist = hc.getHistogram(chan,samp,hc.tagNominal());
	for(auto nVar : tSampVarMap[sMergeName]){
	  bool vfound = false;
	  for(auto hVar : hc.variations(chan, samp)){
	    if(hVar.EqualTo(nVar.c_str())) vfound = true;
	  }
	  if(!vfound && iHist){ //didn't find this one, so add the nominal for this bkgd to avoid a 100% uncertainty from a missing syst
	    TString iname = hc.getHistoName(chan,sMergeName,nVar.c_str());
	    hist_map[iname]->Add(iHist); 
	    cout << "MergeBkgds: FYI had to substitute nominal for missing variation: " << chan << " " << samp << " " << nVar << endl;
	  }
	}
      }
    }//samples

    Histo_t* iHist = hc.getData(chan);    
    TString key = hc.getDataName(chan);
    hist_map[key] = iHist;
    chNames.push_back(chan);
    sampNames.push_back(hc.tagData());
    variNames.push_back("");
    key.Remove(0,2);
    hm.erase(key);//erase the data...we'll put it back
  }//channels

  ///As we add back all the histograms, we'll build back up the channel list and repopulate the HistoMap
  TString cname, sname, vname, tname;
  for(auto hist : hist_map){
    
    for(uint i=0; i<chNames.size(); i++){
      cname = chNames[i];
      sname = sampNames[i];
      vname = variNames[i];
      tname = TString("h_").Append(cname).Append("_").Append(sname);
      if(tname.EqualTo(hist.first)) break; //could be data or nominal here
      tname.Append("_").Append(vname);
      if(tname.EqualTo(hist.first)) break; //could be "standard" syst
    }
    
    if(sname == hc.tagData()){
      hc.addData(hist.second,cname);
    }
    else{
      hc.addHistogram(hist.second,cname,sname,vname);
    }
  }

  cout << endl << "*****************************************" << endl;
  cout << "Merged HistoCollection Report" << endl;
  cout << "*****************************************" << endl;
  for(auto chan : hc.channels()){
    cout << "Channel: " << chan << endl;
    for(auto samp : hc.samples(chan)){            
      cout << "  ==>: " << samp << endl;
    }
  }

  cout << endl << "*****************************************" << endl;
  cout << "Merged Channel Report" << endl;
  cout << "*****************************************" << endl;
  for(auto chan : (this->m_channels)){
    cout << "Channel: " << chan.first << endl;
    for(auto samp : chan.second.samples()){
      cout << "  ==>: " << samp << endl;
      cout << "***** " << chan.second.sample(samp).variations().size() << " Shape Systematics *****" << endl;
      for(auto vari : chan.second.sample(samp).variations()){
	cout << "    oo>: " << vari << endl;
      }
      cout << "***** " << chan.second.sample(samp).factors().size() << " Flat Systematics *****" << endl;
      for(auto vari : chan.second.sample(samp).factors()){
	cout << "    -->: " << vari << endl;
      }
    }
  }

}

void RF::VbaseAnalysisRunner::renameRegions(){

  RF::HistoCollection &hc = histoCollection();
  RF::HistoMap_t &hm = hc.histos();
  std::map<TString,RF::Channel> chan_map;

  //loop over channels listed to be renamed                                                                                                                                                   
  //std::cout << "RenameRegion: start to loop over regions to rename" << std::endl;                                                                                                           
  for(auto &rch : m_renameRegion){

    TString chan=rch.first;
    //std::cout << "RenameRegion: region = "<< chan << std::endl;                                                                                                                           

    //containers to add and erase histogram pointers from map                                                                                                                               
    std::list<RF::HistoMap_t::iterator> iteratorEraseList;

    TString new_channel,new_sample,new_variation;

    TString refhisto_channel,refhisto_sample,refhisto_variation;

    std::vector<TString> histNames_add, channels_add, samples_add, variations_add;
    std::vector<RF::Histo_t*> histPtrAdd;

    //rename histograms of specified channels                                                                                                                                               
    //std::cout << "RenameRegion: start to loop all histograms to rename" << std::endl;                                                                                                     
    for( auto &kh : hm){

      if(!kh.first.Contains(chan)) continue;

      TString hist_newchan_name=kh.first;
      iteratorEraseList.push_back(hm.find(kh.first));

      hc.getHistoNameTokens(hist_newchan_name,new_channel,new_sample,new_variation);

      new_channel=rch.second;

      if(kh.first.Contains(hc.tagData())) hist_newchan_name=hc.getDataName(new_channel);
      else hist_newchan_name=hc.getHistoName(new_channel,new_sample,new_variation);


      //add tokens to list for adding to map later                                                                                                                                        
      histPtrAdd.push_back(hm[kh.first]);
      histNames_add.push_back(hist_newchan_name);
      channels_add.push_back(new_channel);
      samples_add.push_back(new_sample);
      variations_add.push_back(new_variation);

    }

    //erase all map elements (old histograms) at iterators in list                                                                                                                          
    //std::cout << "RenameRegion: erase old histos, old size = " <<hm.size();                                                                                                               
    for(auto i : iteratorEraseList){
      hm.erase(i);
    }
    //std::cout << ", new size = " <<hm.size() << std::endl;                                                                                                                               

    //add all extra histograms                                                                                                                                                              
    //std::cout << "RenameRegion: add new histos: ";                                                                                                                                        
    for(unsigned int i=0; i<histPtrAdd.size(); i++){
      //std::cout<<" channel= " << channels_add[i] << ", sample= " <<samples_add[i]<< ", var = " << variations_add[i]<< std::endl;                                                        
      if(histNames_add[i].Contains("data")){
	hc.addData(histPtrAdd[i],channels_add[i]);
      }else{
	hc.addHistogram(histPtrAdd[i],channels_add[i],samples_add[i],variations_add[i]);
      }
    }

    std::cout<<"=================================="<<std::endl;
    std::cout<<"|| Renaming Region:"<<std::endl;
    std::cout<<"||   CH:"<<chan<< " to " << new_channel << std::endl;
    std::cout<<"=================================="<<std::endl;
    //replace the two channels by the new merged one                                                                                                                                        
    //std::cout << "RenameRegion: set new channel name" << std::endl;                                                                                                                       
    double statThresh = ((this->m_channels)[chan]).statErrorThreshold();
    chan_map[new_channel]=Channel((this->m_channels)[chan]);
    chan_map[new_channel].setName(new_channel);
    chan_map[new_channel].setStatErrorThreshold(statThresh);

    //for now fix rebinning for the new channel, remove rebinning factors for old channels                                                                                                  
    //std::cout << "RenameRegion: fix rebinning" << std::endl;                                                                                                                              
    if(m_rebinFactors.find(chan) != m_rebinFactors.end()){
      m_rebinFactors[new_channel]=m_rebinFactors[chan];
      m_rebinFactors.erase(chan);
    }
  }


  //find out which channels have not been renamed, add them to the new channel map                                                                                                            
  //std::cout << "RenameRegion: add not renamed regions" << std::endl;                                                                                                                        
  for(auto it1 = (this->m_channels).begin(); it1!=(this->m_channels).end(); it1++){
    TString chan=it1->first;
    //std::cout << "RenameRegion: channel = " << chan;                                                                                                                                      
    if(!regionHasBeenRenamed(chan)){
      //std::cout << " was not renamed" << std::endl;                                                                                                                                     
      chan_map[it1->first]=it1->second;
    }else{
      //std::cout << " was renamed, remove!" << std::endl;                                                                                                                                
      hc.removeChannel(chan);
    }
  }

  //replace old with the new channel map                                                                                                                                                      
  //std::cout << "RenameRegion: replace old with the new channel map" << std::endl;                                                                                                           
  this->m_channels=std::move(chan_map);

  return;
}


bool RF::VbaseAnalysisRunner::regionHasBeenRenamed(TString chan){

  //check if channel has been merged (more difficult when including SR)                                                                                                                       
  bool couldBeRenamed=false;

  for(auto &mch : m_renameRegion){

    if(mch.first.EqualTo(chan)){
      couldBeRenamed=true;
      break;
    }

  }


  if(couldBeRenamed) return true;

  return false;
}

//  LocalWords:  histoCollector
