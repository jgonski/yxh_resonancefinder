#include "ResonanceFinder/VHAnalysisRunner.h"
#include "ResonanceFinder/SelectiveHistoCollector.h"
#include "ResonanceFinder/CxAODHistoCollector.h"
#include "ResonanceFinder/HistoLumiRescaler.h"
#include "ResonanceFinder/DownVariationAdder.h"
#include "ResonanceFinder/SystematicsSmoother.h"

RF::VHAnalysisRunner::VHAnalysisRunner(TString analysis) : VbaseAnalysisRunner(analysis)
{  
}

RF::VHAnalysisRunner::~VHAnalysisRunner()
{
}

void RF::VHAnalysisRunner::configInputGetter()
{
  if(m_useCxAODformat){
    
    //    RF::CxAODHistoCollector *chc     = dynamic_cast<RF::CxAODHistoCollector*>(m_histoCollector);
    RF::CxAODHistoCollector *chc = dynamic_cast<RF::CxAODHistoCollector*>(m_histoCollector);
    // manually add data (TreeHistoCollector automatically does that, but CxAODHistoCollector
    // does not, as it is a low-level class allowing disrespect of rigid directory structure)
    
    for (auto &kv : m_selections) {
      chc->addSample(kv.first,chc->getHistoCollection().tagData());
    }
  }
  else{
    RF::SelectiveHistoCollector *shc = dynamic_cast<RF::SelectiveHistoCollector*>(m_histoCollector);
    
    // manually add data (TreeHistoCollector automatically does that, but SelectiveHistoCollector
    // does not, as it is a low-level class allowing disrespect of rigid directory structure)
    
    for (auto &kv : m_selections) {
      shc->addSample(kv.first, shc->getHistoCollection().tagData());
    }
  }
}

void RF::VHAnalysisRunner::manipulate()
{
  if(m_useCxAODformat){
    cout << "Collecting histograms from the cxaod reader file output" << endl;
    RF::CxAODHistoCollector *chc = dynamic_cast<RF::CxAODHistoCollector*>(m_histoCollector);
    chc->collectHistograms();
  }  

  //find any missing histos, add one-sided symmetrized histros
  manageHistos();

  //perform merging of regions if requested
  mergeRegions();

  //perform merging of backgrounds if requested
  mergeBkgds();

  //create a histogram of the total background
  generateTotBkgds();

  //perform automatic rebinning if requested
  rebinHistos();

  //perform renaming of regions if requested
  renameRegions();

  //apply all histo corrections: negative bins, scaling, smoothing, etc
  applyHistoCorrections();

  // generate graphs of the signal rates to be converted to eff*acc
  graphSignalEff(m_sigScales);

}

double RF::VHAnalysisRunner::HistReso(double mVH, int region, double minBin, float xMassReso){
  // calculate 2x the HVT W' resolution
  double reso = 40 + (155.- 40.)/(2800.-500.)*(mVH-500.);
  reso *= xMassReso;
  
  //default to 250 GeV for SR, 500 GeV for CR
  if(minBin==-1){
    reso = 249.5;
    
    if(region==2 || region==4){ // bkgd region
      reso = 999.5;
    }
  }

  //user added the min bin width desired
  if(minBin>9) reso = minBin-0.5;
  
  return reso;
}


int RF::VHAnalysisRunner::rebinRegion(TString chan){
  int region;
  if(chan.Contains("SR") && !(chan.Contains("topaddbjetcr") || chan.Contains("1paddtag"))) region=1; //2016 naming convention, 2015 naming convention
  else region =2;
  return region;
}


bool RF::VHAnalysisRunner::createBin(int bin, int lastBin, TString chan, int flag, Histo_t* totBkgd){
  int iRegion = rebinRegion(chan);

  double iReso = 0; //we only get here if the flag is negative
  if(flag<-3 && flag!=-6){
    iRegion += 2;
    iReso = -1;  //auto set 250/500 GeV bins in SR/CR
  }
  if(flag<-9) iReso = fabs(flag*1.0); //set bin width to what was passed in
  
  // for all flags, collapse bins with zero background
  // flag=-1: Base binning on resolution and statistics
  // flag=-2: base binning on resolution only
  // flag=-3: base binning on prescribed minimum, min bin size = 250/500 for SR/CR
  // flag<-9: base binning on user-specified minimum
  
  double sumWidth = 0;
  double sumInt = 0;
  double sumStat = 0;
  for(int bb=bin; bb<lastBin; bb++){
    sumWidth += totBkgd->GetBinWidth(bb);
    sumInt += totBkgd->GetBinContent(bb);
    sumStat = sqrt(sumStat*sumStat + totBkgd->GetBinError(bb)*totBkgd->GetBinError(bb));
  }
  
  float xMassReso;
  if( (flag==-1) || (flag==-3) ) { xMassReso = 1.0; }
  else                           { xMassReso = 2.0; }

  bool createBin = false;
  if( (flag==-1) || (flag==-2) ){  //resolution and statistics, significant bin content
    createBin = (bin==1 || (sumInt>0.05 && (sumStat/(sumInt+1e-6))<0.70 && sumWidth>=HistReso(totBkgd->GetBinCenter(bin), iRegion, iReso, xMassReso)));
  }
  else if(flag==-3){  //resolution and tighter statistics
    createBin = (bin==1 || (sumInt>50 && (sumStat/(sumInt+1e-6))<0.15 && sumWidth>=HistReso(totBkgd->GetBinCenter(bin), iRegion, iReso, xMassReso)));
  }
  else if(flag==-4){ // used in the 2 lepton resolved region
    double reso = 0;
    if(totBkgd->GetBinCenter(bin) <= 605 ) reso = 20.;
    else if(totBkgd->GetBinCenter(bin) > 605 && totBkgd->GetBinCenter(bin) <= 815) reso = 30.;
    else if(totBkgd->GetBinCenter(bin) > 815 && totBkgd->GetBinCenter(bin) <= 1575) reso = 60.;
    else if(totBkgd->GetBinCenter(bin) > 1575) reso = 400.;
    createBin = (bin==1 || (sumInt>0 && (sumStat/(sumInt+1e-6))<0.75 && sumWidth>=reso));
  }
  else if(flag==-5){
    double cThis = totBkgd->GetBinCenter(bin);
    double cNext = totBkgd->GetBinCenter(bin-1);
    vector<double> oneTag = {4140,3390,3050,2740,2460,2200,1970,1760,1570,1390,1230,1090,960,840,730,630,540,460,390,320,260,0};
    vector<double> twoTag = {2930,2270,2030,1810,1610,1430,1270,1120,990,870,760,660,570,490,410,340,280,0};
    createBin = false;
    if(chan.Contains("Merge1tag")){
      for(auto edge : oneTag){
	if(cThis>edge && cNext<edge){ createBin = true; break;}
      }
    }
    if(chan.Contains("Merge2tag")){
      for(auto edge : twoTag){
	if(cThis>edge && cNext<edge){ createBin = true; break;}
      }
    }
  }
  else if (flag==-6){ //similar binning as in AZh for 0-Lepton HVT                                                                                                                                                                         
    double reso = 0;    
    if(totBkgd->GetBinCenter(bin) <= 1000 ) reso = 99.;
    else if(totBkgd->GetBinCenter(bin) > 1000 && totBkgd->GetBinCenter(bin) <= 1200) reso = 199.;
    else if(totBkgd->GetBinCenter(bin) > 1200 && totBkgd->GetBinCenter(bin) <= 1500) reso = 299.;
    else if(totBkgd->GetBinCenter(bin) > 1500 && totBkgd->GetBinCenter(bin) <= 2200) reso = 699.;
    else if(totBkgd->GetBinCenter(bin) > 2200) reso = 999.;
    createBin = (bin==1 || (sumInt>0.0 && (sumStat/(sumInt+1e-6))<0.75 && sumWidth>=reso));                                                                                                                  
  }
  else if(flag<-6){ //Driven by user-specified bin width
    createBin = (bin==1 || (sumInt>0.0 && sumWidth>=HistReso(totBkgd->GetBinCenter(bin), iRegion, iReso, xMassReso)));
  }
    
  //check to see if we're about to leave a lower bin that's too narrow
  if(bin>1 && flag!=-5){
    double lowInt = 0;
    double lowWidth = 0;
    for(int ii=1; ii<bin; ii++){
      lowInt += totBkgd->GetBinContent(ii);
      lowWidth += totBkgd->GetBinWidth(ii);
    }
    
    double customWidth= flag==-6 ? 99. : HistReso(totBkgd->GetBinCenter(bin-1), iRegion, iReso, xMassReso);

    // if the width of what remains is too narrow or the content is too small, just sum it into the last bin being made
    if( ( lowWidth < customWidth ) || ( lowInt<0.1 )){
      createBin = false;
      cout << "Automerging lower bins below: " << totBkgd->GetBinLowEdge(bin) << " (" << bin << ")" << endl;
    }
  }
  
  if(createBin){
    cout << "Create bin: int=" << sumInt << " , err=" << sumStat/(sumInt+1e-6) << " , width=" << sumWidth << endl;
  }

  return createBin;
}



bool RF::VHAnalysisRunner::checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin){
  ///Sum up the total backgrounds, careful to not include signals or data
  Histo_t* totBkgd=hc.getHistogram(chan,"totalBkgd",hc.tagNominal());
  if(!totBkgd) return false;
  
  //only merge between lowMH and highMH of the same tag region, do not consider signal regions
  if( (chan.Contains("SR") && !(chan.Contains("topaddbjetcr"))) || !(chan.Contains("lowmBBcr") || !(chan.Contains("highmBBcr")) || !(chan.Contains("outermBBcr")))) return true; //2016 naming convention
  if( (chan.Contains("SR") && !(chan.Contains("1paddtag"))) || !(chan.Contains("lowMH") || !(chan.Contains("highMH")))) return true; //2015 naming convention

  double BkgContentChan=totBkgd->Integral();
  
  std::cout<<"BackgroundContent: "<<BkgContentChan<<std::endl;
  
  if(BkgContentChan<Nmin){
    std::cout<<"Background expectation too low, merge!"<<std::endl;
    return false;
  }
  
  return true;
}

void RF::VHAnalysisRunner::mergeRegionsIntl(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm, std::map<TString,RF::Channel>& chan_map){
  
  //containers to add and erase histogram pointers from map
  std::list<RF::HistoMap_t::iterator> iteratorEraseList;
  
  TString new_channel,new_sample,new_variation;
  
  TString refhisto_channel,refhisto_sample,refhisto_variation;
  
  std::vector<TString> histNames_add, channels_add, samples_add, variations_add;
  std::vector<RF::Histo_t*> histPtrAdd;
  TString blank("");
  //merge histograms of specified channels
  for( auto &kh : hm){

    if(!kh.first.Contains(chan)) continue;

    TString hist_newchan_name=kh.first;

    hc.getHistoNameTokens(kh.first,refhisto_channel,refhisto_sample,refhisto_variation);

    if(kh.first.Contains(hc.tagData())){

      iteratorEraseList.push_back(hm.find(kh.first));
      
      for(auto partner : m_mergeRegions[refhisto_channel]){
	
	RF::Histo_t* hist=hc.getData(partner);
	if(!hist){
	  cout << "VHAnalysisRunner::mergeRegionsIntl, Warning:  This data histogram doesn't exist!" << endl;
	  continue;
	}

	if(TMath::Abs(hist->GetXaxis()->GetXmax()-hm[kh.first]->GetXaxis()->GetXmax())>1e-5){
	  cout<<"Cannot merge histos: "<<kh.first<<" "<<partner<<" -- different axis range!!!"<<endl;
	}
	
	hm[kh.first]->Add(hist);
	iteratorEraseList.push_back(hm.find(partner+"_data"));
	delete hist;
      }
    }
    else{      

      iteratorEraseList.push_back(hm.find(kh.first));
      
      for(auto partner : m_mergeRegions[refhisto_channel]){
	
	RF::Histo_t* hist=hc.getHistogram(partner,refhisto_sample,refhisto_variation);
	if(!hist){
	  cout << "VHAnalysisRunner::mergeRegionsIntl, Warning:  This histogram doesn't exist! " << partner << "/" << refhisto_sample << "/" << refhisto_variation << endl;
	  continue;
	}
	
	if(TMath::Abs(hist->GetXaxis()->GetXmax()-hm[kh.first]->GetXaxis()->GetXmax())>1e-5){
	  cout<<"Cannot merge histos: "<<kh.first<<" "<<partner+"_"+refhisto_sample+"_"+refhisto_variation<<" -- different axis range!!!"<<endl;
	}

	hm[kh.first]->Add(hist);
	iteratorEraseList.push_back(hm.find(partner+"_"+refhisto_sample+"_"+refhisto_variation));
	delete hist;

      }
    }


    hc.getHistoNameTokens(hist_newchan_name,new_channel,new_sample,new_variation);

    if(!((m_mergedRegionName[refhisto_channel]).EqualTo("unspecified"))){
      new_channel=m_mergedRegionName[refhisto_channel];
    
      if(kh.first.Contains(hc.tagData())) hist_newchan_name=hc.getDataName(new_channel);
      else hist_newchan_name=hc.getHistoName(new_channel,new_sample,new_variation);

    }else{ //no merged channel name specified, assume MH sideband merging
      //get name of new merged histogram
      // 2016 CR naming conventions
      if(kh.first.Contains("highmBBcr")){
	hist_newchan_name.ReplaceAll("highmBBcr","mergedMH");
	new_channel.ReplaceAll("highmBBcr","mergedMH");
      }else if(kh.first.Contains("lowmBBcr")){
	hist_newchan_name.ReplaceAll("lowmBBcr","mergedMH");
	new_channel.ReplaceAll("lowmBBcr","mergedMH");
      }      
      // 2015 CR naming conventions
      else if(kh.first.Contains("highMH")){
	hist_newchan_name.ReplaceAll("highMH","mergedMH");
	new_channel.ReplaceAll("highMH","mergedMH");
      } else if(kh.first.Contains("lowMH")){
	hist_newchan_name.ReplaceAll("lowMH","mergedMH");
	new_channel.ReplaceAll("lowMH","mergedMH");
      }
      else if(kh.first.Contains("_SR_")){
	hist_newchan_name.ReplaceAll("_SR_","_mergedSR_");
	new_channel.ReplaceAll("_SR_","_mergedSR_");
      }

    }
 
    //add tokens to list for adding to map later
    histPtrAdd.push_back(hm[kh.first]);
    histNames_add.push_back(hist_newchan_name);
    channels_add.push_back(new_channel);
    samples_add.push_back(new_sample);
    variations_add.push_back(new_variation);

  }

  //erase all map elements (old histograms) at iterators in list
  for(auto i : iteratorEraseList){
    hm.erase(i);
  }

  //add all extra histograms
  for(unsigned int i=0; i<histPtrAdd.size(); i++){
    if(histNames_add[i].Contains("data")){
      hc.addData(histPtrAdd[i],channels_add[i]);
    }else{
      hc.addHistogram(histPtrAdd[i],channels_add[i],samples_add[i],variations_add[i]);
    }
  }

  std::cout<<"=================================="<<std::endl;
  std::cout<<"|| Merging Regions:"<<std::endl;
  std::cout<<"||   CH:"<<chan<<std::endl;
  for(auto &partner : m_mergeRegions[chan]) std::cout<<"|| + CH:"<<partner<<std::endl;
  std::cout<<"|| = CHtot:"<<new_channel<<std::endl;
  std::cout<<"=================================="<<std::endl;
  
  //replace the two channels by the new merged one
  double statThresh = ((this->m_channels)[chan]).statErrorThreshold();
  chan_map[new_channel]=Channel((this->m_channels)[chan]);
  chan_map[new_channel].setName(new_channel);
  chan_map[new_channel].setStatErrorThreshold(statThresh);

  //for now fix rebinning for the new channel, remove rebinning factors for old channels
  //TODO: choose binning of channel with larger stats?
  if(m_rebinFactors.find(chan) != m_rebinFactors.end()){
    m_rebinFactors[new_channel]=m_rebinFactors[chan];
    m_rebinFactors.erase(chan);
  }
  for(auto &partner : m_mergeRegions[chan]) m_rebinFactors.erase(partner);

  return;
}

void RF::VHAnalysisRunner::mergeRegions(){
  
  RF::HistoCollection &hc = histoCollection();
  RF::HistoMap_t &hm = hc.histos();
  
  std::map<TString,RF::Channel> chan_map; //will be filled in function mergeRegionsIntl()
  
  //loop over channels listed to be merged
  for(auto &mch : m_mergeRegions){

    TString chan=mch.first;
    
    if(!((m_mergedRegionName[chan]).EqualTo("unspecified"))){

      if(!(chan_map.find(m_mergedRegionName[chan])==chan_map.end())){
	continue;
      }

    }else{//no merged channel name specified, assume MH sideband merging
       //avoid double counting, if already merged, ignore
      bool cont = false;
      //2016 CR naming convention
      if(!((chan_map.find(chan.ReplaceAll("lowmBBcr","mergedMH"))==chan_map.end())
	   && (chan_map.find(chan.ReplaceAll("highmBBcr","mergedMH"))==chan_map.end())
	   && (chan_map.find(chan.ReplaceAll("outermBBcr","mergedMH"))==chan_map.end())
	   && (chan_map.find(chan.ReplaceAll("_SR_","_mergedSR_"))==chan_map.end()))){ 
	cont = true;
      }
      //2015 CR naming convention
      if(!((chan_map.find(chan.ReplaceAll("lowMH","mergedMH"))==chan_map.end())
	   && (chan_map.find(chan.ReplaceAll("highMH","mergedMH"))==chan_map.end())
	   && (chan_map.find(chan.ReplaceAll("_SR_","_mergedSR_"))==chan_map.end()))){ 
	cont = true;
      }
      if(cont) continue;
    }   

    if(m_mergeRegionFlag[mch.first]==-1){ //force merge
      mergeRegionsIntl(mch.first,hc,hm,chan_map);      
    }
    else if((m_mergeRegionFlag[mch.first]>0)){ //stat based merge      
      //check if any of the channels have too little statistics
      bool chan_stat=(checkHasNminExpEvts(mch.first,hc,static_cast<double>(m_mergeRegionFlag[mch.first])));
      for(auto &mer: mch.second){
	chan_stat=chan_stat && (checkHasNminExpEvts(mer,hc,static_cast<double>(m_mergeRegionFlag[mch.first])));
      }
      
      if(!chan_stat) mergeRegionsIntl(mch.first,hc,hm,chan_map);
    }
    
  }

  //find out which channels have not been merged, add them to the new channel map
  for(auto it1 = (this->m_channels).begin(); it1!=(this->m_channels).end(); it1++){
    TString chan=it1->first;
    
    if(!regionHasBeenMerged(chan,chan_map)){
      chan_map[it1->first]=it1->second;
    }else{
      hc.removeChannel(chan);
    }
  } 
  
  //replaceold with the new channel map
  this->m_channels=std::move(chan_map);

  return;
}



bool RF::VHAnalysisRunner::regionHasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map){

  //check if channel has been merged (more difficult when including SR)
  bool couldBeMerged=false;
  TString base_chan;
  
  for(auto &mch : m_mergeRegions){
    
    base_chan=mch.first;
    if(mch.first.EqualTo(chan)){
      couldBeMerged=true;
      break;
    }

    for(auto &merch : mch.second){
      if(merch.EqualTo(chan)){
	couldBeMerged=true;
	break;
      }
      
    }
  }

  if(!((m_mergedRegionName[base_chan]).EqualTo("unspecified"))){
    
    if(couldBeMerged && !(chan_map.find(m_mergedRegionName[base_chan])==chan_map.end())) return true;
    
  }else{//no merged channel name specified, assume MH sideband merging
    if(couldBeMerged
       && !((chan_map.find(chan.ReplaceAll("lowmBBcr","mergedMH"))==chan_map.end())
	    && (chan_map.find(chan.ReplaceAll("highmBBcr","mergedMH"))==chan_map.end())	   
	    && (chan_map.find(chan.ReplaceAll("outermBBcr","mergedMH"))==chan_map.end())	   
	    && (chan_map.find(chan.ReplaceAll("_SR_","_mergedSR_"))==chan_map.end()))){
      return true;
    }
    if(couldBeMerged
       && !((chan_map.find(chan.ReplaceAll("lowMH","mergedMH"))==chan_map.end())
	    && (chan_map.find(chan.ReplaceAll("highMH","mergedMH"))==chan_map.end())   
	    && (chan_map.find(chan.ReplaceAll("_SR_","_mergedSR_"))==chan_map.end()))){
      return true;
    }
  }
  
  return false;
}

