#include "ResonanceFinder/AverageVariationAdder.h"
#include <iostream>

RF::AverageVariationAdder::AverageVariationAdder() : m_variation_up(""), m_variation_down("")
{
}

RF::AverageVariationAdder::~AverageVariationAdder()
{
}

void RF::AverageVariationAdder::setVariation(TString name_up, TString name_down)
{
   m_variation_up = name_up;
   m_variation_down = name_down;
}

TString RF::AverageVariationAdder::variation_up() const
{
   return m_variation_up;
}

TString RF::AverageVariationAdder::variation_down() const
{
   return m_variation_down;
}

void RF::AverageVariationAdder::manipulate(HistoCollection &hc)
{
   UInt_t n_changed(0);
   UInt_t n_tested(0);

   for (auto chan : hc.channels()) {
     for (auto sample : hc.samples(chan)) {
       RF::Histo_t *h_up = hc.getHistogram(chan, sample, m_variation_up);
       RF::Histo_t *h_dn = hc.getHistogram(chan, sample, m_variation_down);
       RF::Histo_t *nominal = hc.getHistogram(chan, sample, hc.tagNominal());
       if(!nominal) continue;
       if(!h_up || !h_dn) continue; //only works if we have up and down variations
       n_tested++;

       //Subtract up variation
       //Note: this also removes the nominal from both (nom+deltaUp - (nom+deltaDn)) = deltaUp-deltaDn
       h_up->Add(h_dn,-1);
       h_up->Scale(0.5); //average between variations
       
       //Copy and invert
       for(int ib=0; ib<=h_dn->GetNbinsX(); ib++) h_dn->SetBinContent(ib,h_up->GetBinContent(ib));
       h_dn->Scale(-1);
       
       //Add back nominal
       h_up->Add(nominal);
       h_dn->Add(nominal);
       
       n_changed++;
     }
   }
   
   if (n_changed == 0 && n_tested>0){
     TString err_message("AverageVariationAdder called for a systematic whose variation does not exist, tested: up:");
     err_message+=m_variation_up;
     err_message+=", down: ";
     err_message+=m_variation_down;
     throw std::runtime_error(err_message.Data());
   }
}
