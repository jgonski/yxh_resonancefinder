#include "ResonanceFinder/CxAODHistoCollector.h"

#include <TFile.h>
#include <TIterator.h>
#include <TKey.h>
#include <stdexcept>
#include <iostream>
#include <stdio.h>

using namespace std;

RF::CxAODHistoCollector::CxAODHistoCollector()
{
}

RF::CxAODHistoCollector::CxAODHistoCollector(TString path,HistoCollection& hc)
{
  m_filePath = path;
  getHistoCollection().setTagNominal(hc.tagNominal());
  getHistoCollection().setTagData(hc.tagData());
  getHistoCollection().setTagUp(hc.tagUp());
  getHistoCollection().setTagDown(hc.tagDown());
}

RF::CxAODHistoCollector::~CxAODHistoCollector()
{
}

void RF::CxAODHistoCollector::addSample(TString channel, TString sample)
{
  vector<TString>::iterator it = find(m_channels.begin(),m_channels.end(),channel);
  if(it == m_channels.end()) m_channels.push_back(channel);

  if(m_samples.find(channel) == m_samples.end()) {
    vector<TString> sVec; 
    sVec.push_back(sample);
    m_samples[channel] = sVec;
  }
  else{
    it = find(m_samples[channel].begin(), m_samples[channel].end(),sample);
    if(it == m_samples[channel].end()) m_samples[channel].push_back(sample);
  }
}

void RF::CxAODHistoCollector::collectHistograms(){

   // input from files containing histograms
   TFile *file = new TFile(m_filePath);
   if (!file) throw std::runtime_error("File not found");

   //Walk through the top level directory first
   TIter nextKey(file->GetListOfKeys());
   TKey* key;
   vector<TString> folders;
 
   while((key = (TKey*)nextKey())){
     if(key->GetCycle()!=1) continue;
     
     if(key->IsFolder()) folders.push_back(key->GetName());
     else{
       matchKey(key);
     }
   }

   //Now walk through potential subdirectories
   for(auto ifold : folders){
     if(ifold.Contains("ystematics") || ifold.Contains("ominal")){ //allow for upper/lowercase first letter
       
       TDirectory* subdir = file->GetDirectory(ifold);
       if(!subdir) continue;

       TIter subKey(subdir->GetListOfKeys());       
       while((key = (TKey*)subKey())){
	 if(key->GetCycle()!=1) continue;
	 matchKey(key);
       }
     }
   }

   delete file;

}

void RF::CxAODHistoCollector::addSample(TString channel, TString sample, TString variation, TString hname, TString fname)
{
  addSample(channel, sample);
  TString name = hname;
  name = fname;
  vector<TString>::iterator it = find(m_variations.begin(), m_variations.end(), variation);
  if(it == m_variations.end()) m_variations.push_back(variation);
}

void RF::CxAODHistoCollector::matchKey(TKey* key){

  TString variation = key->GetName();
  TString chname, hname; 
  vector<TString> sVariations;
  for( auto ichan : m_samples){	 
    chname = ichan.first;

    //dumb hack because CxAOD input has to have numbers at the beginning of the channel name
    if(chname.BeginsWith("Ch") || chname.BeginsWith("CH") || chname.BeginsWith("ch")) chname.Remove(0,2);
    if(chname.BeginsWith("Chan") || chname.BeginsWith("CHAN") || chname.BeginsWith("chan")) chname.Remove(0,4);
    if(!variation.Contains(chname)) continue;

    for(auto isamp : ichan.second){	   
      hname = isamp;
      hname.Append("_").Append(chname);

      //check if the key corresponds to a known quantity
      if(!variation.Contains(hname)) continue;
      
      RF::Histo_t* h = dynamic_cast<RF::Histo_t*>(key->ReadObj());

      bool isData = false;
      if(variation.BeginsWith(getHistoCollection().tagData())) isData = true;
      
      if(!isData){
	if(variation.EqualTo(hname)){
	  getHistoCollection().addHistogram(h, ichan.first, isamp, getHistoCollection().tagNominal());
	}
	else if(variation.Contains(getHistoCollection().tagUp()) || variation.Contains(getHistoCollection().tagDown())){
	 TString tName(key->GetName());
	  variation.ReplaceAll(hname,"");	  
	  while(variation.BeginsWith("_")) variation.Remove(0,1);	  
	  getHistoCollection().addHistogram(h, ichan.first, isamp, variation);
	}
      }
      else{ //data!
	if(variation.EqualTo(hname)){
	  getHistoCollection().addData(h, ichan.first);
	}
	else { cout << "Why do we have a data name that doesn't match the right format??  " << variation << endl;}
      }
    }
  }
}

