static const int mFont = 132;

void h2_style(TH2 *h){
  //  return;
  //   h->SetLabelFont(mFont,"X");       // 42
  //   h->SetLabelFont(mFont,"Y");       // 42
   h->SetLabelOffset(0.005,"X");  // D=0.005
   h->SetLabelOffset(0.010,"Y");  // D=0.005
   //   h->SetLabelSize(0.06,"X");
   //   h->SetLabelSize(0.06,"Y");
   h->SetTitleOffset(1.0,"X");
   h->SetTitleOffset(1.1,"Y");
   //   h->SetTitleSize(0.06,"X");
   //   h->SetTitleSize(0.06,"Y");
   h->SetTitle(0);
}

void leg_style(TLegend *h){

  //  h->SetTextFont(mFont);
  h->SetTextSize(0.045);
  h->SetFillColor(0);
  h->SetFillStyle(0);
  h->SetLineColor(0);
  h->SetBorderSize(0);

}

void setAtlasStyle();

void plotLog(const char* filename, int chan, int type){
  int rType = chan + 10*type;
  plotVHpvalues(filename, true, rType);
}

void plotLin(const char* filename, int chan, int type){
  int rType = chan + 10*type;
  plotVHpvalues(filename, false, rType);
}

void plotVHpvalues(const char* filename, bool doLog = 1, int type=99){

  setAtlasStyle();

  float yMin = 1e-2;
  float yMax = 9.99;
  float xMin = 5000;
  float xMax = 4500;

  bool doGrid = 0;
  bool doObs = 1;
  bool doHVTA = 1;
  bool doHVTB = 1;
  TString Lumi = "3.2";

  //Plot Type Definitions
  //99=No labels
  //0 = LVJ Internal
  //1 = LLJ Internal
  //2 = VVJ Internal
  //3 = W' combo Internal
  //4 = Z' combo Internal
  //5 = HVT combo Internal
  //10 = LVJ Preliminary
  //11 = LLJ Preliminary
  //12 = VVJ Preliminary
  //13 = W' combo Preliminary
  //14 = Z' combo Preliminary
  //15 = HVT combo Preliminary
  //20 = LVJ Publication
  //21 = LLJ Publication
  //22 = VVJ Publication
  //23 = W' combo Publication
  //24 = Z' combo Publication
  //25 = HVT combo Publication
  
  TFile* f1 = new TFile(filename);
  TTree* t1=(TTree*)f1->Get("stats");
  int len0=t1->GetEntries();
  printf("%d entries\n");

  float mwp_i; 
  t1->SetBranchAddress("point",&mwp_i);

  float medFact_i, obsFact_i, muhat_i;

  t1->SetBranchAddress("pb_med",&medFact_i);
  t1->SetBranchAddress("pb_obs",&obsFact_i);
  t1->SetBranchAddress("mu_hat_obs",&muhat_i);

  
  int len1 = 0;
  for(int i=0; i<len0; i++){
    t1->GetEntry(i);
    len1++;
  }

  float *mwp=new float[len1];
  float *med=new float[len1];
  float *obs=new float[len1];

  int len2=0;

  for(int i=0; i<len0; i++){
    t1->GetEntry(i);

    if(mwp_i<xMin) xMin = mwp_i;
    if(mwp_i>xMax) xMax = mwp_i;

    mwp[len2] = mwp_i;
    med[len2]=medFact_i;

    obs[len2]=obsFact_i;

    if(muhat_i<0) obs[len2]=0.5;
    if(med[len2]>0.5) med[len2] = 0.5;
    if(obs[len2]>0.5) obs[len2] = 0.5;

    if(medFact_i<yMin) yMin = medFact_i;
    if(obsFact_i<yMin) yMin = obsFact_i;

    len2++;

  }
  
  if(yMin>0.5) yMin = 0.5;
  else yMin *= 0.25;
  

  printf("m_{W'} &");
  for(int i=0; i<len0; i++) printf("%.0f &",mwp[i]);
  printf("\n");
  printf("Expected: &");
  for(int i=0; i<len0; i++) printf("%.4f &",med[i]);
  printf("\n");
  printf("Observed: &");
  for(int i=0; i<len0; i++) printf("%.4f &",obs[i]);
  printf("\n");

  TGraph* fact_obs=new TGraph(len1,mwp,obs);
  TGraph* fact_med=new TGraph(len1,mwp,med);


  TCanvas *c1 = new TCanvas("factor","factor");
  //  c1->SetLeftMargin(0.12);
  //  c1->SetBottomMargin(0.13);
  //  c1->SetTopMargin(0.03);
  c1->SetRightMargin(0.06);

  if(doGrid){
    c1->SetGridx();
    c1->SetGridy();
  }
  TH2* h1=new TH2F("h1","Cross-Section Limit",200,xMin,xMax,100,yMin,yMax);
  h2_style(h1);
  h1->SetTitle("");
  h1->SetStats(kFALSE);
  h1->SetYTitle("Local p_{0}");
   
  if(type%10==0) h1->SetXTitle("m_{W'} (GeV)");
  else if(type%10==1 || type%10==2) h1->SetXTitle("m_{Z'} (GeV)");
  h1->Draw("");
  h1->SetMaximum(yMax);
  h1->SetMinimum(0.01);
  
  double lenSig=2;
  double xSig[2];
  xSig[0] = xMin;
  xSig[1] = xMax;

  double ySig1[2];
  ySig1[0] = 0.5*(1-TMath::Erf(1/sqrt(2)));
  ySig1[1] = ySig1[0];
    
  TGraph* sigmas1=new TGraph(lenSig,xSig,ySig1);
  sigmas1->SetLineWidth(2);
  sigmas1->SetLineColor(2);
    sigmas1->SetLineStyle(7);
    sigmas1->Draw("L");

    TLatex *tex21 = new TLatex(xMax*1.01, ySig1[0]*(exp(-0.2)),"1#sigma");
    tex21->SetTextSize(0.05);
    tex21->SetTextColor(2);
    tex21->Draw();  

    double ySig2[2];
    ySig2[0] = 0.5*(1-TMath::Erf(2/sqrt(2)));
    ySig2[1] = ySig2[0];
    
    TGraph* sigmas2=new TGraph(lenSig,xSig,ySig2);
    sigmas2->SetLineWidth(2);
    sigmas2->SetLineColor(2);
    sigmas2->SetLineStyle(7);
    if(yMin<ySig2[0]) sigmas2->Draw("L");

    TLatex *tex22 = new TLatex(xMax*1.01, ySig2[0]*(exp(-0.2)),"2#sigma");
    tex22->SetTextSize(0.05);
    tex22->SetTextColor(2);
    if(yMin<ySig2[0]) tex22->Draw();  

    double ySig3[2];
    ySig3[0] = 0.5*(1-TMath::Erf(3/sqrt(2)));
    ySig3[1] = ySig3[0];

    TGraph* sigmas3=new TGraph(lenSig,xSig,ySig3);
    sigmas3->SetLineWidth(2);
    sigmas3->SetLineColor(2);
    sigmas3->SetLineStyle(7);
    if(yMin<ySig3[0]) sigmas3->Draw("L"); 

    TLatex *tex23 = new TLatex(xMax*1.01, ySig3[0]*(exp(-0.2)),"3#sigma");
    tex23->SetTextSize(0.05);
    tex23->SetTextColor(2);
    if(yMin<ySig3[0]) tex23->Draw();  

    double ySig4[2];
    ySig4[0] = 0.5*(1-TMath::Erf(4/sqrt(2)));
    ySig4[1] = ySig4[0];

    TGraph* sigmas4=new TGraph(lenSig,xSig,ySig4);
    sigmas4->SetLineWidth(2);
    sigmas4->SetLineColor(2);
    sigmas4->SetLineStyle(7);
    if(yMin<ySig4[0]) sigmas4->Draw("L"); 

    TLatex *tex24 = new TLatex(1.01, ySig4[0]*(exp(-0.2)),"4#sigma");
    //    tex24->SetTextFont(62);
    tex24->SetTextSize(0.05);
    tex24->SetTextColor(2);
    if(yMin<ySig4[0]) tex24->Draw();  




  fact_med->SetFillColor(0);
  fact_med->SetLineColor(4);
  fact_med->SetLineWidth(3);
  fact_med->SetLineStyle(9);
  fact_med->Draw("L");
  
  if(doObs){
    fact_obs->SetFillColor(0);
    fact_obs->SetLineWidth(3);
    fact_obs->SetLineStyle(1);
    fact_obs->SetLineColor(1);
    fact_obs->Draw("L");
  }

  // Need only for outside world
  
 TLatex *tex0 = new TLatex();
  double lx = 0.175; double ly = 0.9;

  if(type<10) 
    tex0= new TLatex(lx,ly,"#font[72]{ATLAS} Internal");
  else if(type<20) 
    tex0= new TLatex(lx,ly,"#font[72]{ATLAS} Preliminary");
  else if(type<30) 
    tex0= new TLatex(lx,ly,"#font[72]{ATLAS}");
  tex0->SetNDC();
  tex0->SetTextSize(0.05);
  tex0->SetTextColor(1);
  tex0->SetTextFont(42);
  if(type!=99) tex0->Draw("same");

  TLatex *tex1 = new TLatex();
  lx = 0.6;  ly = 0.2;

  if(type%10==0)
    tex1= new TLatex(lx,ly,"l#nu+J_{b#bar{b}} Analysis");
  else if(type%10==1) 
    tex1= new TLatex(lx,ly,"ll+J_{b#bar{b}} Analysis");
  else if(type%10==2) 
    tex1= new TLatex(lx,ly,"MET+J_{b#bar{b}} Analysis");
  else if(type%10==3) 
    tex1= new TLatex(lx,ly,"W' Combination");
  else if(type%10==4) 
    tex1= new TLatex(lx,ly,"Z' Combination");
  else if(type%10==5) 
    tex1= new TLatex(lx,ly,"HVT Combination");

  tex1->SetNDC();
  tex1->SetTextSize(0.06);
  tex1->SetTextColor(1);
  tex1->SetTextFont(42);
  if(type!=99) tex1->Draw("same");
  
  lx = 0.175; ly = 0.8;

  TLatex *lumi = new TLatex(lx, ly, "#sqrt{s} = 13 TeV   #int L dt = "+Lumi+" fb^{-1}");
  lumi->SetNDC();
  lumi->SetTextFont(42);
  lumi->SetTextSize(0.04);
  lumi->SetTextColor(1);
  lumi->Draw("same");


  if(type%10==0 || type%10==3) h1->SetXTitle("m_{W'} (GeV)");
  else if(type%10==1 || type%10==2 || type%10==4) h1->SetXTitle("m_{Z'} (GeV)");
  else if(type%10==5) h1->SetXTitle("m_{HVT} (GeV)");

  TLegend *leg4 = new TLegend(0.55,0.75,0.95,0.95,NULL,"brNDC");
  if(doObs) leg4->AddEntry(fact_obs,"Observed","l");
  if(type%10==0 || type%10==3) leg4->AddEntry(fact_med,"Expected (W' Signal)","l");
  else if(type%10==1 || type%10==2 || type%10==4) leg4->AddEntry(fact_med,"Expected (Z' Signal)","l");
  else if(type%10==5) leg4->AddEntry(fact_med,"Expected (HVT Signal)","l");

  leg_style(leg4);
  leg4->Draw("same");

  if(doLog) c1->SetLogy();
}

void setAtlasStyle()
{

  TStyle *atlasStyle = new TStyle("ATLAS","Atlas style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);

  // set margin sizes
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  atlasStyle->SetTitleXOffset(1.4);
  atlasStyle->SetTitleYOffset(1.4);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.055;
  atlasStyle->SetTextFont(font);

  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");
  
  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");

  // use bold lines and markers
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars 
  //atlasStyle->SetErrorX(0.001);
  // get rid of error bar caps
  atlasStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();



}
