static const int mFont = 132;

void h2_style(TH2 *h){
  //  return;
  //   h->SetLabelFont(mFont,"X");       // 42
  //   h->SetLabelFont(mFont,"Y");       // 42
   h->SetLabelOffset(0.005,"X");  // D=0.005
   h->SetLabelOffset(0.010,"Y");  // D=0.005
   //   h->SetLabelSize(0.06,"X");
   //   h->SetLabelSize(0.06,"Y");
   h->SetTitleOffset(1.0,"X");
   h->SetTitleOffset(1.2,"Y");
   //   h->SetTitleSize(0.06,"X");
   //   h->SetTitleSize(0.06,"Y");
   h->SetTitle(0);
}

void leg_style(TLegend *h){

  //  h->SetTextFont(mFont);
  h->SetTextSize(0.045);
  h->SetFillColor(0);
  h->SetFillStyle(0);
  h->SetLineColor(0);
  h->SetBorderSize(0);

}

void setAtlasStyle();

void plotLog(const char* filename, int chan, int type){
  int rType = chan + 10*type;
  plotVHlimits(filename, true, rType);
}

void plotLin(const char* filename, int chan, int type){
  int rType = chan + 10*type;
  plotVHlimits(filename, false, rType);
}

void plotVHlimits(const char* filename, bool doLog = 1, int type=99){

  setAtlasStyle();

  float yMin = 0.5;
  float yMax = 1;
  float xMin = 5000;
  float xMax = 3000;
  bool isRatio = 0;
  bool doGrid = 0;
  bool doObs = 1;
  bool doHVTA = 1;
  bool doHVTB = 1;
  float divBR = 0.326;
  if(type%10==1) divBR = 0.10;
  else if(type%10==2) divBR = 0.20;
  float hbbBR = 0.569+0.0287; //hbb+hcc @ 125.5 GeV

  TString Lumi = "3.2";

  //Plot Type Definitions
  //99=No labels
  //0 = LVJ Internal
  //1 = LLJ Internal
  //2 = VVJ Internal
  //10 = LVJ Preliminary
  //11 = LLJ Preliminary
  //12 = VVJ Preliminary
  //20 = LVJ Publication
  //21 = LLJ Publication
  //22 = VVJ Publication

  float *thMassA=new float[50];
  float *thXsecA=new float[50];
  float *thMassB=new float[50];
  float *thXsecB=new float[50];

  int iThA=0;
  TString thStringA = "HVT Model A";
  if(type==0 || type==10 || type==20){
    
    thMassA[iThA]=500;thXsecA[iThA++]=3.911137473;
    thMassA[iThA]=600;thXsecA[iThA++]=1.974036642;
    thMassA[iThA]=700;thXsecA[iThA++]=1.098033148;
    thMassA[iThA]=800;thXsecA[iThA++]=0.654898005;
    thMassA[iThA]=900;thXsecA[iThA++]=0.411472433;
    thMassA[iThA]=1000;thXsecA[iThA++]=0.269434145;
    thMassA[iThA]=1100;thXsecA[iThA++]=0.18192435;
    thMassA[iThA]=1200;thXsecA[iThA++]=0.126167635;
    thMassA[iThA]=1300;thXsecA[iThA++]=0.089389972;
    thMassA[iThA]=1400;thXsecA[iThA++]=0.064483197;
    thMassA[iThA]=1500;thXsecA[iThA++]=0.047221882;
    thMassA[iThA]=1600;thXsecA[iThA++]=0.035027974;
    thMassA[iThA]=1700;thXsecA[iThA++]=0.026270451;
    thMassA[iThA]=1800;thXsecA[iThA++]=0.019891376;
    thMassA[iThA]=1900;thXsecA[iThA++]=0.015184488;
    thMassA[iThA]=2000;thXsecA[iThA++]=0.011673325;
    thMassA[iThA]=2200;thXsecA[iThA++]=0.007024137;
    thMassA[iThA]=2400;thXsecA[iThA++]=0.004307948;
    thMassA[iThA]=2600;thXsecA[iThA++]=0.002680885;
    thMassA[iThA]=2800;thXsecA[iThA++]=0.001686908;
    thMassA[iThA]=3000;thXsecA[iThA++]=0.001070171;
    thMassA[iThA]=3500;thXsecA[iThA++]=0.000350735;
    thMassA[iThA]=4000;thXsecA[iThA++]=0.000116179;
    thMassA[iThA]=4500;thXsecA[iThA++]=3.81533E-05;
    thMassA[iThA]=5000;thXsecA[iThA++]=1.22409E-05;
  }
  else if(type==1 || type==11 || type == 21 || type==2 || type==12 || type == 22){
    thMassA[iThA]=500;thXsecA[iThA++]=2.223308931;
    thMassA[iThA]=600;thXsecA[iThA++]=1.093829025;
    thMassA[iThA]=700;thXsecA[iThA++]=0.59680236;
    thMassA[iThA]=800;thXsecA[iThA++]=0.350386356;
    thMassA[iThA]=900;thXsecA[iThA++]=0.21721852;
    thMassA[iThA]=1000;thXsecA[iThA++]=0.140428974;
    thMassA[iThA]=1100;thXsecA[iThA++]=0.093826261;
    thMassA[iThA]=1200;thXsecA[iThA++]=0.064380209;
    thMassA[iThA]=1300;thXsecA[iThA++]=0.045160456;
    thMassA[iThA]=1400;thXsecA[iThA++]=0.032264507;
    thMassA[iThA]=1500;thXsecA[iThA++]=0.023407894;
    thMassA[iThA]=1600;thXsecA[iThA++]=0.017210462;
    thMassA[iThA]=1700;thXsecA[iThA++]=0.012799022;
    thMassA[iThA]=1800;thXsecA[iThA++]=0.009609344;
    thMassA[iThA]=1900;thXsecA[iThA++]=0.007278021;
    thMassA[iThA]=2000;thXsecA[iThA++]=0.005554023;
    thMassA[iThA]=2200;thXsecA[iThA++]=0.003296954;
    thMassA[iThA]=2400;thXsecA[iThA++]=0.001998716;
    thMassA[iThA]=2600;thXsecA[iThA++]=0.001231884;
    thMassA[iThA]=2800;thXsecA[iThA++]=0.000769557;
    thMassA[iThA]=3000;thXsecA[iThA++]=0.000485717;
    thMassA[iThA]=3500;thXsecA[iThA++]=0.000158987;
    thMassA[iThA]=4000;thXsecA[iThA++]=5.3507E-05;
    thMassA[iThA]=4500;thXsecA[iThA++]=1.82003E-05;
    thMassA[iThA]=5000;thXsecA[iThA++]=6.17272E-06;
  }

  int iThB=0;
  TString thStringB = "HVT Model B";
  if(type==0 || type==10 || type==20){    
    
    thMassB[iThB]=800;thXsecB[iThB++]=hbbBR*0.879570623;
    thMassB[iThB]=900;thXsecB[iThB++]=hbbBR*0.685655409;
    thMassB[iThB]=1000;thXsecB[iThB++]=hbbBR*0.501622271;
    thMassB[iThB]=1100;thXsecB[iThB++]=hbbBR*0.363368496;
    thMassB[iThB]=1200;thXsecB[iThB++]=hbbBR*0.264599994;
    thMassB[iThB]=1300;thXsecB[iThB++]=hbbBR*0.194291491;
    thMassB[iThB]=1400;thXsecB[iThB++]=hbbBR*0.144016916;
    thMassB[iThB]=1500;thXsecB[iThB++]=hbbBR*0.107730394;
    thMassB[iThB]=1600;thXsecB[iThB++]=hbbBR*0.081279604;
    thMassB[iThB]=1700;thXsecB[iThB++]=hbbBR*0.061805825;
    thMassB[iThB]=1800;thXsecB[iThB++]=hbbBR*0.047334333;
    thMassB[iThB]=1900;thXsecB[iThB++]=hbbBR*0.03647968;
    thMassB[iThB]=2000;thXsecB[iThB++]=hbbBR*0.028271056;
    thMassB[iThB]=2100;thXsecB[iThB++]=hbbBR*0.022021171;
    thMassB[iThB]=2200;thXsecB[iThB++]=hbbBR*0.01723023;
    thMassB[iThB]=2300;thXsecB[iThB++]=hbbBR*0.013534591;
    thMassB[iThB]=2400;thXsecB[iThB++]=hbbBR*0.0106694;
    thMassB[iThB]=2500;thXsecB[iThB++]=hbbBR*0.00844299;
    thMassB[iThB]=2600;thXsecB[iThB++]=hbbBR*0.006689037;
    thMassB[iThB]=2700;thXsecB[iThB++]=hbbBR*0.005316851;
    thMassB[iThB]=2800;thXsecB[iThB++]=hbbBR*0.004233592;
    thMassB[iThB]=2900;thXsecB[iThB++]=hbbBR*0.003377323;
    thMassB[iThB]=3000;thXsecB[iThB++]=hbbBR*0.002698375;
    thMassB[iThB]=3100;thXsecB[iThB++]=hbbBR*0.00215866;
    thMassB[iThB]=3200;thXsecB[iThB++]=hbbBR*0.001728717;
    thMassB[iThB]=3300;thXsecB[iThB++]=hbbBR*0.001385657;
    thMassB[iThB]=3400;thXsecB[iThB++]=hbbBR*0.001111364;
    thMassB[iThB]=3500;thXsecB[iThB++]=hbbBR*0.000891753;
    thMassB[iThB]=3600;thXsecB[iThB++]=hbbBR*0.000715737;
    thMassB[iThB]=3700;thXsecB[iThB++]=hbbBR*0.000574558;
    thMassB[iThB]=3800;thXsecB[iThB++]=hbbBR*0.000461095;
    thMassB[iThB]=3900;thXsecB[iThB++]=hbbBR*0.000370087;
    thMassB[iThB]=4000;thXsecB[iThB++]=hbbBR*0.000296948;
    thMassB[iThB]=4100;thXsecB[iThB++]=hbbBR*0.000238181;
    thMassB[iThB]=4200;thXsecB[iThB++]=hbbBR*0.000190925;
    thMassB[iThB]=4300;thXsecB[iThB++]=hbbBR*0.00015296;
    thMassB[iThB]=4400;thXsecB[iThB++]=hbbBR*0.000122428;
    thMassB[iThB]=4500;thXsecB[iThB++]=hbbBR*9.7886E-05;
    thMassB[iThB]=4600;thXsecB[iThB++]=hbbBR*7.82146E-05;
    thMassB[iThB]=4700;thXsecB[iThB++]=hbbBR*6.24247E-05;
    thMassB[iThB]=4800;thXsecB[iThB++]=hbbBR*4.97567E-05;
    thMassB[iThB]=4900;thXsecB[iThB++]=hbbBR*3.96074E-05;
    thMassB[iThB]=5000;thXsecB[iThB++]=hbbBR*3.14874E-05;


  }
  else if(type==2 || type==12 || type == 22 || type==1 || type==11 || type == 21){
 

    thMassB[iThB]=800;thXsecB[iThB++]=hbbBR*0.486026339;
    thMassB[iThB]=900;thXsecB[iThB++]=hbbBR*0.36765227;
    thMassB[iThB]=1000;thXsecB[iThB++]=hbbBR*0.264133532;
    thMassB[iThB]=1100;thXsecB[iThB++]=hbbBR*0.188827217;
    thMassB[iThB]=1200;thXsecB[iThB++]=hbbBR*0.135828163;
    thMassB[iThB]=1300;thXsecB[iThB++]=hbbBR*0.098640392;
    thMassB[iThB]=1400;thXsecB[iThB++]=hbbBR*0.072359056;
    thMassB[iThB]=1500;thXsecB[iThB++]=hbbBR*0.053592927;
    thMassB[iThB]=1600;thXsecB[iThB++]=hbbBR*0.04006061;
    thMassB[iThB]=1700;thXsecB[iThB++]=hbbBR*0.030195465;
    thMassB[iThB]=1800;thXsecB[iThB++]=hbbBR*0.02292383;
    thMassB[iThB]=1900;thXsecB[iThB++]=hbbBR*0.017524352;
    thMassB[iThB]=2000;thXsecB[iThB++]=hbbBR*0.013478671;
    thMassB[iThB]=2100;thXsecB[iThB++]=hbbBR*0.010423879;
    thMassB[iThB]=2200;thXsecB[iThB++]=hbbBR*0.008101561;
    thMassB[iThB]=2300;thXsecB[iThB++]=hbbBR*0.006324857;
    thMassB[iThB]=2400;thXsecB[iThB++]=hbbBR*0.004957716;
    thMassB[iThB]=2500;thXsecB[iThB++]=hbbBR*0.003899802;
    thMassB[iThB]=2600;thXsecB[iThB++]=hbbBR*0.003077802;
    thMassB[iThB]=2700;thXsecB[iThB++]=hbbBR*0.002436297;
    thMassB[iThB]=2800;thXsecB[iThB++]=hbbBR*0.001933683;
    thMassB[iThB]=2900;thXsecB[iThB++]=hbbBR*0.001540473;
    thMassB[iThB]=3000;thXsecB[iThB++]=hbbBR*0.001226066;
    thMassB[iThB]=3100;thXsecB[iThB++]=hbbBR*0.000979248;
    thMassB[iThB]=3200;thXsecB[iThB++]=hbbBR*0.000783482;
    thMassB[iThB]=3300;thXsecB[iThB++]=hbbBR*0.000627796;
    thMassB[iThB]=3400;thXsecB[iThB++]=hbbBR*0.000503707;
    thMassB[iThB]=3500;thXsecB[iThB++]=hbbBR*0.000404608;
    thMassB[iThB]=3600;thXsecB[iThB++]=hbbBR*0.000325314;
    thMassB[iThB]=3700;thXsecB[iThB++]=hbbBR*0.000261769;
    thMassB[iThB]=3800;thXsecB[iThB++]=hbbBR*0.000210778;
    thMassB[iThB]=3900;thXsecB[iThB++]=hbbBR*0.00016983;
    thMassB[iThB]=4000;thXsecB[iThB++]=hbbBR*0.000136888;
    thMassB[iThB]=4100;thXsecB[iThB++]=hbbBR*0.000110393;
    thMassB[iThB]=4200;thXsecB[iThB++]=hbbBR*8.904E-05;
    thMassB[iThB]=4300;thXsecB[iThB++]=hbbBR*7.18098E-05;
    thMassB[iThB]=4400;thXsecB[iThB++]=hbbBR*5.79259E-05;
    thMassB[iThB]=4500;thXsecB[iThB++]=hbbBR*4.67295E-05;
    thMassB[iThB]=4600;thXsecB[iThB++]=hbbBR*3.76909E-05;
    thMassB[iThB]=4700;thXsecB[iThB++]=hbbBR*3.03913E-05;
    thMassB[iThB]=4800;thXsecB[iThB++]=hbbBR*2.44941E-05;
    thMassB[iThB]=4900;thXsecB[iThB++]=hbbBR*1.97322E-05;
    thMassB[iThB]=5000;thXsecB[iThB++]=hbbBR*1.5889E-05;
  }

  
  TFile* f1 = new TFile(filename);
  TTree* t1=(TTree*)f1->Get("stats");
  int len0=t1->GetEntries();
  printf("%d entries\n");

  float mwp_i; 
  t1->SetBranchAddress("point",&mwp_i);

  float medFact_i, obsFact_i;
  float medp1Fact_i, medm1Fact_i;
  float medp2Fact_i, medm2Fact_i;
  t1->SetBranchAddress("exp_upperlimit",&medFact_i);
  t1->SetBranchAddress("obs_upperlimit",&obsFact_i);
  t1->SetBranchAddress("exp_upperlimit_plus1",&medp1Fact_i);
  t1->SetBranchAddress("exp_upperlimit_minus1",&medm1Fact_i);
  t1->SetBranchAddress("exp_upperlimit_plus2",&medp2Fact_i);
  t1->SetBranchAddress("exp_upperlimit_minus2",&medm2Fact_i);
  
  int len1 = 0;
  for(int i=0; i<len0; i++){
    t1->GetEntry(i);
    len1++;
  }

  float *mwp=new float[len1];
  float *med=new float[len1];
  float *obs=new float[len1];

  float *mwp_sigma=new float[2*len1+1];
  float *med1s=new float[2*len1+1];
  float *med2s=new float[2*len1+1];

  int len2=0;
  double sigSF = 1;

  for(int i=0; i<len0; i++){
    t1->GetEntry(i);

    if(mwp_i<xMin) xMin = mwp_i;
    if(mwp_i>xMax) xMax = mwp_i;

    for(int tt=0; tt<iThA; tt++){
      if(((int)mwp_i) == thMassA[tt]) sigSF = thXsecA[tt];
    }
    if(isRatio) sigSF = 1;
    
    if(mwp_i >= 2999.99) sigSF *= 1000; //3 TeV and above are normalized to fb, not pb
    
    mwp[len2] = mwp_i;
    med[len2]=medFact_i*sigSF;

    med1s[len2]=medp1Fact_i*sigSF; 
    med1s[2*len1-1-len2]=medm1Fact_i*sigSF; 

    med2s[len2]=medp2Fact_i*sigSF; 
    med2s[2*len1-1-len2]=medm2Fact_i*sigSF; 

    mwp_sigma[len2]=mwp_i;
    mwp_sigma[2*len1-1-len2]=mwp_i;

    obs[len2]=obsFact_i*sigSF;

    if(medp2Fact_i*sigSF > yMax) yMax = medp2Fact_i*sigSF;
    if(doObs && obsFact_i*sigSF > yMax) yMax = obsFact_i*sigSF;

    if(medm2Fact_i*sigSF < yMin && medm2Fact_i>0) yMin = medm2Fact_i*sigSF;
    if(doObs && obsFact_i*sigSF < yMin && obsFact_i>0) yMin = obsFact_i*sigSF;

    len2++;

    printf("%d %.4f %.4f %.4f %.4f %.4f %.4f \n",mwp_i, obsFact_i, medm2Fact_i,medm1Fact_i,medFact_i, medp1Fact_i,medp2Fact_i);

  }
  
  if(doLog) yMax *= 5;
  else yMax *= 1.5;

  if(isRatio && yMin>0.5) yMin = 0.5;
  else yMin *= 0.25;

  //  yMin = 1e-6;

  printf("m_{W'} &");
  for(int i=0; i<len0; i++) printf("%.0f &",mwp[i]);
  printf("\n");
  printf("Expected: &");
  for(int i=0; i<len0; i++) printf("%.4f &",med[i]);
  printf("\n");
  printf("Observed: &");
  for(int i=0; i<len0; i++) printf("%.4f &",obs[i]);
  printf("\n");

  mwp_sigma[2*len1]=mwp_sigma[0];
  med1s[2*len1]=med1s[0];
  med2s[2*len1]=med2s[0];

  TGraph* fact_obs=new TGraph(len1,mwp,obs);
  TGraph* fact_med=new TGraph(len1,mwp,med);
  TGraph* fact_med1s=new TGraph(2*len1+1,mwp_sigma,med1s);
  TGraph* fact_med2s=new TGraph(2*len1+1,mwp_sigma,med2s);


  TCanvas *c1 = new TCanvas("factor","factor");
  c1->SetLeftMargin(0.12);
  c1->SetBottomMargin(0.13);
  c1->SetTopMargin(0.03);
  c1->SetRightMargin(0.04);

  if(doGrid){
    c1->SetGridx();
    c1->SetGridy();
  }
  TH2* h1=new TH2F("h1","Cross-Section Limit",200,xMin,xMax,100,yMin,yMax);
  h2_style(h1);
  h1->SetTitle("");
  h1->SetStats(kFALSE);
  if(type%10==0) h1->SetYTitle("#sigma #times BR(W' #rightarrow WH) #times BR(H #rightarrow b#bar{b}) [pb]");
  else if(type%10==1 || type%10==2) h1->SetYTitle("#sigma #times BR(Z' #rightarrow ZH) #times BR(H #rightarrow b#bar{b}) [pb]");
  if(isRatio) h1->SetYTitle("95% CL Upper Limit / HVTA");

 
  if(type%10==0) h1->SetXTitle("m_{W'} (GeV)");
  else if(type%10==1 || type%10==2) h1->SetXTitle("m_{Z'} (GeV)");
  h1->Draw("");
  h1->SetMaximum(yMax);
  h1->SetMinimum(0.01);
  

  fact_med2s->SetFillColor(5);
  fact_med2s->SetLineColor(5);
  fact_med2s->SetMarkerColor(5);
  //  fact_med2s->SetFillStyle(3001);
  fact_med2s->Draw("F");

  fact_med1s->SetFillColor(3);
  fact_med1s->SetLineColor(3);
  fact_med1s->SetMarkerColor(3);
  //  fact_med1s->SetFillStyle(3001);
  fact_med1s->Draw("F");

  if(doGrid){
    gPad->SetGrid();
    gPad->RedrawAxis("g");
  }

  if(isRatio){
    double lenModel=2;
    double xModel[2];
    double yModel[2];
    xModel[0] = xMin;
    xModel[1] = xMax;
    yModel[0] = 1;
    yModel[1] = 1;
    
    
    TGraph* rModel=new TGraph(lenModel,xModel,yModel);
    rModel->SetLineWidth(2);
    rModel->SetLineColor(2);
    rModel->SetLineStyle(9);
    rModel->Draw("L");
    
    TLatex *tex2 = new TLatex(0.7*xMax,1.07,"Reference Model = 1.0");
    tex2->SetTextSize(0.045);
    tex2->SetTextColor(2);
    tex2->Draw();  
    
  }


  TGraph* thGraphA=new TGraph(iThA,thMassA,thXsecA);
  thGraphA->SetLineColor(2);
  thGraphA->SetLineWidth(3);
  if(!isRatio && doHVTA) thGraphA->Draw("same");

  TGraph* thGraphB=new TGraph(iThB,thMassB,thXsecB);
  thGraphB->SetLineColor(6);
  thGraphB->SetLineWidth(3);
  if(!isRatio && doHVTB) thGraphB->Draw("same");


  gPad->RedrawAxis("y");
  gPad->RedrawAxis("x");
  
  fact_med->SetFillColor(0);
  fact_med->SetLineColor(4);
  fact_med->SetLineWidth(3);
  fact_med->SetLineStyle(9);
  fact_med->Draw("L");
  
  if(doObs){
    fact_obs->SetFillColor(0);
    fact_obs->SetLineWidth(3);
    fact_obs->SetLineStyle(1);
    fact_obs->SetLineColor(1);
    fact_obs->Draw("L");
  }

  // Need only for outside world
  
 TLatex *tex0 = new TLatex();
  double lx = 0.65; double ly = 0.9;
  if(!isRatio) lx = 0.15;
  if(type<10) 
    tex0= new TLatex(lx,ly,"#font[72]{ATLAS} Internal");
  else if(type<20) 
    tex0= new TLatex(lx,ly,"#font[72]{ATLAS} Preliminary");
  else if(type<30) 
    tex0= new TLatex(lx,ly,"#font[72]{ATLAS}");
  tex0->SetNDC();
  tex0->SetTextSize(0.05);
  tex0->SetTextColor(1);
  tex0->SetTextFont(42);
  if(type!=99) tex0->Draw("same");

  TLatex *tex1 = new TLatex();
  lx = 0.6;  ly = 0.3;
  if(!isRatio){
    ly = 0.45;
  }
  if(type%10==0)
    tex1= new TLatex(lx,ly,"l#nu+J_{b#bar{b}} Analysis");
  else if(type%10==1) 
    tex1= new TLatex(lx,ly,"ll+J_{b#bar{b}} Analysis");
  else if(type%10==2) 
    tex1= new TLatex(lx,ly,"MET+J_{b#bar{b}} Analysis");

  tex1->SetNDC();
  tex1->SetTextSize(0.06);
  tex1->SetTextColor(1);
  tex1->SetTextFont(42);
  if(type!=99) tex1->Draw("same");
  
  lx = 0.55; ly = 0.8;
  if(!isRatio) lx = 0.15;
  TLatex *lumi = new TLatex(lx, ly, "#sqrt{s} = 13 TeV   #int L dt = "+Lumi+" fb^{-1}");
  lumi->SetNDC();
  lumi->SetTextFont(42);
  lumi->SetTextSize(0.04);
  lumi->SetTextColor(1);
  lumi->Draw("same");

  TGraph* cl1s = (TGraph*)fact_med1s->Clone("cl1s");
  TGraph* cl2s = (TGraph*)fact_med2s->Clone("cl2s");
  cl1s->SetLineColor(1);
  cl2s->SetLineColor(1);

  TDatime myDate;
  TLatex* tex4= new TLatex(xMin*1.1,yMax*0.6,myDate.AsString());
  tex4->SetTextSize(0.05);
  tex4->SetTextColor(1);
  tex4->SetTextFont(mFont);
  //  tex4->Draw();  


  TLegend *leg4 = 0;
  if(isRatio) leg4 = new TLegend(0.20,0.55,0.55,0.95,NULL,"brNDC");
  else leg4 = new TLegend(0.60,0.55,0.95,0.95,NULL,"brNDC");
  if(doObs) leg4->AddEntry(fact_obs,"Observed","l");
  leg4->AddEntry(fact_med,"Expected","l");
  leg4->AddEntry(cl1s,"Expected #pm1 s.d.","f");
  leg4->AddEntry(cl2s,"Expected #pm2 s.d.","f");
  if(!isRatio && doHVTA) leg4->AddEntry(thGraphA,thStringA,"l");
  if(!isRatio && doHVTB) leg4->AddEntry(thGraphB,thStringB,"l");
  leg_style(leg4);
  leg4->Draw("same");




  if(doLog) c1->SetLogy();
}

void setAtlasStyle()
{

  TStyle *atlasStyle = new TStyle("ATLAS","Atlas style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);

  // set margin sizes
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  atlasStyle->SetTitleXOffset(1.4);
  atlasStyle->SetTitleYOffset(1.4);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  atlasStyle->SetTextFont(font);

  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");
  
  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");

  // use bold lines and markers
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars 
  //atlasStyle->SetErrorX(0.001);
  // get rid of error bar caps
  atlasStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();



}
