import ROOT

def readFile(fname, xsec, br=0.699*0.676, signal='W\'', finalstate='WZ', nick=''):
  f = ROOT.TFile.Open(fname)

  t = f.Get('stats')

  printout_mu = []
  printout_xsec = []

  printout_mu.append('\n\n%%%s (signal strength limit)' % nick)
  printout_xsec.append('\n\n%%%s (signal xsec / BRqqqq limit)' % nick)

  if signal == 'W\'':
    signallong = 'HVT W\'(' + finalstate + ')'
  elif signal == 'Z\'':
    signallong = 'HVT Z\'(' + finalstate + ')'
  elif signal == 'G_{RS}':
    signallong = 'RS Graviton(' + finalstate + ')'
  else:
    raise RuntimeError('Not implemented signal %s' % signal)

  header_xsec = '''\\begin{center}
\\scriptsize
\\begin{table}
\\begin{tabular}{r r | r  r r r r r r }
\\hline
\\hline
 \\multicolumn{2}{c|}{ ''' + signallong + ''' } & \\multicolumn{6}{c}{$\\sigma\\times BR(''' + signal + '''\\to ''' + finalstate + ''')$  95\\% CL Limits [fb]}  \\\\
\\hline
\\hline
 $m_{''' + signal + '''}$, \\GeV   &   $p_0$  &   observed   &  \\multicolumn{5}{c}{expected} \\\\
                &          &            &   $-2\\sigma$   &    $-1\\sigma$ & median &  $+1\\sigma$ &  $+2\\sigma$ \\\\\\hline
'''
  
  header_mu = header_xsec.replace('\\sigma\\times BR', '\mu ')

  footer = '''
\\hline
\\end{tabular}
\\end{table}
\\end{center}
'''

  titleString = '$m_{signal}$   &   $p_0$  &   ${what}$ observed 95\\% CL upper limit  &  expected $-2\\sigma$   &   expected $-1\\sigma$ & expected & expected $+1\\sigma$ & expected $+2\\sigma$ \\\\\\hline'
  printout_mu.append( header_mu )
  printout_xsec.append( header_xsec )

  for entry in xrange(t.GetEntries()):
    t.GetEntry(entry)

    #$1200$ & $0.13$ & $244$ & $80$ & $115$ & $178$ & $298$ & $399$ \\\\
    #$2400$ & $0.50$ & $9.7$ & $6.2$ & $8.0$ & $12$ & $26$ & $43$ \\\\
    lineString_mu = '${mass}$ & ${p0}$ & ${obs}$ & ${exp_m2}$ & ${exp_m1}$ & ${exp}$ & ${exp_p1}$ & ${exp_p2}$ \\\\\\hline'
    lineString_xsec = '${mass:.0f}$ & ${p0:.2f}$ & ${obs}$ & ${exp_m2}$ & ${exp_m1}$ & ${exp}$ & ${exp_p1}$ & ${exp_p2}$ \\\\\\hline'

    # TODO: significance with ROOT::Math::normal_quantile_c(pval, 1)

    printout_mu.append( lineString_mu.format(
      mass=t.point,
      p0=round(t.null_pvalue, 3),
      obs=round(t.obs_upperlimit, 3),
      exp_m2=-1,#VALERIO round(t.exp_upperlimit_minus2, 3),
      exp_m1=-1,#VALERIO round(t.exp_upperlimit_minus1, 3),
      exp=round(t.exp_upperlimit, 3),
      exp_p1=-1,#VALERIO round(t.exp_upperlimit_plus1, 3),
      exp_p2=-1,#VALERIO round(t.exp_upperlimit_plus2, 3),
    ) )

    mu_to_xsec = xsec[t.point] / br

    tmp_obs=t.obs_upperlimit * mu_to_xsec
    tmp_exp_m2=-1#VALERIO t.exp_upperlimit_minus2 * mu_to_xsec
    tmp_exp_m1=-1#VALERIO t.exp_upperlimit_minus1 * mu_to_xsec
    tmp_exp=t.exp_upperlimit * mu_to_xsec
    tmp_exp_p1=-1#VALERIO t.exp_upperlimit_plus1 * mu_to_xsec
    tmp_exp_p2=-1#VALERIO t.exp_upperlimit_plus2 * mu_to_xsec

    if tmp_obs >= 10.05: proper_obs = '%.0f' % tmp_obs
    else: proper_obs = '%.1f' % tmp_obs
    if tmp_exp_m2 >= 10.05: proper_exp_m2 = '%.0f' % tmp_exp_m2
    else: proper_exp_m2 = '%.1f' % tmp_exp_m2
    if tmp_exp_m1 >= 10.05: proper_exp_m1 = '%.0f' % tmp_exp_m1
    else: proper_exp_m1 = '%.1f' % tmp_exp_m1
    if tmp_exp >= 10.05: proper_exp = '%.0f' % tmp_exp
    else: proper_exp = '%.1f' % tmp_exp
    if tmp_exp_p1 >= 10.05: proper_exp_p1 = '%.0f' % tmp_exp_p1
    else: proper_exp_p1 = '%.1f' % tmp_exp_p1
    if tmp_exp_p2 >= 10.05: proper_exp_p2 = '%.0f' % tmp_exp_p2
    else: proper_exp_p2 = '%.1f' % tmp_exp_p2

    printout_xsec.append( lineString_xsec.format(
      mass=t.point,
      p0=t.null_pvalue,
      obs=proper_obs,
      exp_m2=proper_exp_m2,
      exp_m1=proper_exp_m1,
      exp=proper_exp,
      exp_p1=proper_exp_p1,
      exp_p2=proper_exp_p2,
    ) )

  printout_mu.append(footer)
  printout_xsec.append(footer)

  print '\n'.join(printout_mu)
  print '\n'.join(printout_xsec)


if __name__ == '__main__':

# Wprime, WZ
  Wprime_WZ = {
  1100: 128.93,
  1200: 88.662,
  1300: 61.908,
  1400: 44.482,
  1500: 32.327,
  1600: 23.77,
  1700: 17.775,
  1800: 13.422,
  1900: 10.267,
  2000: 7.8715,
  2100: 6.118,
  2200: 4.7763,
  2300: 3.7325,
  2400: 2.9662,
  2500: 2.3708,
  3000: 0.80858,
  }

# HVT, WZ
  HVT_WZ = {
  1000: 201.7,
  1100: 132.9,
  1200: 90.14,
  1300: 62.72,
  1400: 44.36,
  1500: 31.95,
  1600: 23.32,
  1700: 17.28,
  1800: 12.98,
  1900: 9.819,
  2000: 7.5,
  2200: 4.454,
  2400: 2.703,
  2600: 1.679,
  2800: 1.054,
  3000: 0.6688,
  3500: 0.226,
  4000: 0.07822,
  4500: 0.02748,
  }

# Gravi, WW
  Gravi_WW = {
  1000: 33.21,
  1100: 19.17,
  1200: 11.53,
  1300: 7.176,
  1400: 4.584,
  1500: 3.002,
  1600: 2.004,
  1700: 1.363,
  1800: 0.9409,
  1900: 0.6583,
  2000: 0.4656,
  2200: 0.2401,
  2400: 0.1281,
  2600: 0.07022,
  2800: 0.03933,
  3500: 0.005847,
  4000: 0.001615,
  4500: 0.0004663,
  5000: 0.0001405,
  }

# HVT, WW
  HVT_WW = {
  1000: 93.51,
  1100: 61.32,
  1200: 41.37,
  1300: 28.65,
  1400: 20.24,
  1500: 14.46,
  1600: 10.6,
  1700: 7.851,
  1800: 5.808,
  1900: 4.395,
  2000: 3.345,
  2200: 1.968,
  2400: 1.196,
  2600: 0.7407,
  2800: 0.4671,
  3000: 0.296,
  3500: 0.09876,
  4000: 0.0349,
  4500: 0.01239,
  5000: 0.004408,
  }


#Gravi, ZZ
  Gravi_ZZ = {
  1000: 18.33,
  1100: 10.53,
  1200: 6.327,
  1300: 3.9268,
  1400: 2.509,
  1500: 1.641,
  1600: 1.095,
  1700: 0.7433,
  1800: 0.5131,
  1900: 0.3585,
  2000: 0.2535,
  2200: 0.1307,
  2400: 0.0697,
  2600: 0.03809,
  2800: 0.02133,
  3000: 0.01216,
  3500: 0.003164,
  4000: 0.0008763,
  4500: 0.0002533,
  5000: 7.645e-05,
  }

  br_WZ = 0.699*0.676
  br_WW = 0.676*0.676
  br_ZZ = 0.699*0.699



  print '\n\n%%%%%%%%%%%%%% HVT WZ'
  readFile('minitrees/HVT_WZ.root', #results_v4_3p32fb/VVJJ_HVT_WZ/ws/results_VVJJ_HVT_WZ_v4_WZ_HVT_obs_wNtrk.root',
    HVT_WZ,
    br = br_WZ,
    signal = 'W\'',
    finalstate = 'WZ',
    nick = 'v4',
  )

  print '\n\n%%%%%%%%%%%%%% HVT WW'
  readFile('minitrees/HVT_WW.root', #results_v4_3p32fb/VVJJ_HVT_WW/ws/results_VVJJ_HVT_WW_v4_WW_HVT_obs_wNtrk.root',
    HVT_WW,
    br = br_WW,
    signal = 'Z\'',
    finalstate = 'WW',
    nick = 'v4',
  )

  print '\n\n%%%%%%%%%%%%%% Gravi WW'
  readFile('minitrees/Gravi_WW.root', #results_v4_3p32fb/VVJJ_Gravi_WW/ws/results_VVJJ_Gravi_WW_v4_WW_Gravi_obs_wNtrk.root',
    Gravi_WW,
    br = br_WW,
    signal = 'G_{RS}',
    finalstate = 'WW',
    nick = 'v4',
  )

  print '\n\n%%%%%%%%%%%%%% Gravi ZZ'
  readFile('minitrees/Gravi_ZZ.root', #results_v4_3p32fb/VVJJ_Gravi_ZZ/ws/results_VVJJ_Gravi_ZZ_v4_ZZ_Gravi_obs_wNtrk.root',
    Gravi_ZZ,
    br = br_ZZ,
    signal = 'G_{RS}',
    finalstate = 'ZZ',
    nick = 'v4',
  )
