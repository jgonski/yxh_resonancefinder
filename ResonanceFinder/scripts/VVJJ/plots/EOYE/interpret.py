if __name__ == '__main__':
  import glob
  import ROOT

  # read p0s
  p0s = {}
  fp0 = open('minitrees/p0.txt')
  linesp0 = fp0.readlines()
  for l in linesp0:
    tokens = l.split(' ')
    inverted_analysis = tokens[0].split('_')
    analysis = '%s_%s' % (inverted_analysis[1], inverted_analysis[0])
    if analysis not in p0s: p0s[analysis] = {}
    mass = int(tokens[1])
    p0 = min(float(tokens[2]), 0.5)
    if mass in p0s[analysis]: raise RuntimeError('Read twice %s %d' % (analysis, mass))
    else: print 'Reading %s %d' % (analysis, mass)
    p0s[analysis][mass] = p0
  fp0.close()

  # replace list
  replacements = {} # obs, exp
  replacements['HVT_WZ'] = {}
  replacements['HVT_WZ'][1500] = [0.62, None]
  replacements['HVT_WZ'][1600] = [0.82, None]
  replacements['Gravi_ZZ'] = {}
  replacements['Gravi_ZZ'][1500] = [19, None]
  replacements['Gravi_ZZ'][1600] = [22.4, None]
  replacements['Gravi_ZZ'][1700] = [49, None]
  replacements['Gravi_ZZ'][1900] = [104, 100]
  replacements['Gravi_ZZ'][2000] = [120, 126]

  # removal list
  removals = [2400]

  ROOT.gROOT.SetBatch(True)

  files = {}

  for fname in glob.glob('*root*'):
    if 1:#try:
      f = ROOT.TFile.Open(fname)
      rold = f.Get('result_mu')
      if not rold: continue

     #r = ROOT.RooStats.HypoTestInverterResult('risultao')
     #r.UseCLs(True)
     #r.SetConfidenceLevel(0.95)

      r = rold # read the node below
      for i in xrange(rold.ArraySize()):
        continue # TODO VALERIO: this part was meant at removing failed points using our human wisdom, but there's something wrong in how RooStats deals with adding/removing points from HypoTestInverterResults - a bug or a feature?
        # (so we skip it)
#       print fname,i
        if rold.GetXValue(i) > 0.4 and rold.CLs(i) > 0.95 and rold.CLb(i) > 0.95:
#         print '   SKIPPED'
          continue
#       print '  ',r.FindIndex(rold.GetXValue(i)),rold.GetXValue(i)
        hti = rold.GetResult(i)
        r.Add(rold.GetXValue(i), hti)

      r.ExclusionCleanup() # how crucial!!!

      point = fname.replace('sum_', '').replace('.root', '') # Enrique's naming convention
      
      analysis = point[:-5].replace('G_', 'Gravi_') # move from Enrique's naming convention to everyone else's
      mass = int(point[-4:]) 

      if mass in removals:
        print 'WARNING removing point %s' % point
        continue

      if analysis not in files:
        files[analysis] = open('minitrees/%s.txt' % analysis, 'w')

      print '%s: obs=%.2f (err %.2f) exp=%.2f' % (point, r.UpperLimit(), r.UpperLimitEstimatedError(), r.GetExpectedUpperLimit(0))
      obs_lim = r.UpperLimit()
      exp_lim = r.GetExpectedUpperLimit(0)
      if analysis in replacements and mass in replacements[analysis]:
        if replacements[analysis][mass][1]:
          print 'WARNING replacing read observed result for %s %s with obs=%f exp=%f' % (analysis, point, replacements[analysis][mass][0], replacements[analysis][mass][1])
        else:
          print 'WARNING replacing read observed result for %s %s with obs=%f' % (analysis, point, replacements[analysis][mass][0])
        obs_lim = replacements[analysis][mass][0]
        if replacements[analysis][mass][1]:
          exp_lim = replacements[analysis][mass][1]

      files[analysis].write('%f %f %f %f\n' % (mass, obs_lim, r.GetExpectedUpperLimit(0), p0s[analysis][mass]))

      c = ROOT.TCanvas('c_%s' % point, point, 600, 600)
      p = ROOT.RooStats.HypoTestInverterPlot('p_%s' % point, point, r)
      p.Draw('2CL CLb')
      c.SaveAs('limit/c_%s.pdf' % point)
    #except:
    #  pass

for analysis in files:
  files[analysis].close()
