#include <vector>
#include "RooStats/HypoTestInverterResult.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TString.h"
#include "TFile.h"
#include "TLine.h"
#include "TF1.h"
#include "TROOT.h"
//TF1("f",TString::Format("[0]*2*ROOT::Math::chisquared_pdf(2*x,%d,0)",1),.001,5);

using std::vector;
void plot_test_stat(int mass, TString analysis="HVT_WW")
{
    gROOT->SetStyle("Modern");
    TString fname = TString::Format("sum_"+analysis+"_%d.root", mass);
    TFile *_file0 = TFile::Open(fname);
    RooStats::HypoTestInverterResult *result_mu = (RooStats::HypoTestInverterResult *) _file0->Get("result_mu");     
    //RooStats::HypoTestResult* res =  result_mu->GetResult(0); res->GetTestStatisticData()
    //TString fname = TString::Format("res_limit.root");
    //TFile *_file0 = TFile::Open(fname);
    //RooStats::HypoTestInverterResult *result_mu = (RooStats::HypoTestInverterResult *) _file0->Get("result_mu_WW_HVT_ws_" + TString::Format("%d", mass) + "p000");     
//  result_mu->ExclusionCleanup();
    const int npoints = result_mu->ArraySize();


    vector<TH1*> v_h_sb_dist;
    vector<TH1*> v_h_b_dist;
    vector<double> test_stat_val;
    double min;
    double max;
    for (int i_point = 0; i_point < npoints; i_point++)
    {
          RooStats::SamplingDistribution* sbDist = result_mu->GetSignalAndBackgroundTestStatDist(i_point);
          RooStats::SamplingDistribution* bDist = result_mu->GetBackgroundTestStatDist(i_point);
          // define with 250 bins
          // find range
          min = 0;
          max = -1;
          vector<double> sb_vd = sbDist->GetSamplingDistribution();
          vector<double> b_vd = bDist->GetSamplingDistribution();
          vector<double> sb_vw = sbDist->GetSampleWeights();
          vector<double> b_vw = bDist->GetSampleWeights();
          for (int i =0; i < sb_vd.size(); i++)
          { 
            double val_sb = sb_vd[i];
            double val_b = b_vd[i];
            if ( val_sb < 1e5 && val_sb > -10)
            {
                min = (min > val_sb) ? val_sb : min;
                max = (max < val_sb) ? val_sb : max;
            }
            if ( val_b < 1e5 && val_b > -10)
            {
                min = (min > val_b) ? val_b : min;
                max = (max < val_b) ? val_b : max;
            }
          }
          const double muval = result_mu->GetXValue(i_point);
          TString s_sb_name = TString::Format("sb_%d_%lf", i_point, muval);
          TString s_b_name = TString::Format("b_%d_%lf", i_point, muval);
          const Double_t cls = result_mu->GetResult(i_point)->CLs();
          TString s_sb_title = TString::Format("mu=%.2f CLs(obs)=%.4f", muval, cls);
          TString s_b_title = TString::Format("mu=%.2f CLs(obs)=%.4f", muval, cls);
          TH1* h_sb = new TH1D(s_sb_name, s_sb_title, 100, -10, 30);//max); // VALERIO
          h_sb->SetLineColor(kRed);
          TH1* h_b = new TH1D(s_b_name, s_b_title, 100, -10, 30);//max);  // VALERIO
          h_b->SetLineColor(kBlue);
          for (int i =0; i < sb_vd.size(); i++)
          {
            double val_sb = sb_vd[i];
            double val_b = b_vd[i];
            double w_sb = sb_vw[i];
            double w_b = b_vw[i];
            if ( val_sb < 1e5 && val_sb > -10 && w_sb == w_sb && val_sb == val_sb)
            {
                 h_sb->Fill(val_sb,w_sb);
            }
          //if ( val_b < 1e5 && val_b > -10 && val_b != 1 && w_b == w_b && val_sb == val_sb)
            if ( val_b < 1e5 && val_b > -10 && w_b == w_b && val_sb == val_sb) // TODO ENRIQUE
            {
                 h_b->Fill(val_b, w_b);
            }
          }
          h_b->Scale(1./h_b->Integral());
          h_sb->Scale(1./h_sb->Integral());
          v_h_b_dist.push_back(h_b);
          v_h_sb_dist.push_back(h_sb);
          double ts = result_mu->GetResult(i_point)->GetTestStatisticData();
          //cout << result_mu->GetXValue(i_point) << "\t" << ts << endl;  
          test_stat_val.push_back(ts);
      }
      TCanvas* k;
      for (int i_point = 0; i_point < npoints; i_point++)
      {
           int serial = (i_point % 9);
           if (serial == 0)
           {
                 k = new TCanvas();
                 k->SetName(TString::Format("c_%s_%d_%d", analysis.Data(), mass, i_point));
                 k->Divide(3,3);
           }
           TVirtualPad* p = k->cd(serial+1);
           p->SetLogy();
           TF1* chi2fun = new TF1(TString::Format("chi2fun%i",i_point),TString::Format("[0]*2*ROOT::Math::chisquared_pdf(2*x,%d,0)",1),0,5);
           chi2fun->SetLineColor(kMagenta);
           chi2fun->SetLineWidth(2);
            v_h_sb_dist[i_point]->Fit(chi2fun);

std::cout << __LINE__ << std::endl;
           double maxY = v_h_b_dist[i_point]->GetMaximum() > v_h_sb_dist[i_point]->GetMaximum() ? v_h_b_dist[i_point]->GetMaximum() : v_h_sb_dist[i_point]->GetMaximum();
  
           v_h_b_dist[i_point]->SetMaximum(maxY);
           v_h_sb_dist[i_point]->SetMaximum(maxY);
           v_h_b_dist[i_point]->DrawNormalized();
           v_h_sb_dist[i_point]->DrawNormalized("same");           
            v_h_sb_dist[i_point]->Draw("same");
std::cout << __LINE__ << std::endl;
           TLine* l = new TLine(test_stat_val[i_point], 1e-3, (test_stat_val[i_point]) ,v_h_b_dist[i_point]->GetMaximum());
           l->Draw();

std::cout << __LINE__ << std::endl;
          double probs[5] = {0.025,0.16,0.5,0.84,0.975};
          double qantsx[5];
           v_h_b_dist[i_point]->GetQuantiles(5,qantsx,probs);
           for (int iq = 0; iq <= 4; iq++)
           {
           l = new TLine(qantsx[iq], 3e-1, (qantsx[iq]) ,v_h_b_dist[i_point]->GetMaximum()); l->SetLineColor(kGreen+1);
           l->SetLineWidth(2);
           l->Draw();
          }
       
std::cout << __LINE__ << std::endl;
          std::cout << TString(k->GetName()) << std::endl;
          k->SaveAs(TString("teststat/") + TString(k->GetName()) + ".pdf");
std::cout << __LINE__ << std::endl;
      }
      
      // here we find the CLb , CLs+b and CLs (for the expectation bands)
      for (int i_point = 0; i_point < npoints; i_point++)
      {
          double m2s_b = -1;
          double m1s_b = -1;
          double  med_b = -1;
          double  p1s_b = -1;
          double p2s_b = -1;
          double clsb_m2s;
          double clsb_m1s;
          double clsb_med;
          double clsb_p1s;
          double clsb_p2s;
          TH1* h_sb = v_h_sb_dist[i_point];
          TH1* h_b = v_h_b_dist[i_point];
          //CLs =  CLs+b/CLb
          // CL stands for confidence level

          // what is mu such that CLb = 0.5
          // what is the CLs+b for that mu
          double probs[5] = {0.025,0.16,0.5,0.84,0.975};
          double qantsx[5];
          h_b->GetQuantiles(5,qantsx,probs);
          //cout << qantsx[0] << " " << qantsx[1] << " " << qantsx[2] << " " << qantsx[3] << " " << qantsx[4] << endl;
          clsb_m2s = h_sb->Integral(h_sb->FindBin(qantsx[0]), h_sb->GetNbinsX()) / h_sb->Integral(); 
          clsb_m1s = h_sb->Integral(h_sb->FindBin(qantsx[1]), h_sb->GetNbinsX()) / h_sb->Integral();
          clsb_med = h_sb->Integral(h_sb->FindBin(qantsx[2]), h_sb->GetNbinsX()) / h_sb->Integral();
          clsb_p1s = h_sb->Integral(h_sb->FindBin(qantsx[3]), h_sb->GetNbinsX()) / h_sb->Integral();
          clsb_p2s = h_sb->Integral(h_sb->FindBin(qantsx[4]), h_sb->GetNbinsX()) / h_sb->Integral();

          h_sb->SetTitle(TString::Format("%s CLs(exp)=%.2f", h_sb->GetTitle(), clsb_med/0.5));
          h_b->SetTitle(TString::Format("%s CLs(exp)=%.2f", h_b->GetTitle(), clsb_med/0.5));
//          for (int i_bin = 1; i_bin < h_sb->GetNbinsX(); i_bin++)
//          {
//              double qantiles[5] = {0.025,0.16,0.5,0.84,0.975};
//              double x[5];
//             h_b->GetQuantiles(5,qantiles, x);
//              cout << x[0] << " " << x[1] << " " << x[2] << " " << x[3] << " " << x[4] << endl; 
//          }
            double mu = result_mu->GetXValue(i_point);
            std::cout << mu << " \t " << clsb_m2s / 0.025 << " " << clsb_m1s / 0.16  << " " << clsb_med / 0.5  << " " << clsb_p1s / 0.84  << " " << clsb_p2s / 0.975 << std::endl; 
          // std::cout << mu << " \t " << clsb_med / 0.5 << "\t"  << h_b->Integral(h_b->FindBin(qantsx[2]), h_b->GetNbinsX())/ h_b->Integral()<<  endl;
                
                    

      }
      
}
