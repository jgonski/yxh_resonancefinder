#!/usr/bin/env python
import ROOT, sys

def readFile(fname, xsec, br=0.699*0.676, signal='W\'', finalstate='WZ', nick=''):
  f = ROOT.TFile.Open(fname)
  if not f: return

  t = f.Get('stats')
  if not t: return

  printout_mu = []
  printout_xsec = []

  printout_mu.append('\n\n%%%s (signal strength limit)' % nick)
  printout_xsec.append('\n\n%%%s (signal xsec / BRqqqq limit)' % nick)

  if signal == 'W\'':
    signallong = 'HVT W\'(' + finalstate + ')'
  elif signal == 'Z\'':
    signallong = 'HVT Z\'(' + finalstate + ')'
  elif signal == 'V\'':
    signallong = 'HVT V\'(' + finalstate + ')'
  elif 'KK' in signal:
    signallong = 'KK Graviton(' + finalstate + ')'
  else:
    raise RuntimeError('Not implemented signal %s' % signal)

  header_xsec = '''\\begin{table}
\\scriptsize
\\begin{center}
\\begin{tabular}{c|ccccccc }
\\hline
\\hline
 ''' + signallong + '''  & \\multicolumn{6}{c}{${\ensuremath{\\sigma \\times \\cal{B}}} (''' + signal + '''\\to ''' + finalstate + ''')$  95\\% CL Limits [fb]}  \\\\
\\hline
\\hline
 $m(''' + signal + ''')$ [TeV]   &   observed   &  \\multicolumn{5}{c}{expected} \\\\
                &            &   $-2\\sigma$   &    $-1\\sigma$ & median &  $+1\\sigma$ &  $+2\\sigma$ \\\\\\hline
'''
  
  header_mu = header_xsec.replace('\\sigma\\times BR', '\mu ')

  footer = '''
\\hline
\\hline
\\end{tabular}
\\end{center}
\\end{table}
'''

  titleString = '$m_{signal}$   &  ${what}$ observed 95\\% CL upper limit  &  expected $-2\\sigma$   &   expected $-1\\sigma$ & expected & expected $+1\\sigma$ & expected $+2\\sigma$ \\\\\\hline'
  printout_mu.append( header_mu )
  printout_xsec.append( header_xsec )

  for entry in xrange(t.GetEntries()):
    t.GetEntry(entry)

    #$2400$ & $0.50$ & $9.7$ & $6.2$ & $8.0$ & $12$ & $26$ & $43$ \\\\
    lineString_mu = '${mass}$ & ${obs}$ & ${exp_m2}$ & ${exp_m1}$ & ${exp}$ & ${exp_p1}$ & ${exp_p2}$ \\\\'
    lineString_xsec = '${mass:.0f}$ & ${obs}$ & ${exp_m2}$ & ${exp_m1}$ & ${exp}$ & ${exp_p1}$ & ${exp_p2}$ \\\\'

    # TODO: significance with ROOT::Math::normal_quantile_c(pval, 1)

    printout_mu.append( lineString_mu.format(
      mass=t.point,
      p0=round(t.null_pvalue, 3), # VALERIO
      obs=round(t.obs_upperlimit, 3),
      exp_m2=round(t.exp_upperlimit_minus2, 3),
      exp_m1=round(t.exp_upperlimit_minus1, 3),
      exp=round(t.exp_upperlimit, 3),
      exp_p1=round(t.exp_upperlimit_plus1, 3),
      exp_p2=round(t.exp_upperlimit_plus2, 3),
    ) )

    mu_to_xsec = xsec[t.point] / br

    tmp_obs=t.obs_upperlimit * mu_to_xsec
    tmp_exp_m2=t.exp_upperlimit_minus2 * mu_to_xsec
    tmp_exp_m1=t.exp_upperlimit_minus1 * mu_to_xsec
    tmp_exp=t.exp_upperlimit * mu_to_xsec
    tmp_exp_p1=t.exp_upperlimit_plus1 * mu_to_xsec
    tmp_exp_p2=t.exp_upperlimit_plus2 * mu_to_xsec

    if tmp_obs >= 10.05: proper_obs = '%.0f' % tmp_obs
    else: proper_obs = '%.1f' % tmp_obs
    if tmp_exp_m2 >= 10.05: proper_exp_m2 = '%.0f' % tmp_exp_m2
    else: proper_exp_m2 = '%.1f' % tmp_exp_m2
    if tmp_exp_m1 >= 10.05: proper_exp_m1 = '%.0f' % tmp_exp_m1
    else: proper_exp_m1 = '%.1f' % tmp_exp_m1
    if tmp_exp >= 10.05: proper_exp = '%.0f' % tmp_exp
    else: proper_exp = '%.1f' % tmp_exp
    if tmp_exp_p1 >= 10.05: proper_exp_p1 = '%.0f' % tmp_exp_p1
    else: proper_exp_p1 = '%.1f' % tmp_exp_p1
    if tmp_exp_p2 >= 10.05: proper_exp_p2 = '%.0f' % tmp_exp_p2
    else: proper_exp_p2 = '%.1f' % tmp_exp_p2

    printout_xsec.append( lineString_xsec.format(
      mass=t.point,
      p0=t.null_pvalue, # VALERIO
      obs=proper_obs,
      exp_m2=proper_exp_m2,
      exp_m1=proper_exp_m1,
      exp=proper_exp,
      exp_p1=proper_exp_p1,
      exp_p2=proper_exp_p2,
    ) )

  printout_mu.append(footer)
  printout_xsec.append(footer)

  print '\n'.join(printout_mu)
  print '\n'.join(printout_xsec)


if __name__ == '__main__':

  br_WZ = 0.699*0.676
  br_WW = 0.676*0.676
  br_ZZ = 0.699*0.699

# Wprime, WZ
  Wprime_WZ = {
  1100: 128.93,
  1200: 88.662,
  1300: 61.908,
  1400: 44.482,
  1500: 32.327,
  1600: 23.77,
  1700: 17.775,
  1800: 13.422,
  1900: 10.267,
  2000: 7.8715,
  2100: 6.118,
  2200: 4.7763,
  2300: 3.7325,
  2400: 2.9662,
  2500: 2.3708,
  3000: 0.80858,
  }

# HVT, WZ
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DBCombRun2
  HVT_WZ = {
  1000: 225.7,
  1100: 148.4,
  1200: 100.5,
  1300: 69.79,
  1400: 49.47,
  1500: 35.68,
  1600: 26.08,
  1700: 19.33,
  1800: 14.46,
  1900: 10.934,
  2000: 8.340,
  2200: 4.952,
  2400: 3.010,
  2600: 1.866,
  2800: 1.172,
  3000: 0.747,
  3500: 0.251,
  4000: 0.087,
  4500: 0.0307,
  5000: 0.0109,
  }

# Gravi, WW
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/%20VVVHCombRun2#Signal_samples_used
# k = 0.5
  GraviC05_WW = {
  1000: 63.98*0.25,
  1100: 39.57*0.25,
  1200: 24.45*0.25,
  1300: 15.08*0.25,
  1400: 9.32*0.25,
  1500: 5.76*0.25,
  1600: 3.89*0.25,
  1700: 2.64*0.25,
  1800: 1.79*0.25,
  1900: 1.25*0.25,
  2000: 0.873*0.25,
  2200: 0.462*0.25,
  2400: 0.242*0.25,
  2600: 0.133*0.25,
  2800: 0.0731*0.25,
  3000: 0.0411*0.25,
  3500: 0.0107*0.25,
  4000: 0.00297*0.25,
  4500: 0.000823*0.25,
  5000: 0.000228*0.25,
  }


# Gravi, WW
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/%20VVVHCombRun2#Signal_samples_used
# k = 1
  Gravi_WW = {
  1000: 63.98,
  1100: 39.57,
  1200: 24.45,
  1300: 15.08,
  1400: 9.32,
  1500: 5.76,
  1600: 3.89,
  1700: 2.64,
  1800: 1.79,
  1900: 1.25,
  2000: 0.873,
  2200: 0.462,
  2400: 0.242,
  2600: 0.133,
  2800: 0.0731,
  3000: 0.0411,
  3500: 0.0107,
  4000: 0.00297,
  4500: 0.000823,
  5000: 0.000228,
  }

# HVT, WW
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DBCombRun2
  HVT_WW = {
  1000: 105.424,
  1100: 69.095,
  1200: 46.657,
  1300: 32.276,
  1400: 22.776,
  1500: 16.355,
  1600: 11.918,
  1700: 8.792,
  1800: 6.562,
  1900: 4.944,
  2000: 3.760,
  2200: 2.221,
  2400: 1.345,
  2600: 0.831,
  2800: 0.521,
  3000: 0.332,
  3500: 0.111,
  4000: 0.0374,
  4500: 0.0138,
  5000: 0.00495,
  }

#Gravi, ZZ
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/%20VVVHCombRun2#Signal_samples_used
# k = 0.5
  GraviC05_ZZ = {
  1000: 34.886*0.25,
  1100: 21.498*0.25,
  1200: 13.241*0.25,
  1300: 8.160*0.25,
  1400: 5.033*0.25,
  1500: 3.103*0.25,
  1600: 2.101*0.25,
  1700: 1.422*0.25,
  1800: 0.963*0.25,
  1900: 0.674*0.25,
  2000: 0.469*0.25,
  2200: 0.249*0.25,
  2400: 0.132*0.25,
  2600: 0.0684*0.25,
  2800: 0.0391*0.25,
  3000: 0.0244*0.25,
  3500: 0.00572*0.25,
  4000: 0.00161*0.25,
  4500: 0.000440*0.25,
  5000: 0.000147*0.25,
  }


#Gravi, ZZ
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/%20VVVHCombRun2#Signal_samples_used
# k = 1
  Gravi_ZZ = {
  1000: 34.886,
  1100: 21.498,
  1200: 13.241,
  1300: 8.160,
  1400: 5.033,
  1500: 3.103,
  1600: 2.101,
  1700: 1.422,
  1800: 0.963,
  1900: 0.674,
  2000: 0.469,
  2200: 0.249,
  2400: 0.132,
  2600: 0.0684,
  2800: 0.0391,
  3000: 0.0244,
  3500: 0.00572,
  4000: 0.00161,
  4500: 0.000440,
  5000: 0.000147,
  }

  Scalar_WW = {
  1200 : 1000 * br_WW, 
  1400 : 1000 * br_WW, 
  1600 : 1000 * br_WW, 
  1800 : 1000 * br_WW, 
  2000 : 1000 * br_WW, 
  2200 : 1000 * br_WW, 
  2400 : 1000 * br_WW, 
  2600 : 1000 * br_WW, 
  2800 : 1000 * br_WW, 
  3000 : 1000 * br_WW, 
  }

  Scalar_ZZ = {
  1200 : 1000 * br_ZZ, 
  1400 : 1000 * br_ZZ, 
  1600 : 1000 * br_ZZ, 
  1800 : 1000 * br_ZZ, 
  2000 : 1000 * br_ZZ, 
  2200 : 1000 * br_ZZ, 
  2400 : 1000 * br_ZZ, 
  2600 : 1000 * br_ZZ, 
  2800 : 1000 * br_ZZ, 
  3000 : 1000 * br_ZZ, 
  }
 
# gravi->WW or ZZ
  Scalar_WWorZZ = Scalar_WW
  for (theMass, theXsec) in Scalar_WW.items():
    Scalar_WWorZZ[theMass] = theXsec/br_WW 
  for (theMass, theXsec) in Scalar_ZZ.items():
    Scalar_WWorZZ[theMass] += theXsec/br_ZZ 

# HVT WW or WZ
  HVT_WWorWZ = HVT_WZ 
  for (theMass, theXsec) in HVT_WZ.items():
    HVT_WWorWZ[theMass] = theXsec/br_WZ 
  for (theMass, theXsec) in HVT_WW.items():
    HVT_WWorWZ[theMass] += theXsec/br_WW 

# gravi->WW or ZZ
  Gravi_WWorZZ = Gravi_WW
  for (theMass, theXsec) in Gravi_WW.items():
    Gravi_WWorZZ[theMass] = theXsec/br_WW 
  for (theMass, theXsec) in Gravi_ZZ.items():
    Gravi_WWorZZ[theMass] += theXsec/br_ZZ 

# gravi->WW or ZZ c=0.5
  GraviC05_WWorZZ = GraviC05_WW
  for (theMass, theXsec) in GraviC05_WW.items():
    GraviC05_WWorZZ[theMass] = theXsec/br_WW 
  for (theMass, theXsec) in GraviC05_ZZ.items():
    GraviC05_WWorZZ[theMass] += theXsec/br_ZZ 


  #
  if len(sys.argv)>=2: dir=sys.argv[1]
  else:                dir="merged"


  print '\n\n%%%%%%%%%%%%%% HVT WWorWZ'
  readFile(dir+'/HVT_WWorWZ-obs.root', 
    HVT_WWorWZ,
    br = 1,
    signal = 'V\'',
    finalstate = 'WW + WZ',
    nick = 'paper2017',
  )
  print '\n\n%%%%%%%%%%%%%% Gravi WWorZZ c = 1.0'
  readFile(dir+'/Gravi_WWorZZ-obs.root',
    Gravi_WWorZZ,
    br = 1.0,
    signal = '{\ensuremath{G_{\mathrm{KK}}}}',
    finalstate = 'WW + ZZ',
    nick = 'paper2017',
  )
  print '\n\n%%%%%%%%%%%%%% Scalar WWorZZ'
  readFile(dir+'/Scalar_WWorZZ-obs.root',
    Scalar_WWorZZ,
    br = 1.0,
    signal = '{\ensuremath{G_{\mathrm{KK}}}}',
    finalstate = 'WW + ZZ',
    nick = 'paper2017',
  )

  print '\n\n%%%%%%%%%%%%%% GraviC05 WWorZZ c = 0.5'
  readFile(dir+'/GraviC05_WWorZZ-obs.root',
    GraviC05_WWorZZ,
    br = 1.0,
    signal = '{\ensuremath{G_{\mathrm{KK}}}}',
    finalstate = 'WW + ZZ',
    nick = 'paper2017',
  )


