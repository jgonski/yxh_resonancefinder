#!/usr/bin/env python

import sys
#import ROOT
import os
import numpy as np

if len(sys.argv)<2:
    print """
Usage: 
  python %prog [inflag] [outflag] [massLo] [massHi] [massStep] [sfLo] [sfHi] [sfStep]

"""
    sys.exit()



inflag = sys.argv[1]
outflag = sys.argv[2]

massLo = int(sys.argv[3])
massHi = int(sys.argv[4])
massStep = int(sys.argv[5])

sfLo = float(sys.argv[6])
sfHi = float(sys.argv[7])
sfStep = float(sys.argv[8])

print "Arg 1: %s" % inflag 
print "Arg 2: %s" % outflag 
print "Arg 3: %d" % massLo
print "Arg 4: %d" % massHi
print "Arg 5: %d" % massStep
print "Arg 6: %f" % sfLo
print "Arg 7: %f" % sfHi
print "Arg 8: %f" % sfStep


outDir = "."

for mass in range(massLo, massHi, massStep):
    for sf in np.arange(sfLo, sfHi, sfStep):
        print mass 
        print sf
        print outDir
        print outflag

        if(sf<10):
            outfile = "{}/peCombined_{}_{}_{}.root".format(outDir, outflag, mass, sf)
        else:
            outfile = "{}/peCombined_{}_{}_{:.0f}.root".format(outDir, outflag, mass, sf)

        print outfile

        if sf > 9.99: 
            inpath = "{}*_{}_{:.0f}_*".format(inflag, mass, sf)
        else: 
            inpath = "{0}*_{1}_{2}_*".format(inflag, mass, sf)

        command = "hadd -f {0} {1}/*.root".format(outfile, inpath)

        print command

        os.system(command)


#  LocalWords:  argv
