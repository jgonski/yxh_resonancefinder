#!/usr/bin/env python

import sys
#import ROOT
import os
import numpy as np

class cfile(file):
    #subclass file to have a more convienient use of writeline
    def __init__(self, name, mode = 'w'):
        self = file.__init__(self, name, mode)

    def wl(self, string):
        self.writelines(string + '\n')
        return None

if len(sys.argv)<2:
    print """
Usage: 
  python %prog [inflag] [outflag] [massLo] [massHi] [massStep] [sfLo] [sfHi] [sfStep]

"""
    sys.exit()


workspace = sys.argv[1]
jobName = sys.argv[2]
imass = int(sys.argv[3])
sfLo = float(sys.argv[4])
sfHi = float(sys.argv[5])
sfStep = float(sys.argv[6])
nToysNull = int(sys.argv[7])
nToysTest = int(sys.argv[8])
nJobs = int(sys.argv[9])
doPFLH = int(sys.argv[10])

print "Arg 1: %s" % workspace
print "Arg 2: %s" % jobName
print "Arg 3: %s" % imass
print "Arg 4: %f" % sfLo
print "Arg 5: %f" % sfHi
print "Arg 6: %f" % sfStep
print "Arg 7: %d" % nToysNull
print "Arg 8: %d" % nToysTest
print "Arg 9: %d" % nJobs
print "Arg 10: %d" % doPFLH


topDir = "/msu/data/t3work8/wfisher/jobs/"
outDir = topDir+"/ToyJobs/"
condorFile = "condorPval.submit"

for isf in np.arange(sfLo, sfHi, sfStep):
    
    print "\nWorkspace: %s" % workspace
    print "Processing scalefactor: %s" % isf
    print "Output sent to: %s" % outDir
    
    for ijob in range(0, nJobs, 1):
        print "\nSubmitting job: %s" % ijob

        ident = "%s_%s_%s" % (jobName, isf, ijob)

        scriptName = "batchPval_"+ident+".sh"
        absScript = topDir + scriptName
        logFile = "pvalueLog_"+ident+"log"
        outPath = outDir+"pvalues_"+ident+"/"

        stdOut = outPath+"condorPval.stdout"
        stdErr = outPath+"condorErr.stderr"
        stdLog = outPath+"condorLog.log"

        command = "mkdir "+ outPath
        print outPath

        os.system(command)

        f = cfile(condorFile)
        f.wl("Universe=vanilla")
        f.wl("getenv=true")
        f.wl("Executable="+scriptName)
        f.wl("Input=/dev/null")
        f.wl("Output="+stdOut)
        f.wl("Error="+stdErr)
        f.wl("Log="+stdLog)
        f.wl("Queue")
        f.close()
        
        f = cfile(scriptName)
        f.wl("#!/bin/sh")
        f.wl("echo `/bin/hostname`")
        f.wl("cd $TMPDIR")
        f.wl("echo '1: my directory is'")
        f.wl("pwd")

        f.wl("mkdir -p tmp")
        f.wl("cd tmp")

        f.wl("pwd")
        f.wl("echo '2: do my setups'")

        f.wl("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh")
        f.wl("lsetup root");

        f.wl("cp "+topDir+"getPvalues.py .")
        f.wl("cp "+topDir+"frequentistPvalueCalc.C .")
        f.wl("echo '3: run the job'")
        f.wl("python getPvalues.py %s %s %s %s %s %s %s" % (workspace, jobName, imass, nToysNull, nToysTest, isf, doPFLH))

        f.wl("mv *.root %s" % outPath)
        f.wl("mv *.pdf %s" % outPath)
        f.wl("cd ..")
        f.wl("ls -ltrah")
        f.wl("echo '4: done, now clean up")
        f.wl("rm -rf tmp/")
        f.close()

        os.system("chmod u+x "+scriptName)
        os.system("condor_submit "+condorFile)
        os.system("usleep 300")

#  LocalWords:  argv
