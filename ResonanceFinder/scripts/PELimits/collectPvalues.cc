#include <fstream>
#include <iostream>

void parseFile(const char* name, double& mu){
  TString parse(name);
  TObjArray* tokens = parse.Tokenize("_");

  TObjString* lastToken = (TObjString*) (tokens->Last());
  TString lastString = lastToken->GetString();
  lastString.ReplaceAll(".root","");
  mu = lastString.Atof();

  return;
}

double getErfc(double x, double a, double b, double c, double d){
  double retval = a + b*TMath::Erfc(c*x+d);
  return retval;
}

void calculateLimit(double& cls95, TGraph* gr, bool doFit){

  if(doFit){
    double xlo, xhi, dummy;
    gr->GetPoint(0,xlo,dummy);
    gr->GetPoint(gr->GetN()-1,xhi,dummy);
    TF1* f1 = new TF1("f1","[0]+[1]*TMath::Erfc([2]*x+[3])",xlo,xhi);
    int status = gr->Fit(f1,"Q");
    cls95 = f1->GetX(0.05);

    if(status!=0){
      cout << "Error: Fit status was " << status << ", recovering..." << endl;

      TF1* f9 = new TF1("f9","pol9",xlo,xhi);
      status = gr->Fit(f9,"Q");
      status = gr->Fit(f1,"Q");
      cls95 = f1->GetX(0.05);
      if(status!=0){ cls95 = -1; cout <<"Could not recover, giving up..." << endl; return;}
    }

    double sa = f1->GetParError(0);
    double sb = f1->GetParError(1);
    double sc = f1->GetParError(2);
    double sd = f1->GetParError(3);

    double a = f1->GetParameter(0);
    double b = f1->GetParameter(1);
    double c = f1->GetParameter(2);
    double d = f1->GetParameter(3);

    f1->FixParameter(0,a+sa);
    gr->Fit(f1,"Q");
    f1->ReleaseParameter(0);
    double sssa = cls95 - f1->GetX(0.05);
    f1->FixParameter(1,b+sb);
    gr->Fit(f1,"Q");
    f1->ReleaseParameter(1);
    double sssb = cls95 - f1->GetX(0.05);
    f1->FixParameter(2,c+sc);
    gr->Fit(f1,"Q");
    f1->ReleaseParameter(2);
    double sssc = cls95 - f1->GetX(0.05);
    f1->FixParameter(3,d+sd);
    gr->Fit(f1,"Q");
    f1->ReleaseParameter(3);
    double sssd = cls95 - f1->GetX(0.05);

    double clsErr = sssa*sssa + sssb*sssb + sssc*sssc + sssd*sssd;
    clsErr = sqrt(clsErr)/2.0;
    cout << "Fit found: " << status << " " << cls95 << " +/- " << clsErr << endl;;    
    f1->Delete();
  }
  else{
    double loBound[2];
    double hiBound[2];
    double x, y;
    bool bracketed = false;
    for(int i=0; i<gr->GetN(); i++){
      if(bracketed) break;
      gr->GetPoint(i,x,y);
      if(y>0.05){
	loBound[0] = x;
	loBound[1] = y;
      }
      else{
	hiBound[0] = x;
	hiBound[1] = y;
	bracketed = true;
      }
    }
    cout << "Bracketed at: " << loBound[0] << " " << loBound[1] << endl;
    cout << "         and: " << hiBound[0] << " " << hiBound[1] << endl;
    cls95 = loBound[0] + (hiBound[0]-loBound[0])/(1e-9+hiBound[1]-loBound[1])*(0.05-loBound[1]);
    cout << "Extrap found: " << cls95 << endl;
  }
}

void getCLvals(TH1F* tHist, TH1F* nHist, int refBin, bool isPFLH, double& clb, double& clsb, double& cls){
  int minBin = 1;
  if(isPFLH){
    tHist->SetBinContent(tHist->FindBin(-1),0);
    nHist->SetBinContent(tHist->FindBin(-1),0);
  }
  if(!isPFLH){
    clsb = tHist->Integral(minBin,refBin)/tHist->Integral(minBin,tHist->GetNbinsX()+1);
    //   cout << "Refbin: " << refBin << " , " << tHist->GetBinCenter(refBin) << " , " << clsb << endl;

    if(TMath::IsNaN(clsb)) cout << "NAN CLSB" << endl;
    clb = nHist->Integral(minBin,refBin)/nHist->Integral(minBin,nHist->GetNbinsX()+1);
    cls = clsb/clb;
  }
  else{
    clsb = tHist->Integral(refBin,tHist->GetNbinsX()+1)/tHist->Integral(minBin,tHist->GetNbinsX()+1);
    if(TMath::IsNaN(clsb)) cout << "NAN CLSB" << endl;
    clb = nHist->Integral(refBin,nHist->GetNbinsX()+1)/nHist->Integral(minBin,nHist->GetNbinsX()+1);
    cls = clsb/clb;
  }
  /*
  clb = 1-clb;
  clsb = 1-clsb;
  cls = 1-cls;
  */
  return;
}

vector<int> getMedBins(TH1F* hist, bool isPFLH){
  int medbin = 1;
  int p1sbin = 1;
  int p2sbin = 1;
  int m1sbin = 1;
  int m2sbin = 1;

  if(isPFLH){
    hist->SetBinContent(hist->FindBin(-1),0);
  }

  double intLast = -1;
  double intThis = 2;
  int minBin = 1;
  //  if(isPFLH) minBin = hist->FindBin(0);
  double tot = hist->Integral(minBin,hist->GetNbinsX()+1);

  for(int i=minBin; i<=hist->GetNbinsX()+1; i++){
    intThis = hist->Integral(minBin,i)/tot;

    if(intThis>TMath::Erfc(2.0/sqrt(2))/2.0 && intLast<TMath::Erfc(2.0/sqrt(2.))/2.){
      m2sbin = i;
      //      cout << "Found " << TMath::Erfc(2.0/sqrt(2.))/2. << " at " << i << endl;
    }
    else if(intThis>TMath::Erfc(1.0/sqrt(2.))/2. && intLast<TMath::Erfc(1.0/sqrt(2.))/2.){
      m1sbin = i;
      //      cout << "Found " << TMath::Erfc(1.0/sqrt(2.))/2. << " at " << i << endl;
    }
    else if(intThis>0.50 && intLast<0.50){
      medbin = i;
      //      cout << "Found " << 0.50 << " at " << i << endl;
    }
    else if(intThis>TMath::Erfc(-1.0/sqrt(2.))/2. && intLast<TMath::Erfc(-1.0/sqrt(2.))/2.){
      p1sbin = i;
      //      cout << "Found " << TMath::Erfc(-1.0/sqrt(2.))/2. << " at " << i << endl;
    }
    else if(intThis>TMath::Erfc(-2.0/sqrt(2.))/2. && intLast<TMath::Erfc(-2.0/sqrt(2.))/2.){
      p2sbin = i;
      //      cout << "Found " << TMath::Erfc(-2.0/sqrt(2.))/2. << " at " << i << endl;
      break;
    }

    intLast = intThis;
  }
  vector<int> retv;
  retv.push_back(p2sbin);
  retv.push_back(p1sbin);
  retv.push_back(medbin);
  retv.push_back(m1sbin);
  retv.push_back(m2sbin);
  return retv;
}

void collectPvalues(TString infile = "peFiles.txt"){

  gStyle->SetOptStat(0);

  char fname[256];
  ifstream fileList;
  fileList.open(infile);

  bool doplot = 0;
  bool isPFLH = 0;
  double qObs = 0;
  double qMed = 0;
  
  vector<double> CLsbV;
  vector<double> CLbV;
  vector<double> CLbObsV;
  vector<double> CLsbObsV;
  vector<double> CLsMedV;
  vector<double> CLsObsV;
  vector<double> CLsp2sV;
  vector<double> CLsp1sV;
  vector<double> CLsm1sV;
  vector<double> CLsm2sV;
  vector<double> MuV;

  vector<int> medBins;
  double cls, clsb, clb, clsobs;
  int totPEs = 0;
  double mu = 0;
  double muHi = 0;
  double muLo = 1e9;
  while(fileList >> fname){
    parseFile(fname,mu);

    cout << fname << endl;
    TFile* fin = new TFile(fname);
    TH1F* tHist  = (TH1F*)fin->Get("peTEST"); 
    TH1F* nHist  = (TH1F*)fin->Get("peNULL"); 
    
    TVectorD* qObsV = (TVectorD*)fin->Get("qObs");
    qObs = (*qObsV)[0];
      
    TVectorD* qMedV = (TVectorD*)fin->Get("qMed");
    qMed = (*qMedV)[0];


    if(mu<muLo) muLo = mu;
    if(mu>muHi) muHi = mu;
    
    cout << "Mu = " << mu << endl;
    cout << tHist->GetEntries() << " " << tHist->Integral(0,tHist->GetNbinsX()+1) << endl;
    cout << nHist->GetEntries() << " " << nHist->Integral(0,nHist->GetNbinsX()+1) << endl;
    totPEs += tHist->GetEntries();
    totPEs += nHist->GetEntries();
    cout << "Qobs: " << qObs << endl;
    cout << "Qmed: " << qMed << endl;
    medBins = getMedBins(nHist,isPFLH);
    cout << "MedBin: " << medBins[2] << endl;
    cout << "M2S: " << tHist->GetBinCenter(medBins[0]) << endl;
    cout << "M1S: " << tHist->GetBinCenter(medBins[1]) << endl;
    cout << "Med: " << tHist->GetBinCenter(medBins[2]) << endl;
    cout << "P1S: " << tHist->GetBinCenter(medBins[3]) << endl;
    cout << "P2S: " << tHist->GetBinCenter(medBins[4]) << endl;

    for(int r=0; r<5; r++){
      getCLvals(tHist,nHist,medBins[r],isPFLH,clb,clsb,cls);

      if(r==0) CLsp2sV.push_back(cls);
      if(r==1) CLsp1sV.push_back(cls);
      if(r==3) CLsm1sV.push_back(cls);
      if(r==4) CLsm2sV.push_back(cls);
      if(r==2){
	cout << "CLsb: " << clsb << endl;
	cout << "CLb: " << clb << endl;
	cout << "CLs: " << cls << endl;
      	CLsbV.push_back(clsb);
	CLbV.push_back(clb);
	CLsMedV.push_back(cls);
      }
    }
    getCLvals(tHist,nHist,tHist->FindBin(qObs),isPFLH,clb,clsb,cls);
    cout << "CLsbObs: " << clsb << endl;
    cout << "CLbObs: " << clb << endl;
    cout << "CLsObs: " << cls << endl;
    CLsObsV.push_back(cls);
    CLbObsV.push_back(clb);
    CLsbObsV.push_back(clsb);
    MuV.push_back(mu);

    if(doplot){    
      TCanvas* c1 = new TCanvas("1","1");
      tHist->Draw();
      
      TCanvas* c2 = new TCanvas("2","2");
      nHist->Draw();
      
      TCanvas* c3 = new TCanvas("3","3");
      nHist->Draw("hist");
      tHist->Draw("histsame");
    }
    else 
      fin->Close();    
  }
  fileList.close();

  cout << "Done! " << totPEs << endl;
  TGraph* grCLsb = new TGraph(MuV.size(),&MuV[0],&CLsbV[0]);
  TGraph* grCLsbObs = new TGraph(MuV.size(),&MuV[0],&CLsbObsV[0]);
  TGraph* grCLb = new TGraph(MuV.size(),&MuV[0],&CLbObsV[0]);
  TGraph* grCLsObs = new TGraph(MuV.size(),&MuV[0],&CLsObsV[0]);
  TGraph* grCLsMed = new TGraph(MuV.size(),&MuV[0],&CLsMedV[0]);
  TGraph* grCLsP1S = new TGraph(MuV.size(),&MuV[0],&CLsp1sV[0]);
  TGraph* grCLsP2S = new TGraph(MuV.size(),&MuV[0],&CLsp2sV[0]);
  TGraph* grCLsM1S = new TGraph(MuV.size(),&MuV[0],&CLsm1sV[0]);
  TGraph* grCLsM2S = new TGraph(MuV.size(),&MuV[0],&CLsm2sV[0]);

  grCLsb->SetLineColor(2);
  grCLsb->SetLineWidth(3);
  grCLsbObs->SetLineColor(2);
  grCLsbObs->SetLineWidth(3);
  grCLsbObs->SetLineStyle(2);
  grCLb->SetLineColor(4);
  grCLb->SetLineWidth(3);
  grCLsObs->SetLineColor(1);
  grCLsObs->SetLineWidth(3);
  grCLsObs->SetLineStyle(2);
  grCLsMed->SetLineColor(1);
  grCLsMed->SetLineWidth(3);
  grCLsP1S->SetLineColor(2);
  grCLsP1S->SetLineWidth(3);
  grCLsM1S->SetLineColor(2);
  grCLsM1S->SetLineWidth(3);
  grCLsP2S->SetLineColor(4);
  grCLsP2S->SetLineWidth(3);
  grCLsM2S->SetLineColor(4);
  grCLsM2S->SetLineWidth(3);

  TH2D* h2d = new TH2D("h2d","h2d",100,muLo*0.95,muHi*1.05,100,0,0.5);
  h2d->SetXTitle("Signal Scaling Factor");
  h2d->SetYTitle("CLs");
  h2d->SetTitle("");

  TH2D* h2d2 = new TH2D("h2d2","h2d2",100,muLo*0.95,muHi*1.05,100,0,0.5);
  h2d2->SetXTitle("Signal Scaling Factor");
  h2d2->SetYTitle("CLs");
  h2d2->SetTitle("");
  TCanvas* c2d = new TCanvas("c2d","c2d");
  h2d->Draw();
  c2d->SetGridx();
  c2d->SetGridy();
  grCLsb->Draw("same");
  grCLsMed->Draw("same");
  grCLsObs->Draw("same");
  grCLsbObs->Draw("same");
  grCLb->Draw("same");
  TLegend* leg = new TLegend(0.6,0.6,0.9,0.9);
  leg->AddEntry(grCLsb,"CLsb Med","L");
  leg->AddEntry(grCLsMed,"CLs Med","L");
  leg->AddEntry(grCLsbObs,"CLsb Obs","L");
  leg->AddEntry(grCLsObs,"CLs Obs","L");
  leg->Draw("same");

  TCanvas* c2d2 = new TCanvas("c2d2","c2d2");
  h2d2->Draw();
  c2d2->SetGridx();
  c2d2->SetGridy();
  grCLsMed->Draw("same");
  grCLsP1S->Draw("same");
  grCLsP2S->Draw("same");
  grCLsM1S->Draw("same");
  grCLsM2S->Draw("same");
  TLegend* leg2 = new TLegend(0.6,0.6,0.9,0.9);
  leg2->AddEntry(grCLsP2S,"#pm2 Std. Dev.","L");
  leg2->AddEntry(grCLsP1S,"#pm1 Std. Dev.","L");
  leg2->AddEntry(grCLsMed,"Median","L");
  leg2->Draw("same");

  double limFit[6];
  double limExtrap[6];

  calculateLimit(limFit[0],grCLsObs,1);
  calculateLimit(limFit[1],grCLsM2S,1);
  calculateLimit(limFit[2],grCLsM1S,1);
  calculateLimit(limFit[3],grCLsMed,1);
  calculateLimit(limFit[4],grCLsP1S,1);
  calculateLimit(limFit[5],grCLsP2S,1);


  calculateLimit(limExtrap[0],grCLsObs,0);
  calculateLimit(limExtrap[1],grCLsM2S,0);
  calculateLimit(limExtrap[2],grCLsM1S,0);
  calculateLimit(limExtrap[3],grCLsMed,0);
  calculateLimit(limExtrap[4],grCLsP1S,0);
  calculateLimit(limExtrap[5],grCLsP2S,0);

  string label[6];
  label[0] = "Obs: ";
  label[1] = "M2S: ";
  label[2] = "M1S: ";
  label[3] = "Med: ";
  label[4] = "P1S: ";
  label[5] = "P2S: ";

  cout << "\n\n\nCalculated limit using Erfc fit:" << endl;
  for(int i=0; i<6; i++){ cout << label[i] << limFit[i] << endl;}

  cout << "\nCalculated limit using linear extrapolation:" << endl;
  for(int i=0; i<6; i++){ cout << label[i] << limExtrap[i] << endl;}

}
