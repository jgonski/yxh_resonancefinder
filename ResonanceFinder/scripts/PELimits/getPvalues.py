#!/usr/bin/env python

import sys
import ROOT
import os

if len(sys.argv)<2:
    print """
Usage: 
  python %prog [workspace] [ws flag] [exp/obs] [mass]

    expected = 1
    observed = 0
    default mass point = 125
"""
    sys.exit()

ws = sys.argv[1]

flag = sys.argv[2]

if len(sys.argv)>3:
    mass = sys.argv[3]
else:
    mass = "1000"

imass = int(mass)

if len(sys.argv)>4:
    nullToys = int(sys.argv[4])
else:
    nullToys = 1000

if len(sys.argv)>5:
    altToys = int(sys.argv[5])
else:
    altToys = 0

if len(sys.argv)>6:
    sigRate = float(sys.argv[6])
else:
    sigRate= 1.0

if len(sys.argv)>7:
    doPFLH = int(sys.argv[7])
else:
    doPFLH = 1

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L frequentistPvalueCalc.C+")

#outDir = flag + "-" + mass
outDir = "./"

print "Arg 1: %s" % ws
print "Arg 2: %s" % flag
print "Arg 3: %s" % mass
print "Arg 4: %d" % nullToys
print "Arg 5: %d" % altToys
print "Arg 6: %f" % sigRate
print "Arg 7: %d" % doPFLH

#ROOT.frequentistPvalueCalc(ws, "combined","ModelConfig", "obsData", "asimovData", outDir, imass, nullToys, altToys, sigRate, doPFLH, 4)
ROOT.frequentistPvalueCalc(ws, "combined","ModelConfig", "obsData", "asimovData", outDir, imass, nullToys, altToys, sigRate, doPFLH, 0)
#ROOT.frequentistPvalueCalc()


