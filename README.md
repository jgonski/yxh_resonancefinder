* Checkout

    git clone ssh://git@gitlab.cern.ch:7999/atlas-phys/exot/dbl/ResonanceFinder.git

* Setup (r21 with cmake)

    cd ResonanceFinder

    source setup_RF_v21.sh

    cd build

    make 

    source x86_64-slc6-gcc62-opt/setup.sh

    cd -

* Setup (r20.7 with cmt)

    cd ResonanceFinder/ResonanceFinder

    source setup_RF.sh
    
    rc compile